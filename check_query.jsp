<%@ page import="java.util.*, java.io.*,org.apache.commons.lang3.StringUtils,seek.CheckQuery,seek.CheckDataset" %>
<%! Vector<String> genes, absent; %>
<% 	genes = new Vector<String>();
	absent = new Vector<String>();
	File tempDir = (File) getServletContext().getAttribute("javax.servlet.context.tempdir");
	String entrez_mapping = tempDir.getAbsolutePath() + "/gene_entrez_symbol.txt";

	try{
		CheckQuery cq = new CheckQuery(entrez_mapping);
		StringTokenizer st = new StringTokenizer(
			request.getParameter("query_genes"), " ");
		while(st.hasMoreTokens()){
			String g = st.nextToken().toUpperCase();
			if(cq.GetEntrez(g)==null){
				absent.add(g);
			}else{
				genes.add(cq.GetEntrez(g));
			}
		}

	}catch(IOException e){
		System.out.println("Exception!");
	}
%>

<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Untitled Form</title>
<link type="text/css" href="jquery.qtip.min.css" rel="Stylesheet" />
<link type="text/css" href="css/smoothness/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.22.custom.min.js"></script>
<script src="jquery-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="jquery.qtip.min.js"></script>
<script src="jquery.ba-throttle-debounce.min.js"></script>

<script>
$(document).ready(function(){
	sessionStorage.clear();
	
	var sessionID = "<%=session.getId()%>";
	var times = 0;
	
	function poll_progress(){
		$.get("/servlet/GetSearchProgress", function(responseText){
			var a = responseText.split("\n");
			a.pop();
			$("#search_result").html(times + " " + a.join("<br>"));
			if(a[a.length-1]=="Done writing results."){
				alert("Done!!");
			}else{
				setTimeout(poll_progress, 1000);
			}
			times+=1;
		});	
	}

	$("#search_box").click(function(){
		//alert("Doing search");
		$.get("/servlet/DoSearch", {sessionID:sessionID}, function(responseText){
		});
		setTimeout(poll_progress, 1000);
		return false;
	});

});
</script>

</head>

<body>
<%@ include file="query_box.jsp"%>

<script type="text/javascript">
document.getElementById("query_genes").value="<%=request.getParameter("query_genes")%>";
</script>

<br>
<%
	session.setAttribute("query_genes", request.getParameter("query_genes"));

	if(absent.size()>0){
		for(int i=0; i<absent.size(); i++){
			%> <%=absent.get(i)%> does not exist in the database! Remove this gene from query to continue. <br>
			<%
		}
	}
	else{

%>
Query in entrez id:
<%

		for(int i=0; i<genes.size(); i++){
			%> <%=genes.get(i)%> 
			<%
		}
		%><br><br><%

		String sessionID = session.getId();
		CheckDataset cd = new CheckDataset(genes, tempDir.getAbsolutePath(), sessionID);
		Vector<String> dsets = cd.GetDatasetsContainingAllGenes();
		
		for(String g : genes){
			%>Found <%=cd.GetNumberDatasetsContainingGene(g)%> datasets containing gene <%=g%><br>
			<%
		}
		%><br>Found <%=dsets.size()%> datasets containing all genes. 
		<br>These datasets will be searched.<br>
		<form name="input" action="do_query.jsp" method="post">
		<input type="hidden" name="query_genes" id="query_genes" value="<%=StringUtils.join(genes, " ")%>">
		<input type="hidden" name="datasets" id="datasets" value="<%=StringUtils.join(dsets, " ")%>">
		<input type="submit" value="Search">
		</form>
		<br>
		<div id="dset_list" style="display:none;">
		<%
		for(int i=0; i<dsets.size(); i++){
			%> <%=dsets.get(i)%> <br>
			<%
		}
		%></div>
		<%
		session.setAttribute("query_attr", genes);
		session.setAttribute("dset_attr", dsets);
		
		//String ge = java.net.URLEncoder.encode(StringUtils.join(genes, " "), "UTF-8");
		//String da = java.net.URLEncoder.encode(StringUtils.join(dsets, " "), "UTF-8");
		
		%>
		<a id="search_box" href="#">Search Link</a>
		<div id="search_result">
		</div>
		
		
		<%
		
		
		
	}
%>





</body>
</html>
