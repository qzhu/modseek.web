
function adjust_window(){
	var trueWidth = 0;
	if(imageWidth>microarrayWindowWidth){
		trueWidth = microarrayWindowWidth;
	}else{
		trueWidth = imageWidth;
	}
	$("#divHeader").css("width", (trueWidth) + "px");
	$("#query_table_div").css("width", (trueWidth-15) + "px");
	if(pageMode=="expression")
		$("#dendrogram_div").css("width", (trueWidth-15) + "px");
	$("#table_div").css("width", (trueWidth) + "px");
	$("#wrapper").css("width", panelWindowWidth.toString() + "px"); 
}

function show_dataset_weight(){
	var dsetQuery = [];
	for(var vi=0; vi<dsetNames.length; vi++){
		//var dset_tmp = dsetNames[vi].split(".");
		//dsetQuery.push(dset_tmp[0] + "." + dset_tmp[1]);
		dsetQuery.push(dsetNames[vi]);
	}
	$.get("../servlet/GetScoreServlet", 
	{organism:organism, sessionID:sessionID, type:"dataset_weight", keyword:dsetQuery.join("+")}, 
	function(responseText){
		var s = responseText.split(";");
		for(var vi=0; vi<s.length; vi++){
			$("#dset_"+vi).find(":last").after($(document.createElement("br")));
			$("#dset_"+vi).find("br:last").after(
				$(document.createElement("div"))
					.css("padding", "0px")
					.css("margin", "0px")
					.css("border", "0px")
					.attr("id", "dataset_weight_"+vi)
					.addClass("small_font")
					.html(format_weight_percentage(s[vi]))
					.after($("<br>"))
					.after($("<br>"))
				);
			dsetWeight.push(s[vi]);
		}
	});
}

function format_weight(num){
	var f = num.split("e");
	var first = f[0].substring(0,3);
	var second = "e";
	var third = parseInt(f[1]);
	return "<font style=\"font-family:Arial; font-size:10px;\">" + first + 
		"</font><br><font style=\"font-family:Arial; font-size:10px;\">" + second +
		"</font><font style=\"font-family:Arial; font-size:10px;\">" + third;
}

function format_weight_percentage(num){
	return "<font style=\"font-family:Arial; font-size:10px;\">" + num + 
		"</font>";
}

function fix_gradient(){
	var dd = $("<tr>").attr("id", "gradient_label")
		.addClass("small_font").css("font-size", "9px")
		.append($("<td>")).append($("<td>"));
		
	var lower_clip = down_clip;
	var upper_clip = up_clip;

	if(pageMode=="expression"){
	}else{
		upper_clip = 3.2;
		lower_clip = -3.2;
	}

	var max_size = color_red.length;
	var unit_length = (upper_clip - lower_clip) / max_size;
	var st = 0;
	for(var i=0; i<color_red.length; i++){
		st = lower_clip + unit_length*i;
		vv = st.toFixed(1);
		if(i%2==0) dd = dd.append($("<td>").append(vv));
		else dd = dd.append($("<td>"));
	}

	$("#gradient_scale #gradient_label").remove();
	$("#gradient_scale").append(dd);

	if(pageMode=="coexpression"){
		var dd = $("<tr>").attr("id", "gradient_label")
		.addClass("small_font").css("font-size", "9px")
		.append($("<td>")).append($("<td>"));
		max_size = query_color_red.length;
		unit_length = (query_upper_clip - query_lower_clip) / max_size;
		st = 0;
		for(var i=0; i<query_color_red.length; i++){
			st = query_lower_clip + unit_length*i;
			var vv = st.toFixed(1);
			dd.append($("<td>").append(vv));
		}
		$("#query_gradient_scale #gradient_label").remove();
		$("#query_gradient_scale").append(dd);
	}
}


function activate_external_link(){
	$("a[rel*=external]").unbind("click").click(function(){
		window.open($(this).attr("href"));
		return false;
	});
}

function get_response_parameters(str){
	var prmstr = str;
	var prmarr = prmstr.split("|");
	var params = {};
	
	for(var i=0; i<prmarr.length; i++){
		var tmparr = prmarr[i].split(":");
		if(tmparr.length>2){
			var x = "";
			for(var j=0; j<tmparr.length-2; j++){
				x+=tmparr[j+1]+":";
			}
			x+=tmparr[tmparr.length-2];
			params[tmparr[0]] = x;
		}
		else{
			params[tmparr[0]] = tmparr[1];
		}
	}
	return params;
}

function process_response_expression_image(responseText){
	var args = get_response_parameters(responseText);
	var dset_score = args["dset_score"].split(",");
	var gene_score = args["gene_score"].split(",");
	var pval_score = args["pval_score"].split(",");
	var dsets = args["dsets"].split(",");
	var genes = args["genes"].split(",");
	var query = args["query"].split(",");
	var geneHGNC = args["geneHGNC"].split(",");
	var queryHGNC = args["queryHGNC"].split(",");
	var sizes = args["sizes"].split(",");
	horizUnit = parseInt(args["horizUnit"]);
	vertUnit = parseInt(args["vertUnit"]);
	horizSpacing = parseInt(args["horizSpacing"]);
	imageWidth = parseInt(args["imageWidth"]);
	windowWidth = imageWidth;
	var neutralColor = args["neutralColor"].split(",");
	var mainColors = args["mainColors"].split(",");
	var sessionID = args["sessionID"];	
	var sampleOrder = null;
	var averageExpr = null;
	var coexprMatrix = null;
	var queryColors = null;

	var k = 0;
	if(pageMode=="expression"){
		sampleOrder = args["sampleOrder"].split(",");
		averageExpr = args["averageExpr"].split(",");
	}else if(pageMode=="coexpression"){
		coexprMatrix = args["coexprMatrix"].split(",");
		queryColors = args["queryColors"].split(",");
		k = 0;
		query_color_red = [];
		query_color_green = [];
		query_color_blue = [];
		while(k<queryColors.length){
			query_color_red.push(parseInt(queryColors[k]));
			k++;
			query_color_green.push(parseInt(queryColors[k]));
			k++;
			query_color_blue.push(parseInt(queryColors[k]));
			k++;
		}
		query_upper_clip = parseFloat(args["queryUpperClip"]);
		query_lower_clip = parseFloat(args["queryLowerClip"]);
	}

	k = 0;
	color_red = [];
	color_green = [];
	color_blue = [];
	while(k<mainColors.length){
		color_red.push(parseInt(mainColors[k]));
		k++;
		color_green.push(parseInt(mainColors[k]));
		k++;
		color_blue.push(parseInt(mainColors[k]));
		k++;
	}

	k = 0;
	neutral_color_red = parseInt(neutralColor[k]); k++;
	neutral_color_green = parseInt(neutralColor[k]); k++;
	neutral_color_blue = parseInt(neutralColor[k]); k++;

	geneScore = [];
	for(var i=0; i<gene_score.length; i++){
		geneScore.push(parseFloat(gene_score[i]));
	}
	pvalScore = [];
	for(var i=0; i<pval_score.length; i++){
		pvalScore.push(parseFloat(pval_score[i]));
	}
	dsetScore = [];
	for(var i=0; i<dset_score.length; i++){
		var x = parseFloat(dset_score[i]) * 100.0;
		dsetScore.push(x.toFixed(2));
	}
	dsetSizes = [];
	for(var i=0; i<sizes.length; i++){
		dsetSizes.push(parseInt(sizes[i]));
	}
	dsetNames = [];
	for(var i=0; i<dsets.length; i++){
		dsetNames.push(dsets[i]);
	}
	geneNames = [];
	for(var i=0; i<genes.length; i++){
		geneNames.push(genes[i]);
	}
	geneHGNCNames = [];
	for(var i=0; i<genes.length; i++){
		geneHGNCNames.push(geneHGNC[i]);
	}
	queryNames = [];
	for(var i=0; i<query.length; i++){
		queryNames.push(query[i]);
	}
	queryHGNCNames = [];
	for(var i=0; i<query.length; i++){
		queryHGNCNames.push(queryHGNC[i]);
	}

	if(pageMode=="expression"){
		sample_order = new Array(dsetNames.length);
		for(var vi=0; vi<sample_order.length; vi++)
			sample_order[vi] = new Array(dsetSizes[vi]);
		average_expr = new Array(dsetNames.length);
		for(var vi=0; vi<average_expr.length; vi++)
			average_expr[vi] = new Array(dsetSizes[vi]);

		k = 0;
		for(var vi=0; vi<sample_order.length; vi++){
			for(var vj=0; vj<dsetSizes[vi]; vj++){
				sample_order[vi][vj] = parseInt(sampleOrder[k]);
				k++;
			}
		}
		k = 0;
		for(var vi=0; vi<average_expr.length; vi++){
			for(var vj=0; vj<dsetSizes[vi]; vj++){
				average_expr[vi][vj] = averageExpr[k];
				k++;
			}
		}
	}else if(pageMode=="coexpression"){
		comat = new Array(dsetNames.length);
		for(var vi=0; vi<dsetNames.length; vi++)
			comat[vi] = new Array(geneNames.length);
		k = 0;
		for(var vi=0; vi<comat.length; vi++){
			for(var vj=0; vj<geneNames.length; vj++){
				var x = parseFloat(coexprMatrix[k]);
				if(x>327){
					comat[vi][vj] = "Not present";
				}else{
					comat[vi][vj] = x.toFixed(2);
				}
				k++;
			}
		}
	}				
}

function get_request_arguments(str){
	var prmstr = str;
	var prmarr = prmstr.split ("&");
	var params = {};
	for ( var i = 0; i < prmarr.length; i++) {
		var tmparr = prmarr[i].split("=");
		params[tmparr[0]] = tmparr[1];
	}
	return params;
}

function copy_args(args){
	var ex = {};
	for(var e in args){
		ex[e] = args[e];
	}
	return ex;
}

function reset_expression_view(){
	$("#header_image_src").attr("src", "../img/loading.png");
	$("#query_microarray").attr("src", "../img/loading.png");
	$("#microarray").attr("src", "../img/loading.png");
	if(pageMode=="expression")
		$("#dendrogram_image_src").attr("src", "../img/loading.png");
	$("#query_names").empty();
	$("#gene_names").empty();
}

function reset_description(){
	dsetDescriptionOneLineUnformatted = [];
	dsetDescriptionTitleUnformatted = [];
	dsetDescriptionOneLine = [];
	dsetPubmed = [];
	dsetDescriptions = [];
	geneDescriptionUnformatted = [];
	geneDescriptionOneLineUnformatted = [];
	geneDescriptionOneLine = [];
	geneDescriptions = [];
}

function refresh_images(){
	var imageFile=null, headerFile=null, queryimageFile=null, 
		dendrogramFile=null;
	if(pageMode=="expression"){
		imageFile = sessionID + "_image.png";
		headerFile = sessionID + "_header_image.png";
		queryimageFile = sessionID + "_query_image.png";
		dendrogramFile = sessionID + "_dendrogram_image.png";
	}else if(pageMode=="coexpression"){
		imageFile = sessionID + "_coexpress_image.png";
		headerFile = sessionID + "_coexpress_header_image.png";
		queryimageFile = sessionID + "_coexpress_query_image.png";
	}
	//added date as a parameter to Servlet to force browser refresh of the image
	var da = new Date();
	$("#header_image_src").attr("src", "../servlet/ImageDownloadServlet?organism=" + organism + "&file=" + 
		headerFile + "&" + da.getTime());
	$("#query_microarray").attr("src", "../servlet/ImageDownloadServlet?organism=" + organism + "&file=" + 
		queryimageFile + "&" + da.getTime());
	$("#microarray").attr("src", "../servlet/ImageDownloadServlet?organism=" + organism + "&file=" + 
		imageFile + "&" + da.getTime());
	if(pageMode=="expression"){
		$("#dendrogram_image_src").attr("src", "../servlet/ImageDownloadServlet?organism=" + organism + "&file=" + 
		dendrogramFile + "&" + da.getTime());
	}
}

function update_page_buttons(args){
	$(".gene_next_page").each(function(index){
		if(pageGene==pGene.length-1){
			$(this).hide();
		}else{
			$(this).show().unbind("click").click(function(){
				var va = copy_args(args);
				va["g_start"] = parseInt(va["g_end"]) + 1;
				va["g_end"] = va["g_start"] + pGene[pageGene+1] - 1;
				pageGene+=1;
				reset_expression_view();
				expression_image(va);
				return false;
			});
		}
	});

	$(".gene_prev_page").each(function(index){
		if(pageGene==0){
			$(this).hide();
		}else{
			$(this).show().unbind("click").click(function(){
				var va = copy_args(args);
				va["g_start"] = parseInt(va["g_start"]) - pGene[pageGene-1];
				va["g_end"] = va["g_start"] + pGene[pageGene-1] - 1;
				pageGene-=1;
				reset_expression_view();
				expression_image(va);
				return false;
			});
		}
	});

	$(".dset_next_page").each(function(index){
		if(pageDataset==pDataset.length-1){
			$(this).hide();
		}else{
			$(this).show().unbind("click").click(function(){
				var va = copy_args(args);
				va["d_start"] = parseInt(va["d_end"]) + 1;
				va["d_end"] = va["d_start"] + pDataset[pageDataset+1] - 1;
				pageDataset+=1;
				reset_expression_view();
				expression_image(va);
				return false;
			});
		}
	});

	$(".dset_prev_page").each(function(index){
		if(pageDataset==0){
			$(this).hide();
		}else{
			$(this).show().unbind("click").click(function(){
				var va = copy_args(args);
				va["d_start"] = parseInt(va["d_start"]) - pDataset[pageDataset-1];
				va["d_end"] = va["d_start"] + pDataset[pageDataset-1] - 1;
				pageDataset-=1;
				reset_expression_view();
				expression_image(va);
				return false;
			});
		}
	});

	/* Dataset and Gene labels */
	var geneEnd = args["g_end"] + 1;
	var tx = $("#gene_label_sel").detach();
	tx = tx.empty();
	$(".gene_page_label")
		.append($("<select>")
		.attr("name", "gene_label_sel")
		.attr("id", "gene_label_sel")
		.css("width", "100px")
		.change(function(){
			var va = copy_args(args);
			var tt = $("#gene_label_sel option:selected").text().split("-");
			va["g_start"] = parseInt(tt[0]) - 1;
			va["g_end"] = parseInt(tt[1]) - 1;
			pageGene = parseInt($("#gene_label_sel option:selected").val());
			reset_expression_view();
			expression_image(va);
			return false;
		})
	);
	var gStart = 1;
	var gEnd = 0;
	for(var i=0; i<pGene.length; i++){
		gStart = gEnd + 1;
		gEnd = gStart + pGene[i] - 1;
		$("#gene_label_sel").append(
			$("<option>").val(i).text(gStart + "-" + gEnd)
		);
	}
	$("#gene_label_sel").find('option[value="' + (pageGene) +'"]').attr("selected", "selected");
	

	var dsetEnd = args["d_end"] + 1;
	tx = $("#dset_label_sel").detach();
	tx = tx.empty();
	$(".dset_page_label")
		.append($("<select>")
		.attr("name", "dset_label_sel")
		.attr("id", "dset_label_sel")
		.css("width", "100px")
		.change(function(){
			var va = copy_args(args);
			var tt = $("#dset_label_sel option:selected").text().split("-");
			va["d_start"] = parseInt(tt[0]) - 1;
			va["d_end"] = parseInt(tt[1]) - 1;
			pageDataset = parseInt($("#dset_label_sel option:selected").val());
			reset_expression_view();
			expression_image(va);
			return false;
		})
	);
	var dStart = 1;
	var dEnd = 0;
	for(var i=0; i<pDataset.length; i++){
		dStart = dEnd + 1;
		dEnd = dStart + pDataset[i] - 1;
		$("#dset_label_sel").append(
			$("<option>").val(i).text(dStart + "-" + dEnd)
		);
	}
	$("#dset_label_sel").find('option[value="' + (pageDataset) +'"]').attr("selected", "selected");
		

}

function extract_description(ss){
	var dsetArray = ss[0].split("==");
	var geneArray = ss[1].split("==");
	//var gsmArray = ss[2].split("==");	
	reset_description();			

	for(var i=0; i<dsetArray.length; i++){
		var dd = dsetArray[i].split(";;");
		dsetDescriptionOneLineUnformatted.push(dd[1]);
		dsetDescriptionTitleUnformatted.push(dd[2]);
		var pubmed_line = dd[3].replace("|", " | ");
		dsetPubmed.push("PMID: " + pubmed_line);
		dd[0] = "<b><span style='font-size:16px;'>" + dd[0] +
				"</span> <span style='font-size:12px;'>(" + dd[1] + 
				")</span></b> <a rel='external' href='" +
				"http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=" +
				dd[0] + "'>NCBI GEO</a>";
		dsetDescriptionOneLine.push(dd[0]);
		//dd.splice(0, 3);
		//dsetDescriptions.push(dd.join("<br>"));
	}

	for(var i=0; i<geneArray.length; i++){
		var gg = geneArray[i].split(";;");
		//geneDescriptionUnformatted.push("<b>" + gg[1] + ". </b> " + gg[3]);
		geneDescriptionOneLineUnformatted.push(gg[1]);
		gg[0] = "<b><span style='font-size:16px;'>" + geneHGNCNames[i] +
				"</span> <span style='font-size:12px;'>(" + gg[1] + 
				")</span></b> <a rel='external' href='" +
				"http://www.ncbi.nlm.nih.gov/gene?term=" +
				gg[0] + "'>Entrez: " + geneNames[i] + "</a>";
		//gg.splice(1, 2);
		geneDescriptionOneLine.push(gg[0]);
		//gg.splice(0, 1);
		//geneDescriptions.push(gg.join("<br>"));
	}

	
	arrays = new Array(dsetNames.length);
	for(var vi=0; vi<arrays.length; vi++){
		arrays[vi] = new Array(dsetSizes[vi]);
		for(var j=0; j<dsetSizes[vi]; j++){
			arrays[vi][j] = "uninitialized";
		}
	}

	/*		
	for(var i=0; i<gsmArray.length; i++){
		var dd = gsmArray[i].split(";;");
		if(dd.length!=dsetSizes[i]){
			for(var j=0; j<dsetSizes[i]; j++){
				arrays[i][j] = "NA or array not found";
			}
		}else{
			for(var j=0; j<dsetSizes[i]; j++){
				arrays[i][j] = dd[j];
			}
		}
	}*/
}

function update_gradient(){
	/* Gradient update */			
	$("#gradient_1").nextAll(".gradient").remove();

	for(var i=0; i<color_red.length; i++){
		$("#gradient_2").before(
			$("<td>")
				.addClass("gradient")
				.css("width", "16px").css("height", "10px")
				.css("background-color", "rgb(" + color_red[i] + "," 
					+ color_green[i] + "," + color_blue[i] + ")")
		);
	}

	if(pageMode=="coexpression"){
		$("#gradient_3").nextAll(".gradient").remove();
		for(var i=0; i<query_color_red.length; i++){
			$("#gradient_4").before(
				$("<td>")
					.addClass("gradient")
					.css("width", "32px").css("height", "10px")
					.css("background-color", "rgb(" + query_color_red[i] + ","
						+ query_color_green[i] + "," + query_color_blue[i]+")")
			);
		}
	}
}

function update_gene_table(){
	//==================================================
	for(var i=0; i<queryNames.length; i++){
		var sid = "query_" + queryNames[i];
		var dsid = "#query_" + queryNames[i];
		var title = "<b>Query gene</b>";
		$("#query_names").append(
			$("<tr>")
			.append($("<td>")
				.addClass("exprTable_col1")
				.append("&nbsp;")
			)
			.append($("<td>")
				.addClass("exprTable_col1")
				.append("&nbsp;")
			)
			.append($("<td>")
				.css("width", "100px")
				.attr("id", sid)
				.addClass("queryLink")
				.append($("<a>")
					.attr("href", "#")
					.attr("title", title)
					.append(queryHGNCNames[i])
				)
			)
		);
	}

	for(var i=0; i<geneNames.length; i++){
		var sid = "gene_" + geneNames[i];
		var dsid = "#gene_" + geneNames[i];
		var title = geneDescriptionUnformatted[i];
		var gsc = geneScore[i].toFixed(3);
		$("#gene_names").append(
			$("<tr>")
			.append($("<td>")
				.addClass("exprTable_col1")
				.append(i + geneStart)
			)
			.append($("<td>")
				.addClass("exprTable_col1")
				.append(gsc)
			)
			.append($("<td>")
				.css("color", "#000066") //light background
				//.css("color", "#ffffc8") //dark background
				.attr("id", sid)
				.addClass("tableFirstCol")
				.append(geneHGNCNames[i] + "&nbsp;")
			)
		);
	}
}

function processPageLimit(responseText){
	pGene = [];
	pDataset = [];
	pageGene = 0;
	pageDataset = 0;
	var t = responseText.split(";")[1].split(" ");
	for(var vi=0; vi<t.length; vi++){
		pGene.push(parseInt(t[vi]));
	}
	t = responseText.split(";")[0].split(" ");
	for(var vi=0; vi<t.length; vi++){
		pDataset.push(parseInt(t[vi]));
	}
}

function expression_image(args){
	sessionID = args["sessionID"];
	dsetStart = parseInt(args["d_start"]) + 1;
	geneStart = parseInt(args["g_start"]) + 1;

	sessionSetDefault("pval_filter", "-1");
	pvalCutoff = sessionStorage.getItem("pval_filter");
	if(pvalCutoff=="-1") pval_filter = false;
	else pval_filter = true;

	sessionSetDefault("vis_value", "norm");
	var norm = sessionStorage.getItem("vis_value");
	var color_scale = sessionStorage.getItem("vis_color_scale");
	if(norm=="norm"){
		toNormalize="1";
		args["norm"] = "true";
		args["gradient_up"] = "2.0";
		args["gradient_low"] = "-2.0";
	}else{
		toNormalize="0";
		args["norm"] = "false";
		args["gradient_up"] = "10.0";
		args["gradient_low"] = "0.0";
	}

	up_clip = parseInt(args["gradient_up"]);
	down_clip = parseInt(args["gradient_low"]);
	normalize_data = (args["norm"] === 'true');

	$.post("../servlet/GetExpressionImage", {organism:organism,
	sessionID:args["sessionID"],
	d_start:args["d_start"], g_start:args["g_start"], 
	d_end:args["d_end"], g_end:args["g_end"], 
	gradient_up:args["gradient_up"],
	gradient_low:args["gradient_low"], norm:args["norm"], 
	sort_sample_by_expr:args["sort_sample_by_expr"], 
	mode:pageMode, color_choice:color_scale,
	cluster_by_query: false,
	//temporary value for p-val
	filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
	rbp_p:sessionStorage.getItem("rbp_p"), 
	multiply_d_weight:sessionStorage.getItem("multiply_d_weight"), 
	specify_genes:sessionStorage.getItem("specify_genes"), 
	display_dset_keywords:sessionStorage.getItem("display_dset_keywords")},
	function(responseText){

		process_response_expression_image(responseText);
	
		$.get("../servlet/GetDescription", {organism:organism,
		dset:dsetNames.join(","), gene:geneNames.join(",")}, 
		function(responseText){
			var ss = responseText.split("&&");

			extract_description(ss); //includes propagating the description arrays

			refresh_images(); //updating the various heat map images

			update_page_buttons(args);

			update_gradient();

			update_gene_table();

			init_commands(); //see viewer_init.js
		});

	});
}

/* Data Declaration */
var sessionID = "";
var dsetSizes=[];
var dsetNames=[];
var geneNames=[];
var geneHGNCNames=[];
var geneScore=[];
var dsetScore=[];
var pvalScore=[];

var dsetStart = 0;
var geneStart = 0;
var pageDataset = 0;
var pageGene = 0;
var pGene = []; //page sizes
var pDataset = []; //page sizes

var queryNames=[];
var queryHGNCNames=[];

var dsetDescriptions=[]; //needed
var geneDescriptions=[]; //needed
var dsetDescriptionOneLine=[]; //needed
var dsetDescriptionOneLineUnformatted=[]; //needed
var dsetDescriptionTitleUnformatted=[]; //needed
var dsetPubmed=[]; //needed
var geneDescriptionOneLine=[]; //needed
var geneDescriptionOneLineUnformatted=[]; //needed
var geneDescriptionUnformatted=[]; //needed
var dsetWeight=[];

var color_red = [];
var color_green = [];
var color_blue = [];

var query_color_red = [];
var query_color_green = [];
var query_color_blue = [];

var neutral_color_red = 0;
var neutral_color_green = 0;
var neutral_color_blue = 0;

var arrays;
var sample_order; //for expression mode
var average_expr; //for expression mode

var comat; //for coexpression mode

var up_clip;
var down_clip;

var query_upper_clip;
var query_lower_clip;

var normalize_data;
var toNormalize; //same thing as normalize_data
var middle;

var vertUnit;
var horizUnit;
var horizSpacing;

var imageWidth;
var panelWindowWidth = 1150;
var microarrayWindowWidth = 900;
var windowWidth;

var pval_filter;
var pvalCutoff;

var which_category;

$(document).ready(function(){
	var args = get_request_arguments(window.location.search.substr(1));

	sessionSetDefault("pval_filter", "-1");
	pvalCutoff = sessionStorage.getItem("pval_filter");
	if(pvalCutoff=="-1") pval_filter = false;
	else pval_filter = true;

	sessionSetDefault("specify_genes", "");
	sessionSetDefault("sessionID", "");
	sessionSetDefault("display_all_genes", "true");
	sessionSetDefault("dset_search_mode", "all");
	sessionSetDefault("display_dset_keywords", "false");

	if(sessionStorage.getItem("sessionID")!=args["sessionID"]){
		sessionStorage.setItem("specify_genes", "");
		sessionStorage.setItem("display_all_genes", "true");
	}

	sessionStorage.setItem("sessionID", args["sessionID"]);

	$.post("../servlet/GetPageLimit", {organism:organism,
	sessionID:args["sessionID"], mode:pageMode, 
	filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
	specify_genes:sessionStorage.getItem("specify_genes")},
	function(responseText){
		if(responseText=="Error: Empty!"){
			alert("Error in the genes that you specified in Options, none of the genes exists, or " + 
			"they all overlap with the query genes! Please fix this problem. Expression View cannot continue.");
			return false;
		}
		if(responseText.indexOf("Error: specified genes absent")==0){
			alert(responseText + "\n" + "Expression View cannot continue.");
			return false;
		}
		processPageLimit(responseText);
		args["d_start"] = 0;
		args["g_start"] = 0;
		args["d_end"]  = pDataset[0] - 1;
		args["g_end"] = pGene[0] - 1;

		$.get("../servlet/GetDatasetCategory", {sessionID:args["sessionID"], 
		organism:organism},
		function(dsetCategory){
			if(dsetCategory.indexOf("all")==0){
				if(dsetCategory.indexOf("guided")!=-1){
					which_category="All Datasets, guided by FN";
				}else{
					which_category="All Datasets";
				}
			}else if(dsetCategory.indexOf("category")==0){
				var x = dsetCategory.split(";");
				x.splice(0, 1);
				var xy = x.join(", ");
				which_category = xy;
			}else{
				which_category = "Custom";
			}
			$("#dataset_category").text(which_category);
			expression_image(args);
		});
		/*
		if(sessionStorage.getItem("dset_search_mode")!="all" ||
		sessionStorage.getItem("display_all_genes")=="false"){
			$("#partial_message").show();
			$("#partial_message").text("");
			if(sessionStorage.getItem("dset_search_mode")!="all"){
				$("#partial_message").text("Refined search enabled. ");
			}
			if(sessionStorage.getItem("display_all_genes")=="false"){
				$("#partial_message").text($("#partial_message").text() + 
				" Co-expressed gene filter enabled.");
			}
		}*/
	});
});

