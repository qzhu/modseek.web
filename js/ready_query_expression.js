//Accepts one dataset so far!
function process_query_expression(qid, org, sess, dataset, genes, item_id){
	var sgo = 1;
	$.post("/modSeek/servlet/GetExpressionInfoServlet", {organism:org, sessionID:sess, dataset:dataset, 
	normalize:1, gene:genes, cluster:1, show_gene_order:sgo, show_sample_order:1, 
	show_sample_description:1}, function(responseText){
		var div = responseText.split("^^^");
		var ii = 0;
		var dset_size = div[ii].split(";;"); ii++;
		var sample_order = div[ii].split(";;"); ii++;
		var gene_order = [];
		if(sgo==1){
			gene_order = div[ii].split(";;"); ii++;
		}else{
			for(var ij=0; ij<genes.split("+").length; ij++){
				gene_order.push(ij);
			}
		}
		//var gene_order = div[2].split(";;");
		var sample_description = div[ii].split(";;"); ii++;
		var expr_value = div[ii];

		var dd = dataset.split(".");
		if(dd.length==1){
			dd.push("RNASEQ");
		}else if(dd[1].indexOf("GPL")==-1){
			dd[1] = "RNASEQ";
		}
		var dset_label = dd[0] + "(" + dd[1] + ") <br>(" + 
			$("#"+item_id).attr("rank") + ", p<" + $("#"+item_id).attr("pvalue") + ")";
		var gene_label = $("#q_name_" + qid).text();
		gene_label = gene_label.split(" ");
		var tx = qe_process_response_nowrap(expr_value, sample_order, gene_order, sample_description, gene_label, dset_label,
			qid, org, sess, dataset, genes, item_id);
		if(tx.length==1){
			setTimeout(function(){
			alert("alert is printed!\n");
			//event_microarray_image(dsetID, geneID);
			}, 200);
			return false;
		}
		var t = tx[1];
		t = t.after(tx[0]);
		var tit = "Expression profile";

		$("#visdiv").qtip("option", {"content.text": t});
		$("#visdiv").qtip("api").set("content.title.text", tit);
		$("#visdiv").trigger("click");

		setTimeout(function(){
			activate_external_link();
		}, 500);

	});
}

function qe_judge_color(vi, up_clip, down_clip, size, unit_length, c_red, c_green, c_blue){
	var r = 0;
	var g = 0;
	var b = 0;
	var isAbsent = false;
	var neutral_color = 128;
	if(vi>up_clip && vi<326){
		r = c_red[size-1];
		g = c_green[size-1];
		b = c_blue[size-1];
	}else if(vi<down_clip){
		r = c_red[0];
		g = c_green[0];
		b = c_blue[0];
	}else if(vi>326){
		r = neutral_color; g = neutral_color; b = neutral_color;
		isAbsent = true;
	}else{
		var level = Math.round(1.0*(vi-down_clip)/unit_length);
		if(level<0){
			level = 0;
		}else if(level>=size){
			level = size - 1;
		}
		r = c_red[level];
		g = c_green[level];
		b = c_blue[level];
	}
	return [r,g,b,isAbsent];
}

//Key function!
function qe_group_attribute(gsms){
	//var gsms = arrays[dset_id];
	var attributes = [], u = {};
	for(var jj=0; jj<gsms.length; jj++){
		var s = gsms[jj].split("|");
		for(var jk=0; jk<s.length; jk++){
			var sx = s[jk].split(":");
			var thisVal;
			var nextVal;
			if(jk==0){
				thisVal = "GSM ID";
				nextVal = sx[0];
			}else if(sx.length==1){
				thisVal = "Line " + jk;
				if(thisVal=="Line 1"){
					thisVal="Sample ID";
				}else if(thisVal=="Line 2"){
					thisVal="Title";
				}
				nextVal = sx[0];
			}else{
				thisVal = "Attribute: " + sx[0];
				nextVal = sx[1];
			}
			if(u.hasOwnProperty(thisVal)){
				if(!u[thisVal].hasOwnProperty(nextVal)){
					u[thisVal][nextVal] = [];
				}
				u[thisVal][nextVal].push(jj);
				continue;
			}else{
				u[thisVal] = {};
				u[thisVal][nextVal] = [];
				u[thisVal][nextVal].push(jj);
			}
			attributes.push(thisVal);
		}
	}
	return [attributes, u];
}

//Another key function!
//element to attach the expression table
//re is the expression table
//gene_order is needed (if genes are clustered)
//topOffset is the 2nd return element from qe_draw_sample_label function
//NEED: judge_color()
function qe_draw_expression_table(tx, numG, numS, gene_order, geneLabel, re, topOffset){
	var c_red =  [0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 29, 57, 70, 103, 131, 169, 227, 255];
	var c_green =[135, 110, 95, 80, 65, 50, 35, 10, 0, 7, 7, 20, 43, 60, 90, 122, 150, 188, 211];
	var c_blue = [189, 168, 147, 126, 105, 84, 63, 41, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	var up_clip = 2;
	var down_clip = -2;

	var size = c_red.length;
	var unit_length = (up_clip - down_clip) / size;
	var cell_width = 10;

	var tt = $("<table>")
		.attr("id", "expression_table")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.css("width", 100+cell_width*numS+"px")
		.css("height", numG*15+"px")
		.css("position", "absolute")
		.css("top", 280 - 100 + topOffset + "px")
	;

	for(var vg=0; vg<numG; vg++){
		//if(vg==numG-1){
		//	tt.append($("<tr><td style=\"line-height:5px;\">&nbsp;</td></tr>"));
		//}
		tt.append($("<tr>")
			.css("color", "rgb(128, 128, 128)")
			.addClass("small_font"));
		//if(vg!=numG-1){
			tt.find("tr:last").append($("<td>")
				.addClass("small_font")
				.addClass("expression_query")
				.append(geneLabel[gene_order[vg]]));
		/*}else{
			tt.find("tr:last").append($("<td>")
				.addClass("big_font")
				.addClass("expression_gene")
				.append(geneLabel[vg]));
		}*/
		for(var i=0; i<numS; i++){
			var vi = parseFloat(re[vg][i]);
			var c = qe_judge_color(vi, up_clip, down_clip, size, unit_length, c_red, c_green, c_blue);
			var r = c[0];
			var g = c[1];
			var b = c[2];
			var isAbsent = c[3];
			if(isAbsent==true){
				tt.find("tr:last")
					.append($("<td>")
						.attr("colspan", numS)
						.css("color", "rgb(0,0,0)") //only needed for light background
						.text("Not present")
					);
				break;
			}
			tt.find("tr:last")
				.append($("<td>")
					.addClass("zoom_in_value")
					.css("width", cell_width + "px")
					//.attr("nowrap", "nowrap")
					.css("background-color", "rgb("+r+", "+g+", "+b+")")
					//.text(re[vg][i])
				);
		}
	}
	tx.append(tt);
	return tx;
}

//Another key function!
//tx: element to add the sample labels to
//labels: GSM description lines
function qe_draw_sample_label(tx, numS, dd, labels, attr_name){ //dd is sample order
	var gsmid = [], lab2 = [], lab1 = [];
	var maxLength = -1;
	for(var jj=0; jj<numS; jj++){
		j = dd[jj];
		var lab = new String(labels[j]);
		var lab_split = lab.split("|");
		gsmid.push(lab_split[0]);
		lab = lab.substring(lab.indexOf("|")+1);
		var thisVal, nextVal;
		var vi = 0;
		for(vi=0; vi<lab_split.length; vi++){
			var sx = lab_split[vi].split(":");
			if(vi==0){
				thisVal = "GSM ID";
				nextVal = sx[0];
			}else if(sx.length==1){
				thisVal = "Line " + vi;
				if(thisVal=="Line 1"){
					thisVal="Sample ID";
				}else if(thisVal=="Line 2"){
					thisVal="Title";
				}
				nextVal = sx[0];
			}else{
				thisVal = "Attribute: " + sx[0];
				nextVal = sx[1];
			}
			if(thisVal==attr_name){
				break;
			}
		}
		if(lab_split.length>0 && lab_split.length<=2){
		}else if(lab_split.length==0){
			nextVal = "...";
		}else if(vi==lab_split.length){
			nextVal = "...";
		}
		lab1.push(nextVal);
		if(nextVal.length>maxLength){
			maxLength = nextVal.length;
		}
		lab2.push(lab_split.join("<br>"));
	}
	var ll = Math.min(50, maxLength);
	var le = -100;
	if(ll<10){ 			le = -100;
	}else if(ll<20){	le = -50;
	}else if(ll<30){	le = 0;
	}else if(ll<40){	le = 50;
	}else{				le = 100;
	}

	for(var jj=0; jj<numS; jj++){
		//requires lab2, gsmid, jj, lab1
		//attaching the labels the samples===========================
		tx.append($("<div>")
			.addClass("rotate_270")
			.css("height", "50px")
			.css("width", "300px")
			.css("position", "absolute")
			//.css("top", "175px")
			.css("top", le + "px")
			//.css("left", -10 + jj * 30 + "px")
			.css("left",  -30 + jj * 10 + "px")
			.append($("<a>")
				.attr("rel", "external")
				.attr("href", "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=" + gsmid[jj])
				.addClass("small_font")
				.attr("id", "col_" + (jj+1))
				.attr("title", lab2[jj])
				.text(lab1[jj].substring(0, 50))
				//.text(lab.substring(0, 50))
				//.text(lab.substring(0, 20))
				.qtip({position: {my:"top left", at: "bottom left", adjust: {x: 20,},}, style:{classes: "ui-tooltip-gsm", }, })
				)
			);
	}
	return [tx, le];
}

//Key function
//order: sample order
//gorder: gene order
//re: original table
//si: number of arrays
function qe_prepare_table_clustering(order, gorder, re, numG, si){
	var xi = 0;
	var re2 = new Array(numG);
	for(var vi=0; vi<numG; vi++){
		re2[vi] = new Array(si);
	}
	for(var vi=0; vi<numG; vi++){
		for(var vj=0; vj<si; vj++){
			re2[vi][vj] = re[gorder[vi]][order[vj]];
		}
	}
	return [re2, order];
}

//Key function
//sam: sample attribute, a hash: (key: attribute value, value: samples with the attribute)
//re: original table
//si: number of arrays
function qe_prepare_table(sam, re, numG, si){
	var xi = 0;
	var isVisited = {};
	var dd = [];
	for(var i=0; i<si; i++){
		dd.push(i);
	}
	for(var kk in sam){ //for each attribute value
		for(var kj=0; kj<sam[kk].length; kj++){ //for each sample annotated to that attr. value
			dd[xi] = sam[kk][kj]; //let the current item be this sample
			isVisited[dd[xi]] = 1; //mark this item visited
			xi++;
		}
	}
	for(var kj=0; kj<si; kj++){
		if(!isVisited.hasOwnProperty(kj)){ //for those unvisited samples
			dd[xi] = kj; //mark them as visited one by one
			xi++;
		}
	}
	//new table
	var re2 = new Array(numG);
	for(var vi=0; vi<numG; vi++){
		re2[vi] = new Array(si);
	}
	for(var vi=0; vi<numG; vi++){
		for(var vj=0; vj<si; vj++){
			re2[vi][vj] = re[vi][dd[vj]];
		}
	}
	return [re2, dd];
}

//Main Query Expression Dialogue generating function
//Accepts one dataset, so far
function qe_process_response_nowrap(values, sample_order, gene_order, gsms, geneLabel, dsetLabel, 
qid, org, sess, dataset, genes, item_id){ //this line of parameters is used for event_...
	var ss = values;
	var dd = [];
	for(var i=0; i<sample_order.length; i++){
		dd.push(sample_order[i]);
	}
	var dg = [];
	for(var i=0; i<gene_order.length; i++){
		dg.push(gene_order[i]);
	}

	var v = [];
	var w = ss.split(","); //gene
	var xx = qe_group_attribute(gsms);

	//var xx = group_dataset_attributes(dset_id);
	if(xx.length==1){ //should expect two return parameters
		return [false];
	}

	var attributes = xx[0];
	var u = xx[1];
	//add the gene
	for(var i=0; i<w.length; i++){
		v.push(w[i]);
	}

	//get table size
	var numG = geneLabel.length;
	var si = gsms.length;

	//prepare table entry - re
	var re = new Array(numG);
	for(var vi=0; vi<numG; vi++){
		re[vi] = new Array(si);
	}

	var ss = 0;
	for(var vi=0; vi<numG; vi++){
		for(var vj=0; vj<si; vj++){
			re[vi][vj] = v[ss];
			ss++;
		}
	}

	//sort attribute
	var sel_sort = $("<select>")
		.attr("name", "sort_attr").attr("id", "sort_attr").css("font-size", "12px")
		.css("max-width", "100px").css("margin-top", "0px")
		.change(function(){
			var tt = $("#sort_attr option:selected").val(); //attribute name
			var dtt = $("#attr option:selected").val(); //display attribute name
			var ret = [];
			if(tt=="Clustering"){
				ret = qe_prepare_table_clustering(sample_order, gene_order, re, numG, si);
			}else{
				var sam = u[tt]; //a hash: (key: attribute value, value: samples with the attribute)
				ret = qe_prepare_table(sam, re, numG, si);
			}
			var re2 = ret[0];
			dd = ret[1];
		
			var tx = $("#expr_table").detach();
			tx = tx.empty();
			var tmp = qe_draw_sample_label(tx, si, dd, gsms, dtt);

			tx = tmp[0]; 
			var topOffset = tmp[1];
			tx = qe_draw_expression_table(tx, numG, si, gene_order, geneLabel, re2, topOffset);
			$("#zoomin").prepend(tx);
			$("#zoom_level").trigger("change");
		});

	for(var vv=0; vv<attributes.length; vv++){
		sel_sort.append($("<option>").val(attributes[vv]).text(attributes[vv]));
	}
	sel_sort.append($("<option>").val("Clustering").text("H. clustering"));

	//display attributes
	var sel = $("<select>")
		.attr("name", "attr").attr("id", "attr").css("font-size", "12px")
		.css("max-width", "100px").css("margin-top", "0px")
		.change(function(){
			var tt = $("#sort_attr option:selected").val(); //attribute name
			var dtt = $("#attr option:selected").val(); //display attribute name
			var ret = [];
			if(tt=="Clustering"){
				ret = qe_prepare_table_clustering(sample_order, gene_order, re, numG, si);
			}else{
				var sam = u[tt]; //a hash: (key: attribute value, value: samples with the attribute)
				ret = qe_prepare_table(sam, re, numG, si);
			}
			var re2 = ret[0];
			dd = ret[1];
			var tx = $("#expr_table").detach();
			tx = tx.empty();
			var tmp = qe_draw_sample_label(tx, si, dd, gsms, dtt);

			tx = tmp[0]; 
			var topOffset = tmp[1];
			tx = qe_draw_expression_table(tx, numG, si, gene_order, geneLabel, re2, topOffset);

			$("#zoomin").prepend(tx);
			$("#zoom_level").trigger("change");
		});

	for(var vv=0; vv<attributes.length; vv++){
		sel.append($("<option>").val(attributes[vv]).text(attributes[vv]));
	}

	var zoom=$("<select>")
		.attr("name", "zoom_level").attr("id", "zoom_level").css("font-size", "12px")
		.css("margin-top", "0px")
		.change(function(){
			var tt = $("#zoom_level option:selected").text();
			if(tt=="small"){
				for(var jj=0; jj<si; jj++){
					$("#col_" + (jj+1)).parent().css("left",  -30 + jj * 10 + "px");
				}
				$("#expression_table").css("width", 10*si+100 + "px");
				$(".zoom_in_value").each(function(){
					$(this).css("width", "10px");
				});
			}
			else if(tt=="normal"){
				for(var jj=0; jj<si; jj++){
					$("#col_" + (jj+1)).parent().css("left",  -10 + jj * 30 + "px");
				}
				$("#expression_table").css("width", 30*si+100 + "px");
				$(".zoom_in_value").each(function(){
					$(this).css("width", "30px");
				});
			}
		});
	zoom.append($("<option>").text("normal"));
	zoom.append($("<option selected='selected'>").text("small"));

	var windowH = Math.min(600, 325+numG*15);
	var ta = $("<div>")
		.attr("id", "zoomin")
		//.css("width", 100+30*arrays[dset_id].length+"px")
		.css("width", "700px")
		//.css("height", 325+numG*15 + "px")
		.css("height", windowH + "px")
		.css("overflow", "auto")
	;

	var taa = $("<div>")
		.attr("id", "expr_table")
		.css("position", "relative")
	;

	//DEFAULT OPTION==================================
	var default_attr = "Clustering"; //attribute name
	var ret = qe_prepare_table_clustering(sample_order, gene_order, re, numG, si);
	var re2 = ret[0];
	var dd_new = ret[1];
	sel_sort.find('option[value="' + default_attr + '"]').attr("selected", "selected");
	var default_display_attr = "Title"; //display attribute name
	sel.find('option[value="' + default_display_attr + '"]').attr("selected", "selected");
	var tmp  = qe_draw_sample_label(taa, si, dd_new, gsms, default_display_attr);
	taa = tmp[0]; 
	topOffset = tmp[1];
	taa = qe_draw_expression_table(taa, numG, si, gene_order, geneLabel, re2, topOffset);
	ta.append(taa);
	//================================================


	//var gll = "<span style='font-weight:bold;' class='big_font'>" + geneLabel[numG-1] + "</span>";
	var gl = $("<div>").append(
		$("<table>")
			.css("border-spacing", "0px").css("border", "0px").css("padding", "0px").css("margin", "0px")
			.append($("<tr>")
				.append($("<td>").append("Query gene expression profile in dataset "))
				/*.append($("<td>").css("width", "30px")
					.append($("<a>")
						.attr("href", "#").attr("id", "gene_up").attr("title", "Previous gene")
						.click(function(){
							alert("Un-implemented");
							//event_microarray_image(dsetID, Math.max(0, geneID - 1));
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_up.png").css("vertical-align", "middle").addClass("arrow2"))
					)
					.append($("<a>")
						.attr("href", "#").attr("id", "gene_down").attr("title", "Next gene")
						.click(function(){
							alert("Un-implemented");
							//event_microarray_image(dsetID, Math.min(geneNames.length-1, geneID + 1));
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_down.png").css("vertical-align", "middle").addClass("arrow2"))
					)
				)
				.append($("<td>").css("width", "100px").css("text-align", "center")
					.append($("<span>")
						.css("font-weight", "bold").addClass("big_font")
						.append(geneLabel[numG-1])
					)
				)*/
				//.append($("<td>").append(" in dataset "))
				.append($("<td>").css("width", "30px")
					.append($("<a>")
						.attr("href", "#").attr("id", "dset_left").attr("title", "Previous dataset")
						.click(function(){
							var it_id = Math.max(1, parseInt(item_id.split("_")[3]) - 1);
							var new_dataset = $("#dset_item_" + qid + "_" + it_id).attr("did");
							var new_item_id = $("#dset_item_" + qid + "_" + it_id).attr("id");
							process_query_expression(qid, org, sess, new_dataset, genes, new_item_id);
							//alert("Un-implemented");
							//event_microarray_image(Math.max(0, dsetID-1), geneID);
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_left.png").css("vertical-align", "middle").addClass("arrow2"))
					)
					.append($("<a>")
						.attr("href", "#").attr("id", "dset_right").attr("title", "Next dataset")
						.click(function(){
							var max_id = parseInt($("#"+item_id).attr("max_id"));
							var it_id = Math.min(max_id, parseInt(item_id.split("_")[3]) + 1);
							var new_dataset = $("#dset_item_" + qid + "_" + it_id).attr("did");
							var new_item_id = $("#dset_item_" + qid + "_" + it_id).attr("id");
							process_query_expression(qid, org, sess, new_dataset, genes, new_item_id);
							//alert("Un-implemented");
							//event_microarray_image(Math.min(dsetNames.length-1, dsetID+1), geneID);
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_right.png").css("vertical-align", "middle").addClass("arrow2"))
					)
				)
				.append($("<td>").css("width", "200px").css("text-align", "center")
					.append($("<span>")
						.css("font-weight", "bold").addClass("big_font")
						.append(dsetLabel)
					)
				)
			)
		);

	//var coexprl = "<span style=\"font-weight:bold;\" class=\"big_font\">" + coexpr[0] + "</span>";
	var tb = $("<div>")
		.append($("<table>")
			.append($("<tr>")
				.append($("<td>")
					.css("width", "600px")
					.append("<b>Title: " + $("#" + item_id).attr("title") + "</b>")
				)
			)
		)
		//.append("The coexpression score of " + gll + " with the query in this dataset is " + coexprl 
		//	+ "<br>")
		.append($("<table>")
			.css("font-size", "12px")
			.css("color", "black") //only needed for light background
			.append($("<tr>")
				.append($("<td>")
					.css("width", "260px")
					.css("vertical-align", "top")
					.append("Display condition attribute: ")
					.append(sel)
				)
				.append($("<td>")
					.css("width", "220px")
					.css("vertical-align", "top")
					.append("Sort conditions by: ")
					.append(sel_sort)
				)
				.append($("<td>")
					.css("width", "120px")
					.css("vertical-align", "top")
					.append("Zoom: ")
					.append(zoom)
				)
			)
		)
		.append("(Tip: mouse-over a condition label)");
	tb.append(ta);
	
	return [tb, gl];
}

