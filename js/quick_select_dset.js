$("#dset_choice").change(function(){
	var tt = $("#dset_choice option:selected").attr("value");
	var cat = "";

	if(tt.indexOf("---")==0){
		return false;
	}
	if(tt=="more"){
		$(".select_dataset").trigger("click");
		return false;
	}
	if(organism=="worm"){
		if(tt=="common"){
			cat = "category;(Category): Multi-tissue/Commonly weighted";
		}else if(tt=="cv_custom_all"){
			cat = "all";
		}else if(tt=="all"){
			cat = "all";
		}else if(tt=="adult"){
			cat = "category;Adult";
		}else if(tt=="larva"){
			cat = "category;Larva";
		}else if(tt=="embryo"){
			cat = "category;Embryo";
		}else if(tt=="young-adult"){
			cat = "category;Young Adult";
		}
	}
	else if(organism=="mouse"){
		if(tt=="common"){
			cat = "category;(Category): Multi-tissue/Commonly weighted";
		}else if(tt=="all"){
			cat = "all";
		}else if(tt=="cv_custom_all"){
			cat = "all";
		}else{
			cat = "category;" + tt;
		}
	}
	else if(organism=="fly"){
		if(tt=="common"){
			cat = "category;(Category): Multi-tissue/Commonly weighted";
		}else if(tt=="cv_custom_all"){
			cat = "all";
		}else if(tt=="all"){
			cat = "all";
		}else{
			cat = "category;" + tt;
		}
	}
	else if(organism=="yeast"){
		if(tt=="common"){
			cat = "category;(Category): Commonly weighted";
		}else if(tt=="cv_custom_all"){
			cat = "all";
		}else if(tt=="all"){
			cat = "all";
		}else{
			cat = "category;" + tt;
		}
	}
	else if(organism=="zebrafish"){
		if(tt=="common"){
			cat = "category;(Category): Commonly weighted";
		}else if(tt=="cv_custom_all"){
			cat = "all";
		}else if(tt=="all"){
			cat = "all";
		}else{
			cat = "category;" + tt;
		}
	}
	/* Old */
	/*
	if(tt=="all"){
		cat = "all";
	}else if(tt=="multi-tissue"){
		cat = "category;(Category): Multiple Tissue Profiling";
	}else if(tt=="cancer"){
		cat = "category;(Category): Cancer";
	}else if(tt=="non-cancer"){
		cat = "category;(Category): Non-Cancer";
	}else if(tt=="disease-state"){
		cat = "category;(Variation): Disease State Variation";
	}else if(tt=="treatment"){
		cat = "category;(Variation): Treatment Variation";
	}
	else{
		var tis = ["blood", "brain", "breast", "bronchus", "colon", "gingival", "gastric", "intestine", "kidney", 
			"liver", "lung", "muscle", "pancreas", "prostate", "ovary", "skin"];

		var cor_tis = ["Blood", "Brain", "Breast/Mammary", "Bronchial/Esophageal/Nasopharyngeal", 
			"Colon/Colorectal", "Oral/Gingival", 
			"Stomach/Gastric/Gastrointestinal", 
			"Intestine", "Kidney/Renal", "Liver/Hepatocellular", 
			"Lung", "Muscle", "Pancreas/Pancreatic", "Prostate", "Ovary/Ovarian", "Skin"];

		for(var ti=0; ti<tis.length; ti++){
			if(tt.indexOf(tis[ti])==0){
				if(tt.indexOf("_non_cancer")!=-1){
					cat = "category;" + cor_tis[ti] + " (Non-cancer)";
				}else if(tt.indexOf("_cancer")!=-1){
					cat = "category;" + cor_tis[ti] + " (Cancer)";
				}else if(tt.indexOf("_cell_line")!=-1){
					cat = "category;" + cor_tis[ti] + " (Cell line)";
				}else if(tt.indexOf("_tumor")!=-1){
					cat = "category;" + cor_tis[ti] + " (Tumor)";
				}
			}
		}
	}
	*/

	$("#dataset_result").html("");
	$("#search_result").html("");
	$("#vis_result").hide();
	$("#status_percentage").text("33%");				
	$("#status_text").text("Looking for datasets...");
	$("#status_text").text("Performing search...");
	$("#statusdiv").qtip("option", {"content.text": $("#statusdiv")});
	$("#statusdiv").qtip("api").set("content.title.text", $("#status_message"));
	$("#statusdiv").trigger("click");
		
	$("#status_percentage").text("66%");				

	var queryUpper = $("#query_genes").attr("value").toUpperCase();
	queryUpper = queryUpper.replace(/;/g, " ").replace(/,/g, " ")
		.replace(/"/g, " ").replace(/\s{2,}/g, " ");
	queryUpper = $.trim(queryUpper);

	var d = new Date();
	var n = d.getTime();
	sessionID = n.toString();

	sessionSetDefault("search_alg", "RBP");
	sessionSetDefault("search_distance", "ZscoreHubbinessCorrected");
	sessionSetDefault("rbp_p", "0.99");
	sessionSetDefault("percent_query", "0.5");
	sessionSetDefault("percent_genome", "0.5");
	sessionSetDefault("pval_filter", "-1");
		
	var queryLength = queryUpper.split(" ").length;
	var dset = cat;
	var alg = sessionStorage.getItem("search_alg");
	var guide_dset = "none";

	if(queryLength==1){
		alg = "EqualWeighting";
	}
	if(cat=="category;(Category): Multi-tissue/Commonly weighted"){
		alg = "EqualWeighting";
	}
	if(tt=="cv_custom_all"){
		alg = "CV_CUSTOM";
		guide_dset = "FN";
	}

	$.get("../servlet/GetSearchMessage", {organism:organism, param:"status", sessionID:sessionID}, function(resT){ //function is solely to create a new session
		$.post("../servlet/DoSearch2", {organism:organism, sessionID:sessionID, dset:dset,
		correlation_sign:"positive", 
		//check_dset_size:"true",
		guide_dset:guide_dset,
		query:queryUpper, query_mode:"gene_symbol", 
		search_alg:alg, 
		search_distance:sessionStorage.getItem("search_distance"),
		rbp_p:sessionStorage.getItem("rbp_p"), 
		percent_query:sessionStorage.getItem("percent_query"), 
		percent_genome:sessionStorage.getItem("percent_genome")}, 
		function(responseText){}
		);
	});

	setTimeout(poll_progress, 1000);
	return false;
	
});

$(document).ready(function(){
	var tt = "---";

	/*
	var tis = ["blood", "brain", "breast", "bronchus", "colon", "gingival", "gastric", "intestine", "kidney", 
		"liver", "lung", "muscle", "pancreas", "prostate", "ovary", "skin"];

	var tx = $("<option>").attr("value", tis[0] + "_cancer").text(tis[0] + " (cancer)");
	for(var ti=0; ti<tis.length; ti++){
		var ttx = tis[ti];
		if(ti>0){
			tx = tx.after($("<option>").attr("value", ttx + "_cancer").text(ttx + " (cancer)"));
		}
		tx = tx.after($("<option>").attr("value", ttx + "_non_cancer").text(ttx + " (noncancer)"))
		.after($("<option>").attr("value", ttx + "_cell_line").text(ttx + " (cell line)"))
		.after($("<option>").attr("value", ttx + "_tumor").text(ttx + " (tumor)"));
	}

	$("#dset_choice").find('option[value="---single-tissue"]')
	.after(tx);
	*/
	/*
	.after($("<option>").attr("value", "brain_cancer").text("brain (cancer)")
		.after($("<option>").attr("value", "brain_non_cancer").text("brain (noncancer)"))
		.after($("<option>").attr("value", "brain_cell_line").text("brain (cell line)"))
		.after($("<option>").attr("value", "brain_tumor").text("brain (tumor)"))
		.after($("<option>").attr("value", "breast").text("breast"))

	);*/


	$("#dset_choice").find('option[value="' + tt + '"]').attr("selected", "selected");
});
