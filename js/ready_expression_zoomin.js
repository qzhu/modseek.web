/* requires viewer_common.js imported before this */
function event_microarray_image(dsetID, geneID){
	var geneQuery = [];
	geneQuery.push(geneNames[geneID]);

	var geneLabel = [];
	for(var vi=0; vi<queryNames.length; vi++){
		geneLabel.push(queryHGNCNames[vi]);
	}
	geneLabel.push(geneHGNCNames[geneID]);
	$.post("../servlet/GetExpressionServlet", {organism:organism,
	normalize:toNormalize, enable_query:"1", sessionID:sessionID, 
	dataset:dsetNames[dsetID], gene:geneQuery.join("+"), 
	query:queryNames.join("+"), rbp_p:sessionStorage.getItem("rbp_p")}, 
	function(responseText){
		var dd = dsetNames[dsetID].split(".");
		if(dd.length==1){
			dd.push("RNASEQ");
		}else if(dd[1].indexOf("GPL")==-1){
			dd[1] = "RNASEQ";
		}
		var tx = process_response_nowrap(responseText, dsetID, geneLabel, dd[0] + "(" + dd[1] + ")", dsetID, geneID);
		if(tx.length==1){
			setTimeout(function(){
				event_microarray_image(dsetID, geneID);
			}, 200);
			return false;
		}
		var t = tx[1];
		t = t.after(tx[0]);
		var tit = "Expression profile";	
		
		$("#somediv").qtip("option", {"content.text": t});
		$("#somediv").qtip("api").set("content.title.text", tit);
		$("#somediv").trigger("click");

		setTimeout(function(){
			activate_external_link();
		}, 500);
	});	
}

function add_attribute(u, thisVal, nextVal, jj){
	if(u.hasOwnProperty(thisVal)){
		if(!u[thisVal].hasOwnProperty(nextVal)){
			u[thisVal][nextVal] = [];
		}
		u[thisVal][nextVal].push(jj);
		//continue;
	}else{
		u[thisVal] = {};
		u[thisVal][nextVal] = [];
		u[thisVal][nextVal].push(jj);
	}
	return u;
}

function push_attribute(attributes, u, thisVal){
	if(u.hasOwnProperty(thisVal)){
		//do nothing
	}else{
		attributes.push(thisVal);
	}
	return attributes;
}

function group_dataset_attributes_2(dset_id){
	var gsms = arrays[dset_id];
	var attributes = [], u = {};
	for(var jj=0; jj<gsms.length; jj++){ //samples
		var s = gsms[jj].split("|"); 
		var twoChannel = false;
		if(gsms[jj].indexOf("Channel 2")!=-1){
			twoChannel = true;
		}
		if(twoChannel){
			if(organism=="wormbase"){
				var thisVal, nextVal;
				thisVal = "Title";
				nextVal = s[0].split(":", 2)[1];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				var chan = "";
				for(var jk=1; jk<s.length; jk++){
					if(s[jk]=="Channel 1" || s[jk]=="Channel 2"){
						if(s[jk]=="Channel 1"){
							chan = "1"; 
						}else{
							chan = "2";
						}
						jk++;
						thisVal = "Ch" + chan + " Title";
						nextVal = s[jk];
						push_attribute(attributes, u, thisVal);
						u = add_attribute(u, thisVal, nextVal, jj);
						jk++;
						continue;
					}
					var sx = s[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Ch" + chan + " Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Ch" + chan + " Attribute: " + sx[0];
						nextVal = sx[1];
					}
					push_attribute(attributes, u, thisVal);
					u = add_attribute(u, thisVal, nextVal, jj);
				}
			}
			else{
				var thisVal, nextVal;
				thisVal = "GSM ID";
				nextVal = s[0];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				thisVal = "Title";
				nextVal = s[1];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				var chan = "";
				for(var jk=2; jk<s.length; jk++){
					if(s[jk]=="Channel 1" || s[jk]=="Channel 2"){
						if(s[jk]=="Channel 1"){
							chan = "1"; 
						}else{
							chan = "2";
						}
						jk++;
						thisVal = "Ch" + chan + " Title";
						nextVal = s[jk];
						push_attribute(attributes, u, thisVal);
						u = add_attribute(u, thisVal, nextVal, jj);
						jk++;
						continue;
					}
					var sx = s[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Ch" + chan + " Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Ch" + chan + " Attribute: " + sx[0];
						nextVal = sx[1];
					}
					push_attribute(attributes, u, thisVal);
					u = add_attribute(u, thisVal, nextVal, jj);
				}
			}

		}else{
			if(organism=="wormbase"){
				var thisVal, nextVal;
				thisVal = "Sample ID";
				nextVal = s[0].split(":", 2)[1];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				thisVal = "Title";
				nextVal = s[1];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				for(var jk=2; jk<s.length; jk++){
					var sx = s[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Attribute: " + sx[0];
						nextVal = sx[1];
					}
					push_attribute(attributes, u, thisVal);
					u = add_attribute(u, thisVal, nextVal, jj);
				}
			}else{
				var thisVal, nextVal;
				thisVal = "GSM ID";
				nextVal = s[0];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				thisVal = "Title";
				nextVal = s[1];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				thisVal = "Source name";
				nextVal = s[2];
				push_attribute(attributes, u, thisVal);
				u = add_attribute(u, thisVal, nextVal, jj);
				for(var jk=3; jk<s.length; jk++){
					var sx = s[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Attribute: " + sx[0];
						nextVal = sx[1];
					}
					push_attribute(attributes, u, thisVal);
					u = add_attribute(u, thisVal, nextVal, jj);
				}
			}	
		}
		/*
		for(var jk=0; jk<s.length; jk++){ //characteristics in a sample
			var sx = s[jk].split(":", 2); //maximum 2 values in sx
			var thisVal;
			var nextVal;
			if(jk==0){
				thisVal = "GSM ID"; //first line is GSM id
				nextVal = sx[0];
				if(organism=="wormbase"){ //unless it is wormbase
					thisVal = "Title";
					nextVal = sx[1];
				}
			}else if(sx.length==1){ //else if it does not have a ":"
				thisVal = "Line " + jk;
				if(thisVal=="Line 1"){
					thisVal="Sample ID";
				}else if(thisVal=="Line 2"){
					thisVal="Title";
				}
				nextVal = sx[0];
			}else{
				thisVal = "Attribute: " + sx[0];
				nextVal = sx[1];
			}
			push_attribute(attributes, u, thisVal);
			u = add_attribute(u, thisVal, nextVal, jj);
		}*/
	}
	return [attributes, u];
}

function group_dataset_attributes(dset_id){
	/* Grouping attributes for a dataset */
	var gsms = arrays[dset_id];
	if(gsms[arrays[dset_id].length - 1]=="uninitialized"){
		$.get("../servlet/GetDescription", {organism:organism, dset:dsetNames[dset_id], 
		gene:geneNames.join(","), needSample:"true"}, function(responseText){
			var sp = responseText.split("&&");
			var gsmArray = sp[2].split(";;");
			if(gsmArray.length!=dsetSizes[dset_id]){
				for(var j=0; j<dsetSizes[dset_id]; j++)
					arrays[dset_id][j] = "NA or array not found";
			}else{
				for(var j=0; j<dsetSizes[dset_id]; j++)
					arrays[dset_id][j] = gsmArray[j];
			}
		});
		return ["loading..."];
	}else{
		var xx = group_dataset_attributes_2(dset_id);
		var attributes = xx[0];
		var u = xx[1];
		return [attributes,u];
	}
}

function draw_expression_table(tx, numG, numS, geneLabel, re, topOffset){
	var size = color_red.length;
	var unit_length = (up_clip - down_clip) / size;
	var cell_width = 10;

	var tt = $("<table>")
		.attr("id", "expression_table")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.css("width", 100+cell_width*numS+"px")
		.css("height", numG*15+"px")
		.css("position", "absolute")
		.css("top", 280 - 100 + topOffset + "px")
	;

	for(var vg=0; vg<numG; vg++){
		if(vg==numG-1){
			tt.append($("<tr><td style=\"line-height:5px;\">&nbsp;</td></tr>"));
		}
		tt.append($("<tr>")
			.css("color", "rgb(128, 128, 128)")
			.addClass("small_font"));
		if(vg!=numG-1){
			tt.find("tr:last").append($("<td>")
				.addClass("small_font")
				.addClass("expression_query")
				.append(geneLabel[vg]));
		}else{
			tt.find("tr:last").append($("<td>")
				.addClass("big_font")
				.addClass("expression_gene")
				.append(geneLabel[vg]));
		}


		for(var i=0; i<numS; i++){
			var vi = parseFloat(re[vg][i]);
			var c = judge_color(vi, up_clip, down_clip, size, unit_length);
			var r = c[0];
			var g = c[1];
			var b = c[2];
			var isAbsent = c[3];
			if(isAbsent==true){
				tt.find("tr:last")
					.append($("<td>")
						.attr("colspan", numS)
						.css("color", "rgb(0,0,0)") //only needed for light background
						.text("Not present")
					);
				break;
			}
			tt.find("tr:last")
				.append($("<td>")
					.addClass("zoom_in_value")
					.css("width", cell_width + "px")
					//.attr("nowrap", "nowrap")
					.css("background-color", "rgb("+r+", "+g+", "+b+")")
					//.text(re[vg][i])
				);
		}
	}
	tx.append(tt);
	return tx;
}

function draw_sample_label(tx, numS, dd, labels, attr_name){ //dd is sample order
	var gsmid = [], lab2 = [], lab1 = [];
	var maxLength = -1;
	for(var jj=0; jj<numS; jj++){ //samples
		j = dd[jj];
		var lab = new String(labels[j]);
		var lab_split = lab.split("|");
		
		var twoChannel = false;
		if(lab.indexOf("Channel 2")!=-1){
			twoChannel = true;
		}

		var thisVal, nextVal;
		var jk;
		if(twoChannel){
			if(organism=="wormbase"){
				var chan = "";
				for(jk=0; jk<lab_split.length; jk++){
					if(jk==0){
						thisVal = "Title";
						nextVal = lab_split[0].split(":", 2)[1];
						if(thisVal==attr_name){
							break;
						}
					}
					if(lab_split[jk]=="Channel 1" || lab_split[jk]=="Channel 2"){
						if(lab_split[jk]=="Channel 1"){
							chan = "1"; 
						}else{
							chan = "2";
						}
						jk++;
						thisVal = "Ch" + chan + " Title";
						nextVal = lab_split[jk];
						if(thisVal==attr_name){
							break;
						}
						jk++;
						continue;
					}
					var sx = lab_split[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Ch" + chan + " Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Ch" + chan + " Attribute: " + sx[0];
						nextVal = sx[1];
					}
					if(thisVal==attr_name){
						break;
					}
				}
			}
			else{
				var chan = "";
				for(jk=0; jk<lab_split.length; jk++){
					if(jk==0){
						thisVal = "GSM ID";
						nextVal = lab_split[0];
						if(thisVal==attr_name){
							break;
						}
					}else if(jk==1){
						thisVal = "Title";
						nextVal = lab_split[1];
						if(thisVal==attr_name){
							break;
						}
					}
					if(lab_split[jk]=="Channel 1" || lab_split[jk]=="Channel 2"){
						if(lab_split[jk]=="Channel 1"){
							chan = "1"; 
						}else{
							chan = "2";
						}
						jk++;
						thisVal = "Ch" + chan + " Title";
						nextVal = lab_split[jk];
						if(thisVal==attr_name){
							break;
						}
						jk++;
						continue;
					}
					var sx = lab_split[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Ch" + chan + " Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Ch" + chan + " Attribute: " + sx[0];
						nextVal = sx[1];
					}
					if(thisVal==attr_name){
						break;
					}
				}
			}

			if(lab_split.length>0 && lab_split.length<=2){
			}else if(lab_split.length==0){
				nextVal = "...";
			}else if(jk==lab_split.length){
				nextVal = "...";
			}
		}
		else{
			if(organism=="wormbase"){
				for(jk=0; jk<lab_split.length; jk++){
					if(jk==0){
						thisVal = "Sample ID";
						nextVal = lab_split[0].split(":", 2)[1];
						if(thisVal==attr_name){
							break;
						}
					}else if(jk==1){
						thisVal = "Title";
						nextVal = lab_split[1];
						if(thisVal==attr_name){
							break;
						}
					}
					var sx = lab_split[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Attribute: " + sx[0];
						nextVal = sx[1];
					}
					if(thisVal==attr_name){
						break;
					}
				}
			}else{
				for(jk=0; jk<lab_split.length; jk++){
					if(jk==0){
						thisVal = "GSM ID";
						nextVal = lab_split[0];
						if(thisVal==attr_name){
							break;
						}
					}else if(jk==1){
						thisVal = "Title";
						nextVal = lab_split[1];
						if(thisVal==attr_name){
							break;
						}
					}else if(jk==2){
						thisVal = "Source name";
						nextVal = lab_split[2];
						if(thisVal==attr_name){
							break;
						}
					}	
					var sx = lab_split[jk].split(":", 2); //maximum 2 values in sx
					if(sx.length==1){
						thisVal = "Line " + jk;
						nextVal = sx[0];
					}else{
						thisVal = "Attribute: " + sx[0];
						nextVal = sx[1];
					}
					if(thisVal==attr_name){
						break;
					}
				}
			}

			if(lab_split.length>0 && lab_split.length<=2){
			}else if(lab_split.length==0){
				nextVal = "...";
			}else if(jk==lab_split.length){
				nextVal = "...";
			}
		}

		lab1.push(nextVal);
		if(nextVal.length>maxLength){
			maxLength = nextVal.length;
		}
		lab2.push(lab_split.join("<br>"));

	}
		/*				
			}
		}
		gsmid.push(lab_split[0]);
		lab = lab.substring(lab.indexOf("|")+1);
		var thisVal, nextVal;



		var vi = 0;
		for(vi=0; vi<lab_split.length; vi++){
			var sx = lab_split[vi].split(":", 2);
			if(vi==0){
				thisVal = "GSM ID";
				nextVal = sx[0];
				if(organism=="wormbase"){
					nextVal = sx[1];
				}
			}else if(sx.length==1){
				thisVal = "Line " + vi;
				if(thisVal=="Line 1"){
					thisVal="Sample ID";
				}else if(thisVal=="Line 2"){
					thisVal="Title";
				}
				nextVal = sx[0];
			}else{
				thisVal = "Attribute: " + sx[0];
				nextVal = sx[1];
			}
			if(thisVal==attr_name){
				break;
			}
		}
		if(lab_split.length>0 && lab_split.length<=2){
		}else if(lab_split.length==0){
			nextVal = "...";
		}else if(vi==lab_split.length){
			nextVal = "...";
		}
		lab1.push(nextVal);
		if(nextVal.length>maxLength){
			maxLength = nextVal.length;
		}
		lab2.push(lab_split.join("<br>"));
	}
	*/
	var ll = Math.min(50, maxLength);
	var le = -100;
	if(ll<10){
		le = -100;
	}else if(ll<20){
		le = -50;
	}else if(ll<30){
		le = 0;
	}else if(ll<40){
		le = 50;
	}else{
		le = 100;
	}

	for(var jj=0; jj<numS; jj++){
		//requires lab2, gsmid, jj, lab1
		//attaching the labels the samples===========================
		tx.append($("<div>")
			.addClass("rotate_270")
			.css("height", "50px")
			.css("width", "300px")
			.css("position", "absolute")
			//.css("top", "175px")
			.css("top", le + "px")
			//.css("left", -10 + jj * 30 + "px")
			.css("left",  -30 + jj * 10 + "px")
			.append($("<a>")
				.attr("rel", "external")
				.attr("href", "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=" + gsmid[jj])
				.addClass("small_font")
				.attr("id", "col_" + (jj+1))
				.attr("title", lab2[jj])
				.text(lab1[jj].substring(0, 50))
				//.text(lab.substring(0, 50))
				//.text(lab.substring(0, 20))
				.qtip({position: {my:"top left", at: "bottom left", adjust: {x: 20,},}, style:{classes: "ui-tooltip-gsm", }, })
				)
			);
	}
	return [tx, le];
}

function prepare_table_clustering(order, re, numG, si){
	var xi = 0;
	var re2 = new Array(numG);
	for(var vi=0; vi<numG; vi++){
		re2[vi] = new Array(si);
	}
	for(var vi=0; vi<numG; vi++){
		for(var vj=0; vj<si; vj++){
			re2[vi][vj] = re[vi][order[vj]];
		}
	}
	return [re2, order];
}

function prepare_table(sam, re, numG, si){
	var xi = 0;
	var isVisited = {};
	var dd = [];
	for(var i=0; i<si; i++){
		dd.push(i);
	}

	for(var kk in sam){ //for each attribute value
		for(var kj=0; kj<sam[kk].length; kj++){ //for each sample annotated to that attr. value
			dd[xi] = sam[kk][kj]; //let the current item be this sample
			isVisited[dd[xi]] = 1; //mark this item visited
			xi++;
		}
	}
	for(var kj=0; kj<si; kj++){
		if(!isVisited.hasOwnProperty(kj)){ //for those unvisited samples
			dd[xi] = kj; //mark them as visited one by one
			xi++;
		}
	}

	//new table
	var re2 = new Array(numG);
	for(var vi=0; vi<numG; vi++){
		re2[vi] = new Array(si);
	}
	for(var vi=0; vi<numG; vi++){
		for(var vj=0; vj<si; vj++){
			re2[vi][vj] = re[vi][dd[vj]];
		}
	}

	return [re2, dd];
}

function process_response_nowrap(responseText, dset_id, geneLabel, dsetLabel, dsetID, geneID){
	var ss = responseText.split("\n");
	var dd = [];
	for(var i=0; i<sample_order[dsetID].length; i++){
		dd.push(sample_order[dsetID][i]);
	}

	var v = ss[0].split(","); //query
	var w = ss[1].split(","); //gene
	var coexpr = ss[2].split(","); //coexpr score
	var xx = group_dataset_attributes(dset_id);
	if(xx.length==1){ //should expect two return parameters
		return [false];
	}

	var attributes = xx[0];
	var u = xx[1];
	//add the gene
	for(var i=0; i<w.length; i++){
		v.push(w[i]);
	}

	//get table size
	var numG = geneLabel.length;
	var si = arrays[dset_id].length;

	//prepare table entry - re
	var re = new Array(numG);
	for(var vi=0; vi<numG; vi++){
		re[vi] = new Array(si);
	}

	var ss = 0;
	for(var vi=0; vi<numG; vi++){
		for(var vj=0; vj<si; vj++){
			re[vi][vj] = v[ss];
			ss++;
		}
	}

	//sort attribute
	var sel_sort = $("<select>")
		.attr("name", "sort_attr").attr("id", "sort_attr").css("font-size", "12px")
		.css("max-width", "100px").css("margin-top", "0px")
		.change(function(){
			var tt = $("#sort_attr option:selected").val(); //attribute name
			var dtt = $("#attr option:selected").val(); //display attribute name
			var ret = [];
			if(tt=="Clustering"){
				ret = prepare_table_clustering(sample_order[dset_id], re, numG, si);
			}else{
				var sam = u[tt]; //a hash: (key: attribute value, value: samples with the attribute)
				ret = prepare_table(sam, re, numG, si);
			}
			var re2 = ret[0];
			dd = ret[1];
		
			var tx = $("#expr_table").detach();
			tx = tx.empty();
			var tmp = draw_sample_label(tx, si, dd, arrays[dset_id], dtt);

			tx = tmp[0]; 
			var topOffset = tmp[1];
			tx = draw_expression_table(tx, numG, si, geneLabel, re2, topOffset);
			$("#zoomin").prepend(tx);
			$("#zoom_level").trigger("change");
		});

	for(var vv=0; vv<attributes.length; vv++){
		sel_sort.append($("<option>").val(attributes[vv]).text(attributes[vv]));
	}
	sel_sort.append($("<option>").val("Clustering").text("H. clustering"));

	//display attributes
	var sel = $("<select>")
		.attr("name", "attr").attr("id", "attr").css("font-size", "12px")
		.css("max-width", "100px").css("margin-top", "0px")
		.change(function(){
			var tt = $("#sort_attr option:selected").val(); //attribute name
			var dtt = $("#attr option:selected").val(); //display attribute name
			var ret = [];
			if(tt=="Clustering"){
				ret = prepare_table_clustering(sample_order[dset_id], re, numG, si);
			}else{
				var sam = u[tt]; //a hash: (key: attribute value, value: samples with the attribute)
				ret = prepare_table(sam, re, numG, si);
			}
			var re2 = ret[0];
			dd = ret[1];
			var tx = $("#expr_table").detach();
			tx = tx.empty();
			var tmp = draw_sample_label(tx, si, dd, arrays[dset_id], dtt);

			tx = tmp[0]; 
			var topOffset = tmp[1];
			tx = draw_expression_table(tx, numG, si, geneLabel, re2, topOffset);

			$("#zoomin").prepend(tx);
			$("#zoom_level").trigger("change");
		});

	for(var vv=0; vv<attributes.length; vv++){
		sel.append($("<option>").val(attributes[vv]).text(attributes[vv]));
	}

	var zoom=$("<select>")
		.attr("name", "zoom_level").attr("id", "zoom_level").css("font-size", "12px")
		.css("margin-top", "0px")
		.change(function(){
			var tt = $("#zoom_level option:selected").text();
			if(tt=="small"){
				for(var jj=0; jj<si; jj++){
					$("#col_" + (jj+1)).parent().css("left",  -30 + jj * 10 + "px");
				}
				$("#expression_table").css("width", 10*si+100 + "px");
				$(".zoom_in_value").each(function(){
					$(this).css("width", "10px");
				});
			}
			else if(tt=="normal"){
				for(var jj=0; jj<si; jj++){
					$("#col_" + (jj+1)).parent().css("left",  -10 + jj * 30 + "px");
				}
				$("#expression_table").css("width", 30*si+100 + "px");
				$(".zoom_in_value").each(function(){
					$(this).css("width", "30px");
				});
			}
		});
	zoom.append($("<option>").text("normal"));
	zoom.append($("<option selected='selected'>").text("small"));

	var windowH = Math.min(600, 325+numG*15);
	var ta = $("<div>")
		.attr("id", "zoomin")
		//.css("width", 100+30*arrays[dset_id].length+"px")
		.css("width", "700px")
		//.css("height", 325+numG*15 + "px")
		.css("height", windowH + "px")
		.css("overflow", "auto")
	;

	var taa = $("<div>")
		.attr("id", "expr_table")
		.css("position", "relative")
	;

	//DEFAULT OPTION==================================
	var default_attr = "Clustering"; //attribute name
	var ret = prepare_table_clustering(sample_order[dset_id], re, numG, si);
	var re2 = ret[0];
	var dd_new = ret[1];
	sel_sort.find('option[value="' + default_attr + '"]').attr("selected", "selected");
	var default_display_attr = "Title"; //display attribute name
	sel.find('option[value="' + default_display_attr + '"]').attr("selected", "selected");
	var tmp  = draw_sample_label(taa, si, dd_new, arrays[dset_id], default_display_attr);
	taa = tmp[0]; 
	topOffset = tmp[1];
	taa = draw_expression_table(taa, numG, si, geneLabel, re2, topOffset);
	ta.append(taa);
	//================================================


	var gll = "<span style='font-weight:bold;' class='big_font'>" + geneLabel[numG-1] + "</span>";
	var gl = $("<div>").append(
		$("<table>")
			.css("border-spacing", "0px").css("border", "0px").css("padding", "0px").css("margin", "0px")
			.append($("<tr>")
				.append($("<td>").append("You have clicked on the expression profile of gene "))
				.append($("<td>").css("width", "30px")
					.append($("<a>")
						.attr("href", "#").attr("id", "gene_up").attr("title", "Previous gene")
						.click(function(){
							event_microarray_image(dsetID, Math.max(0, geneID - 1));
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_up.png").css("vertical-align", "middle").addClass("arrow2"))
					)
					.append($("<a>")
						.attr("href", "#").attr("id", "gene_down").attr("title", "Next gene")
						.click(function(){
							event_microarray_image(dsetID, Math.min(geneNames.length-1, geneID + 1));
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_down.png").css("vertical-align", "middle").addClass("arrow2"))
					)
				)
				.append($("<td>").css("width", "100px").css("text-align", "center")
					.append($("<span>")
						.css("font-weight", "bold").addClass("big_font")
						.append(geneLabel[numG-1])
					)
				)
				.append($("<td>").append(" in dataset "))
				.append($("<td>").css("width", "30px")
					.append($("<a>")
						.attr("href", "#").attr("id", "dset_left").attr("title", "Previous dataset")
						.click(function(){
							event_microarray_image(Math.max(0, dsetID-1), geneID);
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_left.png").css("vertical-align", "middle").addClass("arrow2"))
					)
					.append($("<a>")
						.attr("href", "#").attr("id", "dset_right").attr("title", "Next dataset")
						.click(function(){
							event_microarray_image(Math.min(dsetNames.length-1, dsetID+1), geneID);
							return false;
						})
						.append($("<img>").attr("src", "../img/arrow_right.png").css("vertical-align", "middle").addClass("arrow2"))
					)
				)
				.append($("<td>").css("width", "200px").css("text-align", "center")
					.append($("<span>")
						.css("font-weight", "bold").addClass("big_font")
						.append(dsetLabel)
					)
				)
			)
		);

	var coexprl = "<span style=\"font-weight:bold;\" class=\"big_font\">" + coexpr[0] + "</span>";
	var tb = $("<div>")
		.append("The coexpression score of " + gll + " with the query in this dataset is " + coexprl 
			+ "<br>")
		.append($("<table>")
			.css("font-size", "12px")
			.css("color", "black") //only needed for light background
			.append($("<tr>")
				.append($("<td>")
					.css("width", "260px")
					.css("vertical-align", "top")
					.append("Display condition attribute: ")
					.append(sel)
				)
				.append($("<td>")
					.css("width", "220px")
					.css("vertical-align", "top")
					.append("Sort conditions by: ")
					.append(sel_sort)
				)
				.append($("<td>")
					.css("width", "120px")
					.css("vertical-align", "top")
					.append("Zoom: ")
					.append(zoom)
				)
			)
		)
		.append("(Tip: mouse-over a condition label)");
	tb.append(ta);
	
	return [tb, gl];
}

var current_image_coord_X = -1;
var current_image_coord_Y = -1;

var current_gene_tooltip = -1;
var current_dataset_tooltip = -1;

$(document).ready(function(){
/* Must be after viewer_ready.js has been imported before */
$("#image_coord").click(function(e){
	var dsetID=getOffsetX("#image_coord", e.pageX, dsetNames.length);
	var geneID=getOffsetY("#image_coord", e.pageY, geneNames.length);
	if(pageMode=="expression"){
		event_microarray_image(dsetID, geneID);
	}
	return false;
});

$("#query_microarray").click(function(e){
	var dset_ID=getOffsetX("#query_microarray", e.pageX, dsetNames.length);
	var gene_ID=getOffsetY("#query_microarray", e.pageY, queryNames.length);

	$.post("../servlet/GetExpressionServlet", {organism:organism, 
	normalize:toNormalize, sessionID:sessionID, dataset:dsetNames[dset_ID], 
	gene:queryNames[gene_ID], rbp_p:sessionStorage.getItem("rbp_p")}, function(responseText){
		var t = process_response_nowrap(responseText, dset_ID, [queryNames[gene_ID]], dsetID, geneID);		
		var dd = dsetNames[dset_ID].split(".");
		var tit = "<font style=\"font-family:Arial; font-size:12px;\">"  + roman[dset_ID] + ". " + 
			dd[0] + " (" + dd[1] + ") " + queryHGNCNames[gene_ID] + "</font>";
		
		$("#somediv").qtip("option", {"content.text": t});
		$("#somediv").qtip("api").set("content.title.text", tit);
		$("#somediv").trigger("click");

		setTimeout(function(){
			activate_external_link();
		}, 500);
	});
	return false;
});

$("#header_coord").click(function(e){
	var dsetID=getOffsetX("#header_coord", e.pageX, dsetNames.length);
	var i = dsetID;

	$.get("../servlet/GetDescription", {organism:organism, 
	dset:dsetNames[i],gene:geneNames.join(","),needSample:"true"},
	function(responseText){
		var sp = responseText.split("&&");
		var gsmArray = sp[2].split(";;");
		if(gsmArray.length!=dsetSizes[i]){
			for(var j=0; j<dsetSizes[i]; j++){
				arrays[i][j] = "NA or array not found";
			}
		}else{
			for(var j=0; j<dsetSizes[i]; j++){
				arrays[i][j] = gsmArray[j];
			}
		}	

		var lp = dsetPubmed[i].split(" | ");
		var lp_id = lp[0].replace("PMID: ", "");
		var linked_pubmed = "";
		if(lp_id=="Unpublished"){
			linked_pubmed = "PMID: " + lp_id + " | " + lp[1];
		}else{
			linked_pubmed = "PMID: " + "<a rel='external' href='http://www.ncbi.nlm.nih.gov/pubmed/" + lp_id
			+ "'>" + lp_id + "</a> | " + lp[1];
		}

		var di = $("<div>")
			.attr("id", "desc")
			.css("line-height", "18px")
			.append("<span class='basic_font'>" + dsetDescriptionOneLine[i] + "</span>")
			.append("<br>")
			.append("<span class='basic_font'><b>" + linked_pubmed + "</b></span>")
			.append("<br>")
			.append($("<span>").css("font-size", "14px").css("font-weight", "bold").text("Description "))
			.append($("<a>")
				.attr("href", "#")
				.attr("id", "desc_label")
				.click(function(){
					if($(this).text()=="[ + ]"){
						$.get("../servlet/GetDescription", {organism:organism,
						dset:dsetNames[i],dsetLong:"true"},
						function(responseText){
							$("#desc_label").text("[ - ]");
							var dd = responseText.split(";;");
							dd.splice(0,3);
							$("#desc_text").html(dd.join("<br>") + "<br>");
							return false;
						});
					}else{
						$(this).text("[ + ]");
						$("#desc_text").html("");
						return false;
					}
				})
				.append($("<span>").css("font-size", "10px").text("[ + ]")))
			.append("<br>")
			.append($("<span>").attr("id", "desc_text").addClass("basic_font").html(""))
		;

		var wi = $("<span>").css("font-size", "14px").css("font-weight", "bold")
			.append("Dataset's query-relevance weight ")
			.append($("<span>")
				.addClass("seek-help")
				.append("&nbsp;?&nbsp;")
				.attr("title", "A measure of query co-regulation in the dataset.")
				//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
				.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
			)
			.append(" : " + dsetScore[i]); 
		var ri = $("<span>").css("font-size", "14px").css("font-weight", "bold").text("Dataset's rank: " + (dsetStart + i)); 

		var dd = $("<div>")
					.append(di)
					.append("<br>")
					.append(wi)
					.append("<br>")
					.append("<br>")
					.append(ri);


		var size = color_red.length;
		var unit_length = (up_clip - down_clip) / size;

		if(pageMode=="expression"){	
		var dt = $("<table>").append($("<tr>").addClass("row1")).append($("<tr>").addClass("row2"));
		//var upper = Math.round(arrays[i].length * 0.10);
		//var upper = Math.min(10, arrays[i].length);
		for(var jj=0; jj<average_expr[i].length; jj++){
			var vi = average_expr[i][jj];
			if(vi>-0.99) continue;
			var j = sample_order[i][jj];
			var lab_split = arrays[i][j].split("|").join("<br>");
			var cc = judge_color(vi, up_clip, down_clip, size, unit_length);
			dt.find("tr.row1").css("height", "10px").append($("<td>").addClass("small_font").css("white-space", "nowrap").css("background-color", "rgb(" + cc[0] + ", " + cc[1] + ", " + cc[2] + ")"));
			dt.find("tr.row2").append($("<td>").css("vertical-align", "top").addClass("vsmall_font").css("line-height", "10px").append(lab_split));
		}

		var du = $("<table>").append($("<tr>").addClass("row1")).append($("<tr>").addClass("row2"));
		//upper = Math.round(arrays[i].length * 0.90);
		//upper = Math.max(0, arrays[i].length - 10);
		for(var jj=0; jj<average_expr[i].length; jj++){
			var vi = average_expr[i][jj];
			if(vi<0.99) continue;
			var j = sample_order[i][jj];
			var lab_split = arrays[i][j].split("|").join("<br>");
			var cc = judge_color(vi, up_clip, down_clip, size, unit_length);
			du.find("tr.row1").css("height", "10px").append($("<td>").addClass("small_font").css("white-space", "nowrap").css("background-color", "rgb(" + cc[0] + ", " + cc[1] + ", " + cc[2] + ")"));
			du.find("tr.row2").append($("<td>").css("vertical-align", "top").addClass("vsmall_font").css("line-height", "10px").append(lab_split));
		}

		dq=$("<span>")
			.css("font-size", "14px")
			.css("font-weight", "bold")
			.append("Conditions where the query context is down-regulated ");

		dh = $("<span>")
			.addClass("seek-help")
			.append("&nbsp;?&nbsp;")
			.attr("title", "Selects conditions where the query and their top 50 co-expressed genes have a gene-centered expression < -1.0.")
			//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
			.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
		;
			
		/*	
		dd = dd.append("<br><br>")
			.append(dq)
			.append($("<span>")
				.css("font-size", "12px")
				.text(" (centered expr. < -1) "))
			.append(dh)
			.append("<br>");

		dd = dd.append(dt)
			.append("<br><span style='font-size:14px;'><b>Conditions where the query context is up-regulated </b></span><span style='font-size:12px'>(centered expr. > 1)</span><br>")
			.append(du);
		*/

		}
		var dName = "<font face=\"Arial\" style=\"font-size:12px;\">Analysis of dataset in the context of query</font>";

		$("#somediv").qtip("option", {"content.text": dd});
		$("#somediv").qtip("api").set("content.title.text", dName);
		$("#somediv").trigger("click");
		$("#desc_label").trigger("click");

		setTimeout(function(){	
			activate_external_link();
		}, 500);
	});
	return false;
});

$("#gene_name_coord").click(function(e){
	var geneID=getOffsetY("#gene_name_coord", e.pageY, geneNames.length);
	var i = geneID;

	var di = $("<div>")
		.attr("id", "desc")
		.css("line-height", "18px")
		.append("<span class='basic_font'>" + geneDescriptionOneLine[i] + "</span>")
		.append("<br>")
		.append($("<span>").css("font-size", "14px").css("font-weight", "bold").text("Description "))
		.append($("<a>")
			.attr("href", "#")
			.attr("id", "gene_desc_label")
			.click(function(){
				if($(this).text()=="[ + ]"){
					$.get("../servlet/GetDescription", {organism:organism, gene:geneNames[i], geneLong:"true"},
					function(responseText){	
						$("#gene_desc_label").text("[ - ]");
						var gg = responseText.split(";;");
						gg.splice(0,3);
						$("#desc_text").html(gg.join("<br>") + "<br>");
						return false;
					});
				}else{
					$(this).text("[ + ]");
					$("#desc_text").html("");
					return false;
				}
			})
			.append($("<span>").css("font-size", "10px").text("[ + ]")))
		.append("<br>")
		.append($("<span>").attr("id", "desc_text").addClass("basic_font").html(""))
	;

	var wi = $("<span>").css("font-size", "14px").css("font-weight", "bold")
			.append("Gene's integrated co-expression ")
			.append($("<span>")
				.addClass("seek-help")
				.append("&nbsp;?&nbsp;")
				.attr("title", "Co-expression score weighted by each dataset's query relevance weight.")
				//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
				.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
			)
			.append(" : " +geneScore[i]); 

	var ri = $("<span>").css("font-size", "14px").css("font-weight", "bold").text("Gene's rank: " + (geneStart + i)); 

	var dd = $("<div>")
				.append(di)
				.append("<br>")
				.append(wi)
				.append("<br>")
				.append("<br>")
				.append(ri);

	var size = color_red.length;
	var unit_length = (up_clip - down_clip) / size;

	if(pageMode=="coexpression"){
		var dt = $("<table>").append($("<tr>").addClass("row1")).append($("<tr>").addClass("row2"));

		for(var jj=0; jj<comat.length; jj++){
			var vi = comat[jj][i];
			var cc = [];
			if(vi.indexOf("Gene missing")==0){
				cc = [neutral_color_red, neutral_color_green, neutral_color_blue];
			}else{
				var vf = parseFloat(vi);
				/*if(vf<0){
					vf = vf*vf*-1.0;
				}else{
					vf = vf*vf;
				}*/
				cc = judge_color(vf, up_clip, down_clip, size, unit_length);
			}
			var pDsetName = dsetNames[jj].split(".")[0];
			dt.find("tr.row1").css("height", "10px").append($("<td>").addClass("small_font").css("white-space", "nowrap").css("background-color", "rgb(" + cc[0] + ", " + cc[1]+ ", " + cc[2] + ")").append(vi));
			var desc = dsetDescriptionOneLineUnformatted[jj];
			if(desc.length>200){
				desc = desc.substring(0, 200) + "...";
			}
			//dark background
			//dt.find("tr.row2").append($("<td>").css("vertical-align", "top").addClass("small_font").css("line-height", "10px").css("color", "#b0b0b0").append(pDsetName + "<br>" + desc));
			//light background
			dt.find("tr.row2").append($("<td>").css("vertical-align", "top").addClass("small_font").css("line-height", "10px").css("color", "#707070").append(pDsetName + "<br>" + desc));
		}
		
		dd = dd.append("<br><br><span style='font-size:14px;'><b>Individual dataset's coexpression score</span><br>")
			.append(dt);
	}

		var dName = "<font face=\"Arial\" style=\"font-size:12px;\">Analysis of gene in the context of query</font>";

		$("#somediv").qtip("option", {"content.text": dd});
		$("#somediv").qtip("api").set("content.title.text", dName);
		$("#somediv").trigger("click");

		setTimeout(function(){	
			activate_external_link();
		}, 500);

		return false;
});

});
