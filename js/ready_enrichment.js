function process_enrichment(responseText, topX, goldstd){
	if(responseText==""){
		var tt = $("<span>").text("No enrichment found. Increase depth in Step 1.");
		return tt;
	}
	var v = responseText.split("\n");

	var tx = $("<span>")
		.addClass("basic_font")
		.append("<b>Results</b>:<br>Tip: click on the numbers in the column T&amp;A to see the enriched genes.<br>");

	var tt = $("<table>")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.css("table-layout", "fixed")
		.css("width", "600px")
		.addClass("basic_font");

	tt.append($("<tr>")
			.addClass("table_header_underline")
			.append($("<td>")
				.css("width", "320px")
				.append($("<b>").text("Term"))
				.append("<br>&nbsp;")
			)
			.append($("<td>")
				.css("width", "60px")
				.append($("<b>").text("p-value "))
				.append("<br>")
				.append($("<a>")
					.attr("href", "#")
					.click(function(){return false;})
					.addClass("help")
					.addClass("basic_font")
					.append($("<span>")
						.css("background-color", "#e9ab17")
						.css("font-weight", "bold")
						.append("&nbsp;?&nbsp;")
						.attr("title", "P-value adjusted for multiple hypothesis testing (Benjamini-Hochberg)")
						//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
						.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
					)
				)	
			)
			.append($("<td>")
				.css("width", "60px")
				.append($("<b>").text("q-value "))
				.append("<br>")
				.append($("<a>")
					.attr("href", "#")
					.click(function(){return false;})
					.addClass("help")
					.addClass("basic_font")
					.append($("<span>")
						.css("background-color", "#e9ab17")
						.css("font-weight", "bold")
						.append("&nbsp;?&nbsp;")
						.attr("title", "Minimum false discovery rate")
						//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
						.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
					)
				)	
			)
			.append($("<td>")
				.css("width", "40px")
				.append($("<b>").text("T "))
				.append("<br>")
				.append($("<a>")
					.attr("href", "#")
					.click(function(){return false;})
					.addClass("help")
					.addClass("basic_font")
					.append($("<span>")
						.css("background-color", "#e9ab17")
						.css("font-weight", "bold")
						.append("&nbsp;?&nbsp;")
						.attr("title", "Term size")
						//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
						.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
					)
				)
			)	
			.append($("<td>")
				.css("width", "40px")
				.append($("<b>").text("A "))
				.append("<br>")
				.append($("<a>")
					.attr("href", "#")
					.click(function(){return false;})
					.addClass("help")
					.addClass("basic_font")
					.append($("<span>")
						.css("background-color", "#e9ab17")
						.css("font-weight", "bold")
						.append("&nbsp;?&nbsp;")
						.attr("title", "Number of genes in the analysis set with some annotations.")
						//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
						.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
					)
				)
			)	
			.append($("<td>")
				.css("width", "40px")
				.append($("<b>").text("T&A "))
				.append("<br>")
				.append($("<a>")
					.attr("href", "#")
					.click(function(){return false;})
					.addClass("help")
					.addClass("basic_font")
					.append($("<span>")
						.css("background-color", "#e9ab17")
						.css("font-weight", "bold")
						.append("&nbsp;?&nbsp;")
						.attr("title", "Size of overlap between the term set and the analysis set.")
						//.qtip({style: {classes: "ui-tooltip-dark", width:"300px"}})
						.qtip({style: {classes: "ui-tooltip-blue", width:"300px"}})
					)
				)
			));
	for(var i=0; i<v.length; i++){
		var vt = v[i].split("\t");
		var vt_orig = new String(vt[0]);
		vt[0] = vt[0].replace(/_/g, " ");
		tt.append($("<tr>")
			.append($("<td>").text(vt[0]))
			.append($("<td>").text(vt[1]))
			.append($("<td>").text(vt[2]))
			.append($("<td>").text(vt[3]))
			.append($("<td>").text(vt[4]))
			.append($("<td>")
				.append($("<a>")
					.attr("id", "overlap_" + i)
					.attr("href", "#")
					.attr("name", vt_orig)
					.css("text-decoration", "underline")
					.text(vt[5])
					.click(function(e){
						var id_name = $(this).attr("id");
						$.post("../servlet/GeneEnrichment", {organism:organism, sessionID:sessionID, top:topX, 
							goldstd:goldstd, show_overlap:"n", term:$(this).attr("name"), 
							overlap_only:"y", filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
							specify_genes:sessionStorage.getItem("specify_genes")},
							function(responseText){
								//alert(responseText);
								$("#overlapdiv").qtip("option", {"content.text": responseText});
								$("#overlapdiv").qtip("api").set("content.title.text", "<span class='basic_font'>Overlapping genes</span>");
								$("#overlapdiv").qtip("api").set("position.target", $("#" + id_name));
								$("#overlapdiv").qtip("show");
								//$("#overlap_" + id_name).trigger("click");
								//$(this).qtip({style: {classes: "ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});
						});
						return false;
					})
				)
			)
		);
	}
	tt = tx.after(tt);
	//ta.html(tt);
	return tt;
}

$(document).ready(function(){

$("#enrich_genes").click(function(e){

	var th = $("<font>").css("font-family", "Arial")
		.css("font-size", "12px")
		;
	var topt = $("<table>")
		.addClass("basic_font")
		.append($("<tr>")
			.append($("<td>")
				.append("<b>Step 1</b>: Analyze top genes: ")
				.append($("<select>")
					.attr("name", "topX")
					.attr("id", "topX")
					.css("font-size", "12px")
					.append($("<option>").text("20"))
					.append($("<option>").text("50"))
					.append($("<option selected='selected'>").text("100"))
					.append($("<option>").text("200"))
					.append($("<option>").text("500"))
					.append($("<option>").text("1000"))
					.change(function(){
						var tt = parseInt($("#topX option:selected").text());
						var tx = $("#enrich_gold_std option:selected").attr("val");
						$("#overlapdiv").qtip("hide");
						$("#enrichment_window").text("CALCULATING ENRICHMENTS... (1 - 30 seconds).");
						setTimeout(function(){
						$.post("../servlet/GeneEnrichment", {organism:organism, sessionID:sessionID, top:tt, 
							goldstd:tx, show_overlap:"n", term:"none", overlap_only:"n",
							filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
							specify_genes:sessionStorage.getItem("specify_genes")}, 
							function(responseText){
								var t = process_enrichment(responseText, tt, tx);
								$("#enrichment_window").html(t);
							});
						}, 500);
					})
				)
			)
		)
		.append($("<tr>")
			.append($("<td>")
				.append("<b>Step 2</b>: Select a functional database: ")
				.append($("<select>")
					.attr("name", "enrich_gold_std")
					.attr("id", "enrich_gold_std")
					.css("font-size", "12px")
					/*.append($("<option>")
						.attr("val", "msigdb_cgp").text("MsigDB chemical, genetic perturbation terms"))
					.append($("<option>")
						.attr("val", "msigdb_chr").text("MsigDB chromosomal cytoband positions"))
					.append($("<option>")
						.attr("val", "msigdb_biocarta").text("Biocarta terms"))
					.append($("<option>")
						.attr("val", "msigdb_kegg").text("KEGG terms"))
					.append($("<option>")
						.attr("val", "msigdb_reactome").text("Reactome terms"))
					.append($("<option>")
						.attr("val", "targetscan").text("Targetscan miRNA conserved targets"))
					*/
					.append($("<option selected='selected'>")
						.attr("val", "original_bp").text("GO biological process (BP) terms"))
					.append($("<option>")
						.attr("val", "withIEA_bp").text("GO BP terms (with electronic annotations)"))
					.change(function(){
						var tt = parseInt($("#topX option:selected").text());
						var tx = $("#enrich_gold_std option:selected").attr("val");
						$("#overlapdiv").qtip("hide");
						$("#enrichment_window").text("CALCULATING ENRICHMENTS... (1 - 30 seconds).");
						setTimeout(function(){
						$.post("../servlet/GeneEnrichment", {organism:organism, sessionID:sessionID, top:tt, 
							goldstd:tx, show_overlap:"n", term:"none", overlap_only:"n", 
							filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
							specify_genes:sessionStorage.getItem("specify_genes")}, 
							function(responseText){
								var t = process_enrichment(responseText, tt, tx);
								$("#enrichment_window").html(t);
							});
						}, 500);
					})
				)
			)		
		);

	var enrich_options = [];
	var enrich_descriptions = [];

	if(organism=="human"){
		enrich_options = ["msigdb_cgp", "msigdb_chr", "msigdb_biocarta", "msigdb_kegg", 
			"msigdb_reactome", "targetscan", 
			"original_cp", "withIEA_cp", "original_mf", "withIEA_mf"];
		enrich_descriptions = ["MsigDB chemical, genetic perturbation terms",
			"MsigDB chromosomal cytoband positions",
			"Biocarta terms", "KEGG terms", "Reactome terms", "Targetscan conserved miRNA targets", 
			"GO Cellular Component", "GO Cellular Component (with electronic annotations)", 
			"GO Molecular Function", "GO Molecular Function (with electronic annotations)"];
	}else if(organism=="frog"){
		enrich_options = ["kegg"];
		enrich_descriptions = ["KEGG terms"];
	}else{
		enrich_options = ["kegg", "original_cp", "withIEA_cp", "original_mf", "withIEA_mf"];
		enrich_descriptions = ["KEGG terms", 
			"GO Cellular Component", "GO Cellular Component (with electronic annotations)", 
			"GO Molecular Function", "GO Molecular Function (with electronic annotations)"];
	}

	for(var vii=0; vii<enrich_options.length; vii++){
		topt.find("#enrich_gold_std")
			.append($("<option>")
				.attr("val", enrich_options[vii]).text(enrich_descriptions[vii]));
	}

	var ta = $("<div>")
		.attr("id", "enrichment_window")
		.css("width", "620px")
		.css("height", "420px")
		.css("overflow", "auto")
		.scroll($.debounce(250, true, function(){
			$("#overlapdiv").qtip("hide");
		}))
		.append($("<font>").addClass("basic_font").text("CALCULATING ENRICHMENTS... (1 - 30 seconds)."));

	ta = th.after(topt).after("<p>").after(ta);
	var tit = "<font style=\"font-family:Arial; font-size:12px;\">Gene Enrichment</font>";	
	$("#somediv").qtip("option", {"content.text": ta});
	$("#somediv").qtip("api").set("content.title.text", tit);
	$("#somediv").trigger("click");
	
	setTimeout(function(){	
		$.post("../servlet/GeneEnrichment", {organism:organism, sessionID:sessionID, top:100, 
		goldstd:"original_bp", show_overlap:"n", term:"none", overlap_only:"n",
		filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
		specify_genes:sessionStorage.getItem("specify_genes")}, 
		function(responseText){
			var t = process_enrichment(responseText, 100, "original_bp");
			$("#enrichment_window").html(t);
		});
	}, 500);
	return false;
});

});

