function drawOptions(){
	var opt = $("<div>")
		.attr("id", "option_window")
		.css("width", "600px")
		;

	var sel_search_alg = $("<select>")
		.attr("id", "search_alg")
		.append($("<option>").attr("value", "RBP").text("CV RBP weighted")) //RBP
		.append($("<option>").attr("value", "OrderStatistics").text("Order statistics")) //OrderStatistics
		.append($("<option>").attr("value", "EqualWeighting").text("Equally weighted")) //EqualWeighting
		.append($("<option>").attr("value", "CV_CUSTOM").text("FN-guided weighting")) //EqualWeighting
		.change(function(){
			var val = $("#search_alg option:selected").attr("value");
			sessionStorage.setItem("search_alg", val);
		})
	;

	var sel_search_distance = $("<select>")
		.attr("id", "search_distance")
		.append($("<option>").attr("value", "Correlation").text("Correlation"))
		.append($("<option>").attr("value", "Zscore").text("Z-score of correlation"))
		.append($("<option>").attr("value", "ZscoreHubbinessCorrected").text(
			"Z-score of correlation with gene connectivity correction"))
		.change(function(){
			sessionStorage.setItem("search_distance", 
			$("#search_distance option:selected").attr("value"));
		});

	var sel_search_rbp_p = $("<input>")
		.attr("type", "text")
		.attr("name", "rbp_p")
		.attr("id", "rbp_p")
		.css("width", "50")
		.attr("value", sessionStorage.getItem("rbp_p"))
		.focusout(function(){
			sessionStorage.setItem("rbp_p", $("#rbp_p").attr("value"));
		})
		;
	
	var sel_search_percent_query = $("<input>")
		.attr("type", "text")
		.attr("name", "percent_query")
		.attr("id", "percent_query")
		.css("width", "50")
		.attr("value", sessionStorage.getItem("percent_query"))
		.focusout(function(){
			sessionStorage.setItem("percent_query", $("#percent_query").attr("value"));
		})
		;

	var sel_search_percent_genome = $("<input>")
		.attr("type", "text")
		.attr("name", "percent_genome")
		.attr("id", "percent_genome")
		.css("width", "50")
		.attr("value", sessionStorage.getItem("percent_genome"))
		.focusout(function(){
			sessionStorage.setItem("percent_genome", $("#percent_genome").attr("value"));
		})
		;

	var dset_agg = "Methods of weighting and aggregating datasets:<br><br>" + 
		"<b>CV RBP weighted</b>: cross-validated, query-based dataset weighting, followed by combination of datasets' co-expression scores using the relevance weights (Default)<br><br>" + 
		"<b>Order statistics</b>: an order-statistics based approach, favors genes which rank high in query's correlation vector than if query's correlation vector were randomized. (Adler et al)<br><br>" +
		"<b>Equally weighted</b>: assigns all datasets with weight of 1, then adds the co-expression score across datasets."; 

	var rbp_p_tip = "Applies if <b>CV RBP weighted</b> is chosen.<br><br>" + 
	"The parameter <b>p</b> specifies the emphasis towards high correlations when weighting dataset and scoring genes.<br><br>" +
	"RBP assigns an <i>importance</i> score to a correlation value based on how this correlation ranks with respect to all other correlations in the query's neighborhood. A closely ranked gene will be given a high RBP score." + 
	"<b>0.95</b> - each correlation will be given an emphasis value of 0.95^(the rank)" + 
	"<b>0.99</b> (Default) - emphasis value becomes 0.99^(the rank) see paper for details<br><br>" + 
	"As p increases, emphasis is distributed towards more lowly ranked correlations. Users can adjust dataset weighting using this parameter.";

	var fraction_genome_tip = "Fraction (0 - 1.0) of genome required to be present in each dataset. <br><br>" + 
	"The number (fraction * genome_size) is the minimum number of genes that a dataset should cover. Datasets that do not meet this requirement are discarded." + 
	"<br><br>Default 0 means that there is no such restriction (minimum is 0).";



	var fraction_query_tip = "Fraction (0 - 1.0) of query genes required to be present in each dataset. <br><br>" + 
	"Not all datasets contain all of the query genes. " +
	"This controls the minimum query-presence in a dataset in order to consider it for weighting and integration. " + 
	"Default 0 means that there is no such restriction.";

	var distance_measure_tip = "<b>Correlation</b> - Pearson correlation<br><br>" + 
	"<b>Z-score</b> - Fisher transformation of the Pearson, followed by standardization.<br><br>" + 
	"<b>Z-score with gene connectivity correction</b> (Default) - Balances gene connectivity to downgrade the influence of hubby genes on the overall correlation structure";

	var search_opt = $("<div>")
		.append($("<span>")
			.addClass("big_font")
			.append("<b>Search Options:</b>")
		)
		.append("<br>")
		.append("Dataset aggregation ")
		.append($("<a>")
			.attr("href", "#")
			.attr("title", dset_agg)
			.qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}})
			.addClass("help")
			.append($("<span>")
				.css("background-color", "#e9ab17")
				.css("font-weight", "bold")
				.append("&nbsp;?&nbsp;")
			)
		)
		.append(sel_search_alg)
		.append("<br>")
		.append("RBP parameter p, (0 &lt; p &lt; 1.0) ")
		.append($("<a>")
			.attr("href", "#")
			.attr("title", rbp_p_tip)
			.qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}})
			.addClass("help")
			.append($("<span>")
				.css("background-color", "#e9ab17")
				.css("font-weight", "bold")
				.append("&nbsp;?&nbsp;")
			)
		)
		.append(sel_search_rbp_p)
		.append("<br>")
		.append("Fraction of query genes required present, (0 - 1.0) ")
		.append($("<a>")
			.attr("href", "#")
			.attr("title", fraction_query_tip)
			.qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}})
			.addClass("help")
			.append($("<span>")
				.css("background-color", "#e9ab17")
				.css("font-weight", "bold")
				.append("&nbsp;?&nbsp;")
			)
		)
		.append(sel_search_percent_query)
		.append("<br>")
		.append("Fraction of genome a dataset should cover, (0 - 1.0) ")
		.append($("<a>")
			.attr("href", "#")
			.attr("title", fraction_genome_tip)
			.qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}})
			.addClass("help")
			.append($("<span>")
				.css("background-color", "#e9ab17")
				.css("font-weight", "bold")
				.append("&nbsp;?&nbsp;")
			)
		)
		.append(sel_search_percent_genome)
		.append("<br>")
		.append("Distance measure ")
		.append($("<a>")
			.attr("href", "#")
			.attr("title", distance_measure_tip)
			.qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}})
			.addClass("help")
			.append($("<span>")
				.css("background-color", "#e9ab17")
				.css("font-weight", "bold")
				.append("&nbsp;?&nbsp;")
			)
		)
		.append(sel_search_distance)
		.append("<br>")
	;

	var sel_vis_value = $("<select>")
		.attr("id", "vis_value")
		.append($("<option>").attr("value", "norm").text("Gene-Centered Normalized"))
		.append($("<option>").attr("value", "original").text("Absolute Expression"))
		.change(function(){
			sessionStorage.setItem("vis_value", 
			$("#vis_value option:selected").attr("value"));
			toRefresh = true;
		});



	var sel_vis_color_scale = $("<select>")
		.attr("id", "vis_color_scale")
		.append($("<option>").attr("value", "blue-yellow-red").text("blue-yellow-red"))
		.append($("<option>").attr("value", "blue-white-red").text("blue-white-red"))
		.append($("<option>").attr("value", "green-white-red").text("green-white-red"))
		.append($("<option>").attr("value", "blue-black-yellow").text("blue-black-yellow"))
		.append($("<option>").attr("value", "green-black-red").text("green-black-red"))
		.change(function(){
			sessionStorage.setItem("vis_color_scale", 
			$("#vis_color_scale option:selected").attr("value"));
			toRefresh = true;
		});

	var sel_vis_p_value_filter = $("<input>")
		.attr("type", "text")
		.attr("name", "pval_filter")
		.attr("id", "pval_filter")
		.css("width", "50px")
		.attr("value", sessionStorage.getItem("pval_filter"))
		.focusout(function(){
			sessionStorage.setItem("pval_filter", $("#pval_filter").attr("value"));
			pvalCutoff = sessionStorage.getItem("pval_filter");
			if(pvalCutoff=="-1") pval_filter = false;
			else pval_filter = true;
			toRefresh = true;
		})
		;

	var sel_vis_multiply_d_weight = $("<select>")
		.attr("id", "vis_multiply_d_weight")
		.append($("<option>").attr("value", "true").text("true"))
		.append($("<option>").attr("value", "false").text("false"))
		.change(function(){
			sessionStorage.setItem("multiply_d_weight", 
			$("#vis_multiply_d_weight option:selected").attr("value"));
			toRefresh = true;
		});
	
	var sel_refine_opt = $("<select>")
		.attr("id", "opt_refine_remember_dset")
		.append($("<option>").attr("value", "true").text("true"))
		.append($("<option>").attr("value", "false").text("false"))
		.change(function(){
			sessionStorage.setItem("remember_dset_selection",
			$("#opt_refine_remember_dset option:selected").attr("value"));
			toRefresh = false;
		});
	
	var sel_all_genes = $("<select>")
		.attr("id", "opt_display_all_genes")
		.append($("<option>").attr("value", "true").text("true"))
		.append($("<option>").attr("value", "false").text("false"))
		.change(function(){
			sessionStorage.setItem("display_all_genes",
				$("#opt_display_all_genes option:selected").attr("value"));
			if($("#opt_display_all_genes").val()=="false"){
				sessionStorage.setItem("specify_genes", "");
				$("#specify_genes").show();
				$("#gene_input_box").attr("value", "");
			}else{
				$("#specify_genes").hide();
				$("#gene_input_box").attr("value", "");
				sessionStorage.setItem("specify_genes", "");
			}
			toRefresh = true;
		});

	//whether or not to display dataset text-mined keywords, or use dataset Title
	var sel_display_dset_keywords = $("<select>")
		.attr("id", "opt_display_dset_keywords")
		.append($("<option>").attr("value", "true").text("dataset text-mined keywords"))
		.append($("<option>").attr("value", "false").text("dataset title"))
		.change(function(){
			sessionStorage.setItem("display_dset_keywords", 
				$("#opt_display_dset_keywords option:selected").attr("value"));
			toRefresh = true;
		});

	var view_opt = $("<div>")
		.append($("<span>")
			.addClass("big_font")
			.append("<b>Expression View Options:</b>")
		)
		.append("<br>")
		.append("Expressions values ")
		.append(sel_vis_value)
		.append("<br>")
		.append("P-value filter (0.0001-0.1, or -1: off) ")
		.append(sel_vis_p_value_filter)
		.append("<br>")
		.append("Color scale")
		.append(sel_vis_color_scale)
		.append("<br>")
		.append("Dataset header display")
		.append(sel_display_dset_keywords)
		.append("<br>")
		.append("Display all of the genes in the genome")
		.append(sel_all_genes)
		.append("<br>")
		.append($("<span>")
			.attr("id", "specify_genes")
			.append("Specify what genes to display (space delimited or one gene per line):")
			.append("<br>")
			.append($("<textarea>")
				.attr("id", "gene_input_box")
				.attr("cols", "40")
				.attr("rows", "5")
				.attr("value", sessionStorage.getItem("specify_genes"))
				.focus(function(){
					toRefresh = true;
				})
			)
			.hide()
		)
	;

	var view_coexp_opt = $("<div>")
		.append($("<span>").addClass("big_font").append("<b>Co-expression View Options: </b>"))
		.append("<br>")
		.append("Display dataset-relevance weighted co-expression score")
		.append(sel_vis_multiply_d_weight)
		.append("<br>")
	;

	var button_save = $("<input>")
		.attr("id", "option_save")
		.attr("type", "button")
		.attr("value", "Save")
		.click(function(){
			sessionStorage.setItem("specify_genes", $("#gene_input_box").val());
			$("#somediv").qtip("api").hide();
			if(toRefresh){
				/*
				$.post("../servlet/GetPageLimit", {organism:organism,
				sessionID:sessionID, mode:pageMode, 
				filter_by_pval:"false", pval_cutoff:"-1", 
				specify_genes:sessionStorage.getItem("specify_genes")},
				function(responseText){
					if(responseText=="Error: Empty!"){
						alert("Error in specifying genes, none of the genes exist! " + 
							"Please fix this problem in Options");
						return false;
					}
				*/
				toRefresh = false;
				location.reload();
				//});
			}
		});
	
	var refine_opt = $("<div>")
		.append($("<span>")
			.addClass("big_font")
			.append("<b>Refine Search Options:</b>")
		)
		.append("<br>")
		.append("Remember dataset selection for subsequent queries ")
		.append(sel_refine_opt)
		.append("<br>")
	;

	opt.append(search_opt)
		.append("<br>")
		.append(view_opt)
		.append("<br>")
		.append(view_coexp_opt)
		.append("<br>")
		.append(refine_opt)
		.append("<br>")
		.append(button_save)
	;
	return opt;
}

var toRefresh = false;

$(document).ready(function(){
$("#option_link").click(function(){
	var ta = drawOptions();
	$("#somediv").qtip("option", {"content.text": ta});
	$("#somediv").qtip("option", {"content.title.text": "Options"});
	$("#somediv").trigger("click");
	ta.find('#search_alg option[value="' + sessionStorage.getItem("search_alg") +'"]').attr("selected", "selected");
	ta.find('#search_distance option[value="' + sessionStorage.getItem("search_distance") +'"]').attr("selected", "selected");
	ta.find("#rbp_p").text(sessionStorage.getItem("rbp_p"));
	ta.find("#percent_query").text(sessionStorage.getItem("percent_query"));
	ta.find("#percent_genome").text(sessionStorage.getItem("percent_genome"));

	ta.find('#vis_value option[value="' + sessionStorage.getItem("vis_value") + '"]').attr("selected", "selected");
	ta.find("#pval_filter").text(sessionStorage.getItem("pval_filter"));
	ta.find('#vis_multiply_d_weight option[value="' + sessionStorage.getItem("multiply_d_weight") + '"]')
		.attr("selected", "selected");
	ta.find('#opt_refine_remember_dset option[value="' + sessionStorage.getItem("remember_dset_selection") + '"]')
		.attr("selected", "selected");
	ta.find('#vis_color_scale option[value="' + sessionStorage.getItem("vis_color_scale") + '"]').attr("selected", "selected");
	ta.find('#opt_display_all_genes option[value="' + sessionStorage.getItem("display_all_genes") + '"]')
		.attr("selected", "selected");
	if(sessionStorage.getItem("display_all_genes")=="false"){
		ta.find("#specify_genes").show();	
	}

	ta.find('#opt_display_dset_keywords option[value="' + sessionStorage.getItem("display_dset_keywords") + '"]')
		.attr("selected", "selected");

	return false;
});

sessionSetDefault("specify_genes", "");
sessionSetDefault("display_all_genes", "true");
sessionSetDefault("display_dset_keywords", "true");
sessionSetDefault("remember_dset_selection", "false");
sessionSetDefault("search_alg", "RBP");
sessionSetDefault("search_distance", "ZscoreHubbinessCorrected");
sessionSetDefault("rbp_p", "0.99");
sessionSetDefault("percent_query", "0.5");
sessionSetDefault("percent_genome", "0.5");

sessionSetDefault("vis_value", "norm");
sessionSetDefault("vis_color_scale", "blue-black-yellow");
sessionSetDefault("pval_filter", "-1");
sessionSetDefault("multiply_d_weight", "true");


});
