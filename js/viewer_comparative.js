function adjust_table_width(){
	var widthPerQuery = 475;
	var tableWidth = 475 * numQuery;
	var canvasWidth = tableWidth + 200;

	/*if(numQuery==1){
		widthPerQuery = 675;
		tableWidth = 675;
		canvasWidth = 875;
	}*/

	$("#head1").css("width", tableWidth + "px");
	$("#table1").css("width", tableWidth + "px");
	$("#wrapper").css("width", canvasWidth + "px");

	var percentage = 100 / numQuery;

	$("#query_row").find("td").each(function(){
		$(this).css("width", percentage + "%");
	});

	$("#table1_colspan1").attr("colspan", numQuery);
	$("#table1_colspan2").attr("colspan", numQuery);
	$("#table1_colspan3").attr("colspan", numQuery);
	$("#table1_colspan4").attr("colspan", numQuery);
	$("#table1_colspan5").attr("colspan", numQuery);
}

function resize_array(array1, numQuery){
	var array2 = new Array(numQuery);
	for(var vi=1; vi<=numQuery; vi++){
		array2[vi-1] = array1[vi-1];
	}
	return array2;
}


function get_ready(){
	top_dataset_array = new Array(numQuery);
	top_gene_array = new Array(numQuery);

	sessionID_array = resize_array(sessionID_array, numQuery);
	organism_array = resize_array(organism_array, numQuery);
	page_org_array = resize_array(page_org_array, numQuery);
	page_gene_org_array = resize_array(page_gene_org_array, numQuery);
	queryNames_array = resize_array(queryNames_array, numQuery);	

	arrayQuery = new Array(numQuery);

	for(var vi=1; vi<=numQuery; vi++){
		$("#top_datasets_" + vi).unbind("change").change(function(){
			var qid = $(this).attr("qid");
			var top_d = $("#top_datasets_" + qid + " option:selected").val();
			top_dataset_array[qid-1] = top_d;
			$("#dataset_enrichment_" + qid).find("table.main").find("tr.content").remove();
			get_dataset_enrichment(qid, organism_array[qid-1], sessionID_array[qid-1], top_d);
		});
		$("#top_genes_" + vi).unbind("change").change(function(){
			var qid = $(this).attr("qid");
			var top_g = $("#top_genes_" + qid + " option:selected").val();
			top_gene_array[qid-1] = top_g;
			$("#gene_enrichment_" + qid).find("table.main").find("tr.content").remove();
			get_gene_enrichment(qid, organism_array[qid-1], sessionID_array[qid-1], top_g);
		});
	}

	$("#dataset_enrichment_link").unbind("click").click(function(){
		for(var vi=1; vi<=numQuery; vi++){		
			top_dataset_array[vi-1] = get_default_top_dataset(organism_array[vi-1]);
			$("#top_datasets_" + vi).find('option[value="' + top_dataset_array[vi-1] + '"]')
				.attr("selected", "selected");
			$("#dataset_enrichment_" + vi).find("table.main").find("tr.content").remove();
			get_dataset_enrichment(vi, organism_array[vi-1], sessionID_array[vi-1], 
				top_dataset_array[vi-1]);
		}
	});

	$("#gene_enrichment_link").unbind("click").click(function(){
		for(var vi=1; vi<=numQuery; vi++){
			top_gene_array[vi-1] = get_default_top_gene(organism_array[vi-1]);
			$("#top_genes_"+vi).find('option[value="' + top_gene_array[vi-1] + '"]')
				.attr("selected", "selected");
			$("#gene_enrichment_" + vi).find("table.main").find("tr.content").remove();
			get_gene_enrichment(vi, organism_array[vi-1], sessionID_array[vi-1], 
				top_gene_array[vi-1]);
		}
	});

	$("#dataset_link").unbind("click").click(function(){
		page_org_array = new Array(numQuery);
		for(var vi=1; vi<=numQuery; vi++){
			page_org_array[vi-1] = 0;
			$("#dataset_enrichment_" + vi).hide();
			$("#dataset_result_" + vi).hide();
			get_dataset_result(vi, organism_array[vi-1], sessionID_array[vi-1],  
				page_org_array[vi-1], "20");
		}
	});

	$("#gene_link").unbind("click").click(function(){
		page_gene_org_array = new Array(numQuery);
		for(var vi=1; vi<=numQuery; vi++){		
			page_gene_org_array[vi-1] = 0;
			$("#dataset_enrichment_" + vi).hide();
			$("#dataset_result_" + vi).hide();
			get_gene_result(vi, organism_array[vi-1], sessionID_array[vi-1], 
				page_gene_org_array[vi-1], "100");
		}
	});

	$("#compare_button").unbind("click").click(function(){
		for(var vi=1; vi<=numQuery; vi++){
			var o1 = $("#org" + vi).val();
			if(o1=="-"){
				alert("Select an organism for query " + vi);
				return false;
			}
			var n1 = $("#query_" + vi + "_session").attr("value");
			if(n1==""){
				alert("Enter sessionID for query " + vi);
				return false;
			}
		}
		for(var vi=1; vi<=numQuery; vi++){
			sessionID_array[vi-1] = $("#query_" + vi + "_session").attr("value");
			organism_array[vi-1] = $("#org" + vi).val();
		}
		$("#dataset_link").trigger("click");
		for(var vi=1; vi<=numQuery; vi++){
			$("#query_" + vi).show();
			get_query(vi, organism_array[vi-1], sessionID_array[vi-1]);
			$("#s_name_" + vi).text("(sessionID: " + sessionID_array[vi-1] + ")");
			$("#span" + vi).show();
		}
		activate_visualize();
		return false;
	});


	$("#search_button").unbind("click").click(function(){
		for(var vi=1; vi<=numQuery; vi++){
			var o1 = $("#org" + vi).val();
			if(o1=="-"){
				alert("Select an organism for query " + vi);
				return false;
			}
			var n1 = $("#search_query_" + vi).attr("value");
			if(n1==""){
				alert("Enter the query genes for query " + vi);
				return false;
			}
		}
		sessionStorage.setItem("dset_search_mode", "all");
		for(var vi=1; vi<=numQuery; vi++){
			organism_array[vi-1] = $("#org" + vi).val();
		}
		$("#dataset_result").html("");
		$("#search_result").html("");
		$("#status_text").text("(1/" + numQuery + ") Searching in " + 
			organism_array[0] + " compendium");
		$("#statusdiv").trigger("click");
		var queryUpper = $("#search_query_1").attr("value").toUpperCase();
		queryUpper = queryUpper.replace(/;/g, " ").replace(/,/g, " ")
			.replace(/"/g, " ").replace(/\s{2,}/g, " ");
		current_id = 0;
		completed = 0;
		search_one_query(organism_array[0], queryUpper);
		return false;
	});

	$("#mode1").unbind("click").click(function(){
		if($("#mode1").is(":checked")){
			$("#mode2").prop("checked", false);
			$(".retrieve").show();
			$(".new_query").hide();
		}else{
			$("#mode2").prop("checked", true);
			$(".retrieve").hide();
			$(".new_query").show();
		}
	});
	$("#mode2").unbind("click").click(function(){
		if($("#mode2").is(":checked")){
			$("#mode1").prop("checked", false);
			$(".retrieve").hide();
			$(".new_query").show();
		}else{
			$("#mode1").prop("checked", true);
			$(".retrieve").show();
			$(".new_query").hide();
		}
	});

	$("#mode1").prop("checked", false);
	$("#mode2").prop("checked", true);
	$(".retrieve").hide();
	$(".new_query").show();
	activate_visualize();
}

function delete_comparison(){
	$("td[qid=" + numQuery + "]").remove();
	numQuery -= 1;
	adjust_table_width();
	get_ready();
}

function add_comparison(){
	numQuery+=1; //up to 4 queries supported

	adjust_table_width();
	var queryID = numQuery;
	var percentage = 100 / numQuery;

	$("#query_row").append(
		$("<td>")
			.attr("qid", queryID)
			.css("width", percentage + "%")
			.css("padding", "10px")
			.append("<b>Query " + queryID + ": </b>")
			.append($("<span>")
				.attr("id", "q_name_" + queryID)
				.addClass("q_name")
			)
			.append($("<span>")
				.attr("id", "s_name_" + queryID)
				.addClass("s_name")
			)
			.append($("<span>")
				.attr("id", "span" + queryID)
				.addClass("span_class")
				//.hide()
				.append("<br>(")
				.append($("<a>")
					.attr("href", "#")
					.attr("qid", queryID)
					.addClass("visualize_" + queryID)
					.css("text-decoration", "underline")
					.append("Visualize")
				)
				.append(")")
			)
			.append("<br>Organism")
			.append($("<select>")
				.attr("name", "org" + queryID)
				.attr("id", "org" + queryID)
				.attr("qid", queryID)
				.append($("<option>").attr("value", "-").append("--"))
				.append($("<option>").attr("value", "yeast").append("yeast (SCE)"))
				.append($("<option>").attr("value", "fly").append("fly (DME)"))
				.append($("<option>").attr("value", "mouse").append("mouse (MMU)"))
				.append($("<option>").attr("value", "human").append("human (HSA)"))
				.append($("<option>").attr("value", "worm").append("worm (CEL)"))
				.append($("<option>").attr("value", "zebrafish").append("zebrafish (DRE)"))
			)
	);

	$("#session_row").append(
		$("<td>")
			.attr("qid", queryID)
			.css("padding-left", "10px")
			.css("vertical-align", "top")
			.append($("<div>")
				.addClass("retrieve")
				.append("SessionID")
				.append($("<input>")
					.attr("size", "12")
					.attr("id", "query_" + queryID + "_session")
					.attr("name", "query_" + queryID + "_session")
				)
				.append($("<input>")
					.attr("id", "visualize_" + queryID + "_button")
					.attr("qid", queryID)
					.addClass("visualize_" + queryID)
					.attr("type", "submit")
					.attr("value", "Visualize")
				)
			)
	);

	$("#new_query_row").append(
		$("<td>")
			.attr("qid", queryID)
			.css("padding-left", "10px")
			.append($("<div>")
				.addClass("new_query")
				.append("New query")
				.append($("<input>")
					.attr("size", "12")
					.attr("id", "search_query_" + queryID)
					.attr("name", "search_query_" + queryID)
				)
				.append("<br>")
				.append("(eg: ptch1 ptch2 in mouse)")
				//.append("<br>")
				//.append("(multi-gene query for enhanced context)")
			)
	);

	$("#result_row").append(
		$("<td>")
			.css("vertical-align", "top")
			.attr("qid", queryID)
			//gene results
			.append($("<div>")
				.attr("id", "gene_result_" + queryID)
				.attr("name", "gene_result_" + queryID)
				.css("width", "100%")
				.hide()
				.append($("<table>")
					.addClass("main")
					.css("font-size", "12px")
					.css("width", "100%")
					.append($("<tr>")
						.addClass("head")
						.append($("<td>")
							.attr("colspan", "4")
							.append("All Genes")
						)
					)
					.append($("<tr>")
						.addClass("head")
						.addClass("navigation")
						.append($("<td>")
							.attr("colspan", "4")
						)
					)
					.append($("<tr>")
						.addClass("head")
						.append($("<td>").append("Rank"))
						.append($("<td>").append("Score"))
						.append($("<td>").append("Gene"))
						.append($("<td>").append("Snippet"))
					)
				)
			)
			//dataset results
			.append($("<div>")
				.attr("id", "dataset_result_" + queryID)
				.attr("name", "dataset_result_" + queryID)
				.css("width", "100%")
				.hide()
				.append($("<table>")
					.addClass("main")
					.css("font-size", "12px")
					.css("width", "100%")
					.append($("<tr>")
						.addClass("head")
						.append($("<td>")
							.attr("colspan", "4")
							.append("All Datasets")
						)
					)
					.append($("<tr>")
						.addClass("head")
						.addClass("navigation")
						.append($("<td>")
							.attr("colspan", "4")
						)
					)
					.append($("<tr>")
						.addClass("head")
						.append($("<td>").append("Rank"))
						.append($("<td>").append("PValue"))
						.append($("<td>").append("Dataset"))
						.append($("<td>").append("Title"))
					)
				)
			)
			//gene enrichment results
			.append($("<div>")
				.attr("id", "gene_enrichment_" + queryID)
				.attr("name", "gene_enrichment_" + queryID)
				.css("width", "100%")
				.hide()
				.append($("<table>")
					.addClass("main")
					.css("font-size", "12px")
					.css("width", "100%")
					.append($("<tr>")
						.addClass("head")
						.append($("<td>")
							.attr("colspan", "4")
							.append("All Gene Enrichments<br>in")
							.append($("<select>")
								.attr("name", "top_genes_" + queryID)
								.attr("id", "top_genes_" + queryID)
								.attr("qid", queryID)
								.append($("<option>").attr("value", "100").append("Top 100 genes"))
								.append($("<option>").attr("value", "200").append("Top 200 genes"))
								.append($("<option>").attr("value", "500").append("Top 500 genes"))
							)
						)
					)
					.append($("<tr>")
						.addClass("head")
						.append($("<td>").append("Term"))
						.append($("<td>").append("PValue"))
						.append($("<td>").append("QValue"))
						.append($("<td>").append("Overlap"))
					)
				)
			)
			//dataset enrichment results
			.append($("<div>")
				.attr("id", "dataset_enrichment_" + queryID)
				.attr("name", "dataset_enrichment_" + queryID)
				.css("width", "100%")
				.hide()
				.append($("<table>")
					.addClass("main")
					.css("font-size", "12px")
					.css("width", "100%")
					.append($("<tr>")
						.addClass("head")
						.append($("<td>")
							.attr("colspan", "4")
							.append("Prioritized Dataset Enrichments<br>in")
							.append($("<select>")
								.attr("name", "top_datasets_" + queryID)
								.attr("id", "top_datasets_" + queryID)
								.attr("qid", queryID)
								.append($("<option>").attr("value", "20").append("Top 20 datasets"))
								.append($("<option>").attr("value", "50").append("Top 50 datasets"))
								.append($("<option>").attr("value", "100").append("Top 100 datasets"))
								.append($("<option>").attr("value", "200").append("Top 200 datasets"))
							)
						)
					)
					.append($("<tr>")
						.addClass("head")
						.append($("<td>").append("Term"))
						.append($("<td>").append("PValue"))
						.append($("<td>").append("QValue"))
						.append($("<td>").append("Overlap"))
					)
				)
			)
	);

	get_ready();
}
