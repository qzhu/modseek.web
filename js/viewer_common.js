/* for sessionStorage (HTML5) */
function StorageListFind(key, item){
	var s = sessionStorage.getItem(key);
	var ss = s.split(";");
	for(var i=0; i<ss.length; i++){
		if(ss[i]==item){
			return true;
		}
	}
	return false;
}

function StorageListAdd(key, item){
	if(!StorageListFind(key, item)){
		var t;
		var s = ((t=sessionStorage.getItem(key))=="") ? item : t + ";" + item;
		sessionStorage.setItem(key, s);
	}
}

function StorageListDelete(key, item){
	var s = sessionStorage.getItem(key);
	var ss = s.split(";");
	var ss_new = []
	for(var i=0; i<ss.length; i++){
		if(ss[i]==item) continue;
		ss_new.push(ss[i]);
	}
	sessionStorage.setItem(key, ss_new.join(";"));
}

function show_hide_rows(targets, searchTerm){
	targets.each(function(index, row){
		var allCells = $(row).find('td');
		if(allCells.length > 0){
			var found = false;
			allCells.each(function(index, td){
				var regExp = new RegExp(searchTerm, 'i');
				if(regExp.test($(td).text())){
					found = true;
					return false;
				}
			});
			if(found == true)
				$(row).show();
			else 
				$(row).hide();
		}
	});
}


function select_none_rows(targets, list2, isShort){
	targets.each(function(index, row){
		if($(row).is(":visible")){
			var allCells = $(row).find('.checkbox_large');
			var gn = allCells.attr("name_short");
			var dname = allCells.attr("name");
			allCells.attr("src", "../img/red_unchecked.png");
			if(isShort)
				StorageListDelete(list2, gn);
			else
				StorageListDelete(list2, dname);
		}
	});
}

function select_all_rows(targets, list2, isShort){
	targets.each(function(index, row){
		if($(row).is(":visible")){
			var allCells = $(row).find('.checkbox_large');
			allCells.attr("src", "../img/red_checked.png");
			var gn = allCells.attr("name_short");
			var dname = allCells.attr("name");
			if(isShort)
				StorageListAdd(list2, gn);
			else
				StorageListAdd(list2, dname);
		}
	});
}

function judge_color(vi, up_clip, down_clip, size, unit_length){
	var r = 0;
	var g = 0;
	var b = 0;
	var isAbsent = false;
	if(vi>up_clip && vi<326){
		r = color_red[size-1];
		g = color_green[size-1];
		b = color_blue[size-1];
	}else if(vi<down_clip){
		r = color_red[0];
		g = color_green[0];
		b = color_blue[0];
	}else if(vi>326){
		r = neutral_color_red;
		g = neutral_color_green;
		b = neutral_color_blue;
		isAbsent = true;
	}else{
		var level = Math.round(1.0*(vi-down_clip)/unit_length);
		if(level<0){
			level = 0;
		}else if(level>=size){
			level = size - 1;
		}
		r = color_red[level];
		g = color_green[level];
		b = color_blue[level];
	}
	return [r,g,b,isAbsent];
}

function getOffsetX(select_element, pageX, s){
	var posX = $(select_element).offset().left;
	var X = pageX - posX;
	var x=0;
	var xi=0;
	for(xi=0; xi<s; xi++){
		//if coexpression, dsetSizes[xi] holds the pixel size 
		//(ie horizUnit=1, horizSpacing=0)
		if(x+dsetSizes[xi]*horizUnit+horizSpacing>X) break;
		x+=dsetSizes[xi]*horizUnit + horizSpacing;		
	}
	return xi;
}

function getOffsetY(select_element, pageY, s){
	var posY=$(select_element).offset().top;
	var Y = pageY - posY;
	var y = 0;
	var yi = 0;
	for(yi=0; yi<s; yi++){
		if(y+vertUnit>Y) break;
		y+=vertUnit;
	}
	return yi;
}
