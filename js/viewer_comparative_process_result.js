function process_dataset_enrichment(num, org, sess, aa){
	var element_name = "#dataset_enrichment_" + num;
	$(element_name).find("table.main").find("tr.content").remove();
	$(element_name).find("table.main").find("tr.head").first()
		.css("text-align", "center")
		.css("font-size", "16px");

	for(var vi=0; vi<aa.length; vi++){
		if(aa[vi]=="") continue;
		var vt = aa[vi].split("\t");
		var term = vt[0];
		var pval = vt[1];
		var qval = vt[2];
		var overlap = vt[5];
		var bg_color = bg_color_array[num-1];
		if(vi%2==0){
			bg_color = "white";
		}
		var lighter = "rgb(100,100,100)";
		$(element_name).find("table.main")
			.append($("<tr>")
				.addClass("content")
				.css("background-color", bg_color)
				.append($("<td>")
					.append($("<a>")
						.attr("id", "overlap_" + vi)
						.attr("href", "#")
						.attr("name", term)
						.css("text-decoration", "underline")
						.text(term)
						.click(function(e){
							var id_name=$(this).attr("id");
							var term_name = $(this).attr("name");
							var goldstd = "mesh_disease_anatomy";
							var top = top_dataset_array[num-1];
							if(org=="yeast" || org=="fly" || org=="worm" || org=="zebrafish"){
								goldstd = "mesh";
							}
							$("#dataset_enrichment_" + num).hide("slide");
							$.post("/modSeek/servlet/DatasetEnrichment", {organism:org, sessionID:sess, top:top, goldstd:goldstd, show_overlap:"n", term:$(this).attr("name"), overlap_only:"y"}, function(responseText){
								var dset_list = responseText.replace(/\s*$/g, "").split(" ");
								var dset_string = dset_list.join("|");
								$.get("/modSeek/servlet/GetScoreServlet", {organism:org, sessionID:sess, type:"dataset_weight", page:"0", per_page:"200", keyword:"all_sorted", query:dset_string}, function(responseText){
									var aa = responseText.split("\n");
									process_dataset_result(num, org, sess, aa, "Retrieved datasets in " + term_name);
								});
							});
						})
					)		
				)
				.append($("<td>")
					.css("color", lighter)
					.append(pval)
				)
				.append($("<td>")
					.css("color", lighter)
					.append(qval)
				)
				.append($("<td>")
					.css("color", lighter)
					.css("padding-top", "5px")
					.css("padding-bottom", "5px")
					.append(overlap)
				)
			)
		;
	}
	$(element_name).show("fade");
}

function process_gene_enrichment(num, org, sess, aa){
	var element_name = "#gene_enrichment_" + num;
	$(element_name).find("table.main").find("tr.content").remove();
	$(element_name).find("table.main").find("tr.head").first()
		.css("text-align", "center")
		.css("font-size", "16px");

	for(var vi=0; vi<aa.length; vi++){
		if(aa[vi]=="") continue;
		var vt = aa[vi].split("\t");
		var term_orig = vt[0];
		var term = vt[0].replace(/_/g, " ");
		var pval = vt[1];
		var qval = vt[2];
		var overlap = vt[5];
		var bg_color = bg_color_array[num-1];
		if(vi%2==0){
			bg_color = "white";
		}
		var lighter = "rgb(100,100,100)";
		$(element_name).find("table.main")
			.append($("<tr>")
				.addClass("content")
				.css("background-color", bg_color)
				.append($("<td>")
					.append($("<a>")
						.attr("id", "overlap_" + vi)
						.attr("href", "#")
						.attr("name", term_orig)
						.css("text-decoration", "underline")
						.text(term)
						.click(function(e){
							var id_name=$(this).attr("id");
							var term_name = $(this).attr("name");
							$("#gene_enrichment_" + num).hide("slide");
							var top = top_gene_array[num-1];
							$.post("/modSeek/servlet/GeneEnrichment", {organism:org, sessionID:sess, top:top, goldstd:"sans_regulation_bp", show_overlap:"n", term:$(this).attr("name"), overlap_only:"y", filter_by_pval:"-1", pval_cutoff:"-1", specify_genes:""}, function(responseText){
								var gene_list = responseText.replace(/\s*$/g, "").split(" ");
								var gene_string = gene_list.join("|");
								$.get("/modSeek/servlet/GetScoreServlet", {organism:org, sessionID:sess, type:"gene_score", page:"0", per_page:"500", keyword:"all_sorted", query:gene_string}, function(responseText){
									var aa = responseText.split("\n");
									process_gene_result(num, org, sess, aa, "Retrieved genes in " + term_name.replace(/_/g, " "));
								});
							});
						})
					)		
				)
				.append($("<td>")
					.css("color", lighter)
					.append(pval)
				)
				.append($("<td>")
					.css("color", lighter)
					.append(qval)
				)
				.append($("<td>")
					.css("color", lighter)
					.css("padding-top", "5px")
					.css("padding-bottom", "5px")
					.append(overlap)
				)
			)
		;
	}
	$(element_name).show("fade");
}

function process_dataset_result(num, org, sess, aa, message){
	var element_name = "#dataset_result_" + num;
	$(element_name).find("table.main").find("tr.content").remove();
	if(message=="All Datasets"){
		$(element_name).find("table.main").find("tr.head").first()
			.empty()
			.append($("<td>")
				.attr("colspan", "4")
				.css("text-align", "center")
				.css("font-size", "16px")
				.append("Prioritized Datasets")
			);
		$(element_name).find("table.main").find("tr.navigation")
			.empty()
			.append($("<td>")
				.attr("colspan", "4")
				.append($("<table>")
					.css("width", "100%")
					.append($("<tr>")
						.append($("<td>")
							.css("width", "90%")
						)
						.append($("<td>")
							.css("background-color", "cornflowerblue")
							.css("padding", "2px")
							.css("font-size", "16px")
							.css("text-align", "center")
							.append($("<a>")
								.attr("href", "#")
								.addClass("white_underline")
								.append("&lt;")
								.click(function(){
									page_org_array[num-1] -= 1;
									if(page_org_array[num-1]<0) page_org_array[num-1] = 0;
									var page = page_org_array[num-1];
									get_dataset_result(num, org, sess, page, "20");
									return false;
								})
							)
						)
						.append($("<td>")
							.css("background-color", "cornflowerblue")
							.css("padding", "2px")
							.css("font-size", "16px")
							.css("text-align", "center")
							.append($("<a>")
								.attr("href", "#")
								.addClass("white_underline")
								.append("&gt;")
								.click(function(){
									page_org_array[num-1] += 1;
									var page = page_org_array[num-1];
									get_dataset_result(num, org, sess, page, "20");
									return false;
								})
							)
						)
					)
				)
			);
			
	}else{
		$(element_name).find("table.main").find("tr.navigation").empty();
		$(element_name).find("table.main").find("tr.head").first()
			.empty()
			.append($("<td>")
				.attr("colspan", "4")
				.append($("<table>")
					.css("width", "100%")
					.append($("<tr>")
						.append($("<td>")
							.css("background-color", "cornflowerblue")
							.css("padding", "2px")
							.css("font-size", "10px")
							.append($("<a>")
								.attr("href", "#")
								.addClass("white_underline")
								.append("&lt;&lt; All Dataset Enrichments")
								.click(function(){
									$("#dataset_result_" + num).hide("slide");
									setTimeout(function(){
										$("#dataset_enrichment_" + num).show();
									}, 400);
								})
							)
						)
						.append($("<td>")
							.css("width", "70%")
						)
					)
					.append($("<tr>")
						.append($("<td>")
							.attr("colspan", "2")
							.css("text-align", "center")
							.css("font-size", "16px")
							.append(message)
						)
					)
				)
			);
	}

	for(var vi=1; vi<aa.length; vi++){
		var vt = aa[vi].split("\t");
		var rk = vt[0];
		var ds = vt[1];
		var ds_orig = ds;
		ds = ds.replace(/\W/g, ' ').replace(/_/g, ' ').replace(/ pcl/g, '');
		var pval = vt[3];
		var tit = vt[4];
		var bg_color = bg_color_array[num-1];
		if(vi%2==0){
			bg_color = "white";
		}
		var lighter = "rgb(100,100,100)";
		$(element_name).find("table.main")
			.append($("<tr>")
				.addClass("content")
				.css("background-color", bg_color)
				.append($("<td>")
					.css("color", lighter)
					.append(rk) //rank
				)
				.append($("<td>")
					.css("color", lighter)
					.append(pval) //pvalue
				)
				.append($("<td>")
					.css("color", lighter)
					.append(ds) //dataset name
				)
				.append($("<td>")
					.css("padding-top", "10px")
					.css("padding-bottom", "10px")
					.css("font-size", "12px")
					.append(tit) //title
					.append("&nbsp;&nbsp;")
					.append($("<a>")
						.attr("href", "#")
						.attr("qid", num)
						.attr("did", ds_orig)
						.attr("title", tit)
						.attr("rank", rk)
						.attr("pvalue", pval)
						.attr("id", "dset_item_" + num + "_" + vi)
						.attr("max_id", aa.length)
						.text("Vis")
						.click(function(){
							var n = $(this).attr("qid");
							//$.post("/modSeek/servlet/GetExpressionInfoServlet", {organism:organism_array[n-1], sessionID:sessionID_array[n-1], dataset:$(this).attr("did"), normalize:1, gene:arrayQuery[n-1], cluster:1, show_sample_order:1, show_sample_description:1 }, function(responseText){
								//alert(responseText);
							//});
							process_query_expression(n, organism_array[n-1], sessionID_array[n-1], $(this).attr("did"), arrayQuery[n-1], $(this).attr("id"));

							return false;
						})
					)
				)
			);
	}
	$(element_name).show("fade");
}

function process_gene_result(num, org, sess, aa, message){
	var element_name = "#gene_result_" + num;
	$(element_name).find("table.main").find("tr.content").remove();
	if(message=="All Genes"){
		$(element_name).find("table.main").find("tr.head").first()
			.empty()
			.append($("<td>")
				.attr("colspan", "4")
				.css("text-align", "center")
				.css("font-size", "16px")
				.append("Prioritized Genes")
			);
		$(element_name).find("table.main").find("tr.navigation")
			.empty()
			.append($("<td>")
				.attr("colspan", "4")
				.append($("<table>")
					.css("width", "100%")
					.append($("<tr>")
						.append($("<td>")
							.css("width", "90%")
						)
						.append($("<td>")
							.css("background-color", "cornflowerblue")
							.css("padding", "2px")
							.css("font-size", "16px")
							.css("text-align", "center")
							.append($("<a>")
								.attr("href", "#")
								.addClass("white_underline")
								.append("&lt;")
								.click(function(){
									page_gene_org_array[num-1] -= 1;
									if(page_gene_org_array[num-1]<0) page_gene_org_array[num-1] = 0;
									var page = page_gene_org_array[num-1];
									get_gene_result(num, org, sess, page, "100");
									return false;
								})
							)
						)
						.append($("<td>")
							.css("background-color", "cornflowerblue")
							.css("padding", "2px")
							.css("font-size", "16px")
							.css("text-align", "center")
							.append($("<a>")
								.attr("href", "#")
								.addClass("white_underline")
								.append("&gt;")
								.click(function(){
									page_gene_org_array[num-1] += 1;
									var page = page_gene_org_array[num-1];
									get_gene_result(num, org, sess, page, "100");
									return false;
								})
							)
						)
					)
				)
			);
			
	}else{
		$(element_name).find("table.main").find("tr.navigation").empty();
		$(element_name).find("table.main").find("tr.head").first()
			.empty()
			.append($("<td>")
				.attr("colspan", "4")
				.append($("<table>")
					.css("width", "100%")
					.append($("<tr>")
						.append($("<td>")
							.css("background-color", "cornflowerblue")
							.css("padding", "2px")
							.css("font-size", "10px")
							.append($("<a>")
								.attr("href", "#")
								.addClass("white_underline")
								.append("&lt;&lt; All Gene Enrichments")
								.click(function(){
									$("#gene_result_" + num).hide("slide");
									setTimeout(function(){
										$("#gene_enrichment_" + num).show();
									}, 400);
								})
							)
						)
						.append($("<td>")
							.css("width", "70%")
						)
					)
					.append($("<tr>")
						.append($("<td>")
							.attr("colspan", "2")
							.css("text-align", "center")
							.css("font-size", "16px")
							.append(message)
						)
					)
				)
			);
	}

	for(var vi=1; vi<aa.length; vi++){
		var vt = aa[vi].split("\t");
		var rk = vt[0];
		var ds = vt[1];
		ds = ds.replace(/\W/g, ' ').replace(/_/g, ' ').replace(/ pcl/g, '');
		var pval = vt[3];
		var tit = vt[5];
		var bg_color = bg_color_array[num-1];
		if(vi%2==0){
			bg_color = "white";
		}
		var lighter = "rgb(100,100,100)";
		$(element_name).find("table.main")
			.append($("<tr>")
				.addClass("content")
				.css("background-color", bg_color)
				.append($("<td>")
					.css("color", lighter)
					.append(rk)
				)
				.append($("<td>")
					.css("color", lighter)
					.append(pval)
				)
				.append($("<td>")
					.css("color", lighter)
					.append(ds)
				)
				.append($("<td>")
					.css("padding-top", "5px")
					.css("padding-bottom", "5px")
					.css("font-size", "12px")
					.append(tit)
				)
			);
	}
	$(element_name).show("fade");
}
