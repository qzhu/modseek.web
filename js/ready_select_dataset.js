function Set() {
	this.content = {};
}
Set.prototype.add = function(val) {
	this.content[val]=true;
}
Set.prototype.remove = function(val) {
	delete this.content[val];
}
Set.prototype.contains = function(val) {
	return (val in this.content);
}
Set.prototype.asArray = function() {
	var res = [];
	for (var val in this.content) res.push(val);
	return res;
}

function select_across_all_pages(responseText, applyMode){
	var v = responseText.split(";");
	var d2 = [];
	for(var i=0; i<v.length; i++){
		var vt = v[i].split("\t");
		d2.push(vt[0]);
	}
	d2.sort();
	var dt = sessionStorage.getItem("export_dsetname_select_list").split(";");
	dt.sort();
	var remain1 = []; var remain2 = []; var remain3 = [];
	var n1 = d2.length; var n2 = dt.length;
	var vi = 0; var vj = 0;
	while(vi < n1 && vj < n2){
		if(d2[vi] > dt[vj]){
			remain1.push(dt[vj]); vj++;
		}else if(dt[vj] > d2[vi]){
			remain2.push(d2[vi]); vi++;
		}else{
			remain3.push(d2[vi]); vi++; vj++;
		}
	}
	while(vi < n1){
		remain2.push(d2[vi]); vi++;
	}
	while(vj < n2){
		remain1.push(dt[vj]); vj++;
	}
	if(applyMode==0){ //Select None
		sessionStorage.setItem("export_dsetname_select_list", remain1.join(";"));
	}else if(applyMode==1){ //Select All
		var rr = [];
		rr = rr.concat(remain1); rr = rr.concat(remain2); rr = rr.concat(remain3);
		sessionStorage.setItem("export_dsetname_select_list", rr.join(";"));
	}
}

function process_check_dataset(responseText){
	var v = responseText.split(";");
	var tt = $("<table>")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.addClass("basic_font");
	tt.append($("<tr>")
		.append($("<td>")
			.attr("colspan", "3")
			.append("Here are the datasets you selected:")
		)
	)
	.append($("<tr>").addClass("table_header_underline")
		.append($("<td>"))
		.append($("<td>").text("Dataset"))
		.append($("<td>").text("Description"))
	);

	for(var i=0; i<v.length; i++){
		var vt = v[i].split("\t");
		//vt[0] - dataset id.dataset platform
		//vt[1] - dataset description
		var dset_id = "";
		if(vt[0].indexOf("_")==-1) dset_id = vt[0].split(".")[0];
		else dset_id = vt[0].split("_")[0];

		var dd_str = "";
		if(StorageListFind("export_dsetname_select_list", vt[0]))
			dd_str = "../img/red_checked.png";
		else
			dd_str = "../img/red_unchecked.png";

		tt.append($("<tr>")
			.append($("<td>")
				.css("vertical-align", "top")
				.append($("<img>")
					.addClass("checkbox_large")
					.attr("src", dd_str)
					.attr("id", "export_enable_d-" + vt[0])
					.attr("name", vt[0])
					.attr("name_short", dset_id)
					.click(function(){
						$("#apply_all").hide();
						var state = $(this).attr("src");
						var dname = $(this).attr("name");
						if(state=="../img/red_unchecked.png"){
							$(this).attr("src", "../img/red_checked.png");
							StorageListAdd("export_dsetname_select_list", dname);
						}else{
							$(this).attr("src", "../img/red_unchecked.png");
							StorageListDelete("export_dsetname_select_list", dname);
						}
						return false;
					})
				)
			)
			.append($("<td>")
				.css("vertical-align", "top").css("width", "120px").text(vt[0]))
			.append($("<td>").text(vt[1]))
		);
	}
	return tt;
}

function process_dataset_score(responseText){
	var v = responseText.split(";");
	var tt = $("<table>")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.addClass("basic_font");
	tt.append($("<tr>")
		.append($("<td>")
			.attr("colspan", "4")
			.append("What datasets would you like to limit to?")
		)
	)
	.append($("<tr>").addClass("table_header_underline")
		.append($("<td>"))
		.append($("<td>").text("Rank"))
		.append($("<td>").text("Dataset"))
		.append($("<td>").text("Description"))
	);

	for(var i=0; i<v.length; i++){
		var vt = v[i].split("\t");
		//vt[0] - rank
		//vt[1] - dataset id.dataset platform
		//vt[2] - dataset description
		var dset_id = "";
		if(vt[1].indexOf("_")==-1) dset_id = vt[1].split(".")[0];
		else dset_id = vt[1].split("_")[0];

		var dd_str = "../img/red_unchecked.png";
		if(StorageListFind("export_dsetname_select_list", vt[1])){
			dd_str = "../img/red_checked.png";
		}else{
			dd_str = "../img/red_unchecked.png";
		}
		tt.append($("<tr>")
			.append($("<td>")
				.append($("<img>")
					.addClass("checkbox_large")
					.attr("src", dd_str)
					.attr("id", "export_enable_d-" + vt[1])
					.attr("name", vt[1])
					.attr("name_short", dset_id)
					.click(function(){
						$("#apply_all").hide();
						var state = $(this).attr("src");
						var dname = $(this).attr("name");
						if(state=="../img/red_unchecked.png"){
							$(this).attr("src", "../img/red_checked.png");
							StorageListAdd("export_dsetname_select_list", dname);
						}else{
							$(this).attr("src", "../img/red_unchecked.png");
							StorageListDelete("export_dsetname_select_list", dname);
						}
						return false;
					})
				)
			)
			.append($("<td>").text(vt[0]))
			.append($("<td>").text(vt[1]))
			.append($("<td>").text(vt[2]))
		);
	}
	return tt;
}

function process_dataset_annot(responseText){
	var v = responseText.split(";");
	var tt = $("<table>")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.addClass("basic_font");
	
	tt.append($("<tr>")
		.append($("<td>")
			.attr("colspan", "3")
			.append("What datasets would you like to limit to?")
		)
	)
	.append($("<tr>")
		.addClass("table_header_underline")
		.append($("<td>")
		)
		.append($("<td>")
			.text("Tissue/Cell/Disease Types")
		)
		.append($("<td>")
			.text("Number of datasets")
		)
	);

	for(var i=0; i<v.length; i++){
		var vt = v[i].split("\t");
		var dd_str = "../img/red_unchecked.png";
		if(StorageListFind("datasetname_select_list", vt[0]))
			dd_str = "../img/red_checked.png";
		else
			dd_str = "../img/red_unchecked.png";
		tt.append($("<tr>")
			.append($("<td>").append($("<img>")
				.addClass("checkbox_large")
				.attr("src", dd_str)
				.attr("id", "enable_dd_" + i.toString())
				.attr("name", vt[0])
				.attr("name_short", i.toString())
				.click(function(){
					$("#apply_all").hide();
					var state = $(this).attr("src");
					var dname = $(this).attr("name");
					if(state=="../img/red_unchecked.png"){
						$(this).attr("src", "../img/red_checked.png");
						StorageListAdd("datasetname_select_list", dname);
					}else{
						$(this).attr("src", "../img/red_unchecked.png");
						StorageListDelete("datasetname_select_list", dname);
					}
					return false;
				})
				)
			)
			.append($("<td>").text(vt[0]))
			.append($("<td>").text(vt[1]))			
		);
	}
	return tt;
}

$(document).ready(function(){

$(".select_dataset").click({isMain:"false"}, function(e){
	var disMain = e.data.isMain;

	var newt = $("<table>")
		.addClass("basic_font")
		.append($("<tr>")
			.append($("<td>")
				.append("How do you want to refine search? ")
			)
		);

	/*dark color theme */
	/*var button_bg_color = "#330033";
	var button_bg_color_selected = "#330099";
	var button_border_color = "#339933";
	*/
	//light color theme
	var button_bg_color = "#eeeeee";
	var button_bg_color_selected = "#c8c8c8";
	var button_border_color = "#525252";

	var numPage = -1;
	var currentPage = 1;
	var dsetSelection = "";
	var searchMode = -1; //0-4
	var applyMode = 0; //0 - None, 1 - All

	newt = newt.after($("<table>")
			.append($("<tr>")
				.append($("<td>")
					.css("background-color", button_bg_color)
					.css("font-size", "12px")
					.css("text-align", "center")
					.css("padding", "5px 5px 3px 5px")
					.css("border-style", "solid")
					.css("border-width", "1px 2px 2px 1px")
					.css("border-color", button_border_color)
					.attr("id", "dset_by_categories")
					.append($("<a>")
						.attr("href", "#")
						.text("Limit datasets by tissue/cell/disease types")
						.click(function(){
							$("#apply_all").hide();
							$("#dset_by_categories")
								.css("background-color", button_bg_color_selected);
							if(disMain=="false"){
								$("#dset_by_order")
									.css("background-color", button_bg_color);
							}
							$("#dset_no_refinement")
								.css("background-color", button_bg_color);
							$("#select_dataset_window")
								.html("LOADING TABLE ... (approx. 10 seconds)");
							$("#gene_search_group").hide();
							sessionStorage.setItem("datasetname_select_list", "");
							currentPage = 1;
							setTimeout(function(){
								searchMode = 3;	
								$.post("../servlet/ViewDataset", {organism:organism, mode:3, page:currentPage}, function(responseText){
								var vx = responseText.indexOf(";");
								numPage = parseInt(responseText.substring(0, vx));
								var vz = responseText.substring(vx+1);
								$("#gene_search_group").show();
								var t = process_dataset_annot(vz);
								$("#d_page").css("display", "inline").find("option").remove();
								$("#c_page").hide();
								for(var ti=0; ti<numPage; ti++){
									if(ti==currentPage-1)
										$("#d_page").append($("<option selected='selected'>").text((ti+1).toString()));
									else
										$("#d_page").append($("<option>").text((ti+1).toString()));
								}
								$("#select_dataset_window").html(t);
								$("#dset_save").attr("mode", "category");
								$("#dset_check").show();
								});
							}, 500);
							return false;
						})
					)
				)
			)
		);

	if(disMain=="false"){
		newt = newt.after($("<table>")
			.append($("<tr>")
				.append($("<td>")
					.css("background-color", button_bg_color)
					.css("font-size", "12px")
					.css("text-align", "center")
					.css("padding", "5px 5px 3px 5px")
					.css("border-style", "solid")
					.css("border-width", "1px 2px 2px 1px")
					.css("border-color", button_border_color)
					.attr("id", "dset_by_order")
					.append($("<a>")
						.attr("href", "#")
						.text("Limit datasets by rank")
						.click(function(){
							$("#apply_all").hide();
							$("#dset_by_order")
								.css("background-color", button_bg_color_selected);
							$("#dset_by_categories")
								.css("background-color", button_bg_color);
							$("#dset_no_refinement")
								.css("background-color", button_bg_color);
							$("#select_dataset_window")
								.html("LOADING TABLE ... (approx. 10 seconds)");
							$("#gene_search_group").hide();
							setTimeout(function(){	
							currentPage = 1;
							searchMode = 4;

							$.post("../servlet/ViewDataset", {organism:organism, mode:4, sessionID:sessionID, page:currentPage, 
							print_annot:"n"}, function(responseText){
								var v = responseText.split(";");
								var d2 = [];
								for(var i=0; i<v.length; i++){
									var vt = v[i].split("\t");
									d2.push(vt[0]);
								}
								dsetSelection = d2.join(";"); //dsetSelection important, do not erase
								sessionStorage.setItem("export_dsetname_select_list", dsetSelection);

								$.post("../servlet/ViewDataset", {organism:organism, mode:4, sessionID:sessionID, page:currentPage, 
								print_annot:"y"}, function(responseText){
									$("#gene_search_group").show();
									var vx = responseText.indexOf(";");
									numPage = parseInt(responseText.substring(0, vx));
									var vz = responseText.substring(vx+1);
									var t = process_dataset_score(vz);
									$("#c_page").css("display", "inline").find("option").remove();
									$("#d_page").hide();
									for(var ti=0; ti<numPage; ti++){
										if(ti==currentPage-1)
											$("#c_page").append($("<option selected='selected'>").text((ti+1).toString()));
										else
											$("#c_page").append($("<option>").text((ti+1).toString()));
									}
									$("#select_dataset_window").html(t).scrollTop(0);
									$("#dset_save").attr("mode", "rank");
									$("#dset_check").hide();
								});
							});
							}, 500);
							return false;
						})
					)
				)
			)
			.append($("<tr>")
				.append($("<td>")
					.text("Or")
				)
			)
		);
	}

	newt = newt.after($("<table>")
			.append($("<tr>")
				.append($("<td>")
					.css("background-color", button_bg_color)
					.css("font-size", "12px")
					.css("text-align", "center")
					.css("padding", "5px 5px 3px 5px")
					.css("border-style", "solid")
					.css("border-width", "1px 2px 2px 1px")
					.css("border-color", button_border_color)
					.attr("id", "dset_no_refinement")
					.append($("<a>")
						.attr("href", "#")
						.text("No refinement. Use all datasets available in SEEK.")
						.click(function(){
							$("#apply_all").hide();
							$("#dset_no_refinement")
								.css("background-color", button_bg_color_selected);
							$("#dset_by_categories")
								.css("background-color", button_bg_color);
							if(disMain=="false"){
								$("#dset_by_order")
									.css("background-color", button_bg_color);
							}
							sessionStorage.setItem("export_dsetname_select_list", "");
							sessionStorage.setItem("datasetname_select_list", "");
							$("#gene_search_group").hide();
							$("#select_dataset_window").html("This option is selected!");
							$("#dset_save").attr("mode", "no_refine");
							return false;
						})
					)
				)
			)
		);

	var topt = $("<table>")
		.addClass("basic_font")
		.append($("<tr>")
			.append($("<td>")
				.append($("<div>")
					.attr("id", "gene_search_group")
					.hide()
					.append($("<table>")
					  .append($("<tr>")
						.append($("<td>").html("&nbsp;"))
						.append($("<td>")
							.attr("colspan", "5")
							.append($("<div>")
								.attr("name", "apply_all")
								.attr("id", "apply_all")
								.css("text-align", "center")
								.css("background-color", "orange")
								.append("Apply to all pages?" )
								.append($("<a>")
									.attr("href", "#").text("Y")
									.click(function(){
										if(searchMode == 0 || searchMode == 1){
											var searchTerm = $("#gene_search").val();
											$.post("../servlet/ViewDataset", {organism:organism, mode:1, dset:dsetSelection, 
											dset_keyword:searchTerm, print_annot:"n"}, function(responseText){
												select_across_all_pages(responseText, applyMode);
											});
										}
										else if(searchMode==4){
											var searchTerm = $("#gene_search").val();
											$.post("../servlet/ViewDataset", {organism:organism, mode:4, sessionID:sessionID, 
											dset_keyword:searchTerm, print_annot:"n"}, function(responseText){
												select_across_all_pages(responseText, applyMode);
											});
										}
										$("#apply_all").hide("slow");
										return false;
									})
								)
								.append(" / ")
								.append($("<a>")
									.attr("href", "#").text("N")
									.click(function(){
										$("#apply_all").hide("slow");
										return false;
									})
								)
								.hide()
							)
						)
					  )
					  .append($("<tr>")
					    .append($("<td>")
							.css("width", "100px")
							.append("Page ")
							.append($("<select>")
								.attr("name", "d_page")
								.attr("id", "d_page")
								.css("display", "inline")
								.css("font-size", "12px")
								.change(function(){
									$("#apply_all").hide();
									currentPage = parseInt($("#d_page option:selected").text());
									setTimeout(function(){	
										if(searchMode == 3){
											$.post("../servlet/ViewDataset", {organism:organism, mode:3, page:currentPage}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = responseText.substring(vx+1);
											var t = process_dataset_annot(vz);
											$("#select_dataset_window").html(t).scrollTop();
											});
										}else if(searchMode == 2){
											var searchTerm = $("#gene_search").val();
											$.post("../servlet/ViewDataset", {organism:organism, mode:2, page:currentPage, 
											category_keyword:searchTerm}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = responseText.substring(vx+1);
											var t = process_dataset_annot(vz);
											$("#select_dataset_window").html(t).scrollTop();
											});
										}	
									}, 500);
								})
								.hide()
							)
							.append($("<select>")
								.attr("name", "c_page")
								.attr("id", "c_page")
								.css("font-size", "12px")
								.css("display", "inline")
								.change(function(){
									$("#apply_all").hide();
									currentPage = parseInt($("#c_page option:selected").text());
									setTimeout(function(){	
										if(searchMode == 0){
											var sxs = sessionStorage.getItem("datasetname_select_list");
											if(sxs==""){
												alert("No datasets selected!");
												return false;
											}
											var dd = sxs.split(";");
											var s2 = [];
											for(var vi=0; vi<dd.length; vi++)
												s2.push(dd[vi]);
											$.post("../servlet/ViewDataset", {organism:organism, mode:0, print_annot:"y", 
											page:currentPage, category:s2.join(";")}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = responseText.substring(vx+1);
											var t = process_check_dataset(vz);
											$("#select_dataset_window").html(t).scrollTop(0);
											});
										}else if(searchMode == 1){
											var searchTerm = $("#gene_search").val();
											$.post("../servlet/ViewDataset", {organism:organism, mode:1, dset:dsetSelection, 
											page:currentPage, dset_keyword:searchTerm, print_annot:"y"}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = responseText.substring(vx+1);
											var t = process_check_dataset(vz);
											$("#select_dataset_window").html(t).scrollTop(0);
											});
										}else if(searchMode == 4){
											var searchTerm = $("#gene_search").val();
											$.post("../servlet/ViewDataset", {organism:organism, mode:4, sessionID:sessionID, page:currentPage, 
											dset_keyword:searchTerm, print_annot:"y"}, function(responseText){
											var vx = responseText.indexOf(";");
											var vz = responseText.substring(vx+1);
											var t = process_dataset_score(vz);
											$("#select_dataset_window").html(t).scrollTop();
											});
										}
									}, 500);
								})
								.hide()
							)
						)
						.append($("<td>")
							.append($("<a>")
								.attr("href", "#")
								.addClass("basic_font")
								.attr("id", "clear_dset_selection")
								.text("None")
								.attr("title", "Clear selection")
								.click(function(){
									if($("#dset_save").attr("mode")=="category"){
										select_none_rows($("#select_dataset_window table").find("tr").slice(2), 
										"datasetname_select_list", false);
									}else if($("#dset_save").attr("mode")=="rank"){
										applyMode = 0;
										select_none_rows($("#select_dataset_window table").find("tr").slice(2), 
										"export_dsetname_select_list", false);
										if($("#c_page option").size()>1)
											$("#apply_all").show("slow");
									}
									return false;
								})
							)
						)
						.append($("<td>").css("width", "10px"))
						.append($("<td>")
							.append($("<a>")
								.attr("href", "#")
								.addClass("basic_font")
								.attr("id", "all_dset_selection")
								.text("All")
								.attr("title", "Select all")
								.click(function(){
									if($("#dset_save").attr("mode")=="category"){
										select_all_rows($("#select_dataset_window table").find("tr").slice(2), 
										"datasetname_select_list", false);
									}else if($("#dset_save").attr("mode")=="rank"){
										applyMode = 1;
										select_all_rows($("#select_dataset_window table").find("tr").slice(2), 
										"export_dsetname_select_list", false);
										if($("#c_page option").size()>1)
											$("#apply_all").show("slow");
									}
									return false;
								})
							)
						)
						.append($("<td>").css("width", "10px"))
						.append($("<td>").append($("<input>")
								.attr("type", "text")
								.attr("name", "gene_search")
								.attr("id", "gene_search")
								.css("font", "12px Arial")
								.attr("size", "15")
								.keypress(function(e){
									if(e.which==13) $("#gene_search_button").click();
								})
							)
							.append($("<input>")
								.attr("type", "button")
								.attr("name", "gene_search_button")
								.attr("id", "gene_search_button")
								.attr("value", ">")
								.attr("title", "Search")
								.css("font", "12px Arial")
								.click(function(){
									$("#apply_all").hide();
									var searchTerm = $("#gene_search").val();
									currentPage = 1;
									setTimeout(function(){	
										if($("#dset_save").attr("mode")=="category"){
											searchMode = 2;
											$.post("../servlet/ViewDataset", {organism:organism, mode:2, 
											category_keyword:searchTerm, page:currentPage}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = "";
											var t = "No datasets found. Search again.";
											if(numPage>0){
												vz = responseText.substring(vx+1);
												t = process_dataset_annot(vz);
												$("#d_page").css("display", "inline").find("option").remove();
												$("#c_page").hide();
												for(var ti=0; ti<numPage; ti++){
													if(ti==currentPage-1)
														$("#d_page").append($("<option selected='selected'>").text((ti+1).toString()));
													else
														$("#d_page").append($("<option>").text((ti+1).toString()));
												}
											}
											$("#select_dataset_window").html(t).scrollTop(0);
											$("#dset_check").show();
											});
										}
										else if($("#dset_save").attr("mode")=="rank" && searchMode == 1){
											$.post("../servlet/ViewDataset", {organism:organism, mode:1, print_annot:"y", dset:dsetSelection, 
											dset_keyword:searchTerm, page:currentPage}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = "";
											var t = "No datasets found. Search again.";
											if(numPage>0){
												vz = responseText.substring(vx+1);
												t = process_check_dataset(vz);
												$("#c_page").css("display", "inline").find("option").remove();
												$("#d_page").hide();
												for(var ti=0; ti<numPage; ti++){
													if(ti==currentPage-1)
														$("#c_page").append($("<option selected='selected'>").text((ti+1).toString()));
													else
														$("#c_page").append($("<option>").text((ti+1).toString()));
												}
											}
											$("#select_dataset_window").html(t).scrollTop(0);
											$("#dset_check").hide();
											});
										}
										else if($("#dset_save").attr("mode")=="rank" && searchMode == 4){
											$.post("../servlet/ViewDataset", {organism:organism, mode:4, sessionID:sessionID, print_annot:"y",
											dset_keyword:searchTerm, page:currentPage}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = "";
											var t = "No datasets found. Search again.";
											if(numPage>0){
												vz = responseText.substring(vx+1);
												t = process_dataset_score(vz);
												$("#c_page").css("display", "inline").find("option").remove();
												$("#d_page").hide();
												for(var ti=0; ti<numPage; ti++){
													if(ti==currentPage-1)
														$("#c_page").append($("<option selected='selected'>").text((ti+1).toString()));
													else
														$("#c_page").append($("<option>").text((ti+1).toString()));
												}
											}
											$("#select_dataset_window").html(t).scrollTop(0);
											$("#dset_check").hide();
											});
										}
									}, 500);
									return false;
								})
							)
							.append($("<input>")
								.attr("type", "button")
								.attr("name", "gene_search_clear")
								.attr("id", "gene_search_clear")
								.attr("value", "X")
								.attr("title", "Clear search box")
								.css("font", "12px Arial")
								.click(function(){
									$("#apply_all").hide();
									$("#gene_search").attr("value", "");
									$("#gene_search_button").click();
									return false;
								})
							)
						)
						.append($("<td>").css("width", "10px"))
						.append($("<td>")
							.append($("<a>")
								.attr("href", "#")
								.addClass("basic_font")
								.attr("id", "dset_check")
								.text("Check Selection")
								.attr("title", "Check dataset selections")
								.click(function(){
									$("#apply_all").hide();
									var sxs = sessionStorage.getItem("datasetname_select_list"); //exact category names
									if(sxs==""){
										alert("No datasets selected!");
										return false;
									}
									var dd = sxs.split(";");
									var s2 = [];
									for(var vi=0; vi<dd.length; vi++)
										s2.push(dd[vi]);
									sessionStorage.setItem("export_dsetname_select_list", "");
									currentPage = 1;
									setTimeout(function(){
									searchMode = 0;
									$.post("../servlet/ViewDataset", {organism:organism, mode:0, print_annot:"n", 
									category:s2.join(";")}, function(responseText){
										var v = responseText.split(";");
										var d2 = [];
										for(var i=0; i<v.length; i++){
											var vt = v[i].split("\t");
											d2.push(vt[0]);
										}
										dsetSelection = d2.join(";"); //dsetSelection important, do not erase
										sessionStorage.setItem("export_dsetname_select_list", dsetSelection);

										$.post("../servlet/ViewDataset", {organism:organism, mode:0, print_annot:"y", 
										page:currentPage, category:s2.join(";")}, function(responseText){
											var vx = responseText.indexOf(";");
											numPage = parseInt(responseText.substring(0, vx));
											var vz = responseText.substring(vx+1);
											var t = process_check_dataset(vz);
											$("#c_page").css("display", "inline").find("option").remove();
											$("#d_page").hide();
											for(var ti=0; ti<numPage; ti++){
												if(ti==currentPage-1)
													$("#c_page").append($("<option selected='selected'>").text((ti+1).toString()));
												else
													$("#c_page").append($("<option>").text((ti+1).toString()));
											}
											$("#select_dataset_window").html(t).scrollTop(0);
											$("#dset_save").attr("mode", "rank");
											$("#dset_check").hide();
											searchMode = 1;
										});
									});
									}, 500);
									return false;
								})
							)
						)
					  )
					)
				)
			)
		);

	var ta = $("<div>")
		.attr("id", "select_dataset_window")
		.css("width", "500px")
		.css("height", "300px")
		.css("overflow", "auto")
		.append($("<font>").addClass("basic_font").text(""));

	var ac = $("<table>")
			.append($("<tr>")
				.append($("<td>")
					.css("background-color", button_bg_color)
					.css("font-size", "12px")
					.css("text-align", "center")
					.css("padding", "5px 5px 3px 5px")
					.css("border-style", "solid")
					.css("border-width", "1px 2px 2px 1px")
					.css("border-color", button_border_color)
					.attr("id", "dset_save")
					.attr("mode", "rank")
					.append($("<a>")
						.attr("href", "#")
						.text("Refine")
						.click(function(){
							$("#apply_all").hide();
							var s = "";
							var ss = "";
							if($("#dset_save").attr("mode")=="category"){
								s = sessionStorage.getItem("datasetname_select_list");
								ss = s.split(";");
								if(ss==""){
									alert("Please select some datasets!");
									return false;
								}
								sessionStorage.setItem("dset_search_mode", "category");
								category_search2(ss, true);
							}else if($("#dset_save").attr("mode")=="rank"){
								//just simple dataset search, using rank
								s = sessionStorage.getItem("export_dsetname_select_list");
								ss = s.split(";");
								if(ss==""){
									alert("Please select some datasets!");
									return false;
								}
								sessionStorage.setItem("dset_search_mode", "rank");
								category_search2(ss, false);
							}else{ //mode is no_refine
								sessionStorage.setItem("dset_search_mode", "all");
								$("#next_button").trigger("click");
							}
						})
					)
				)
			);

	ta = newt.after("<p>").after(ta).after(topt).after(ac);
	var tit = "<font style=\"font-family:Arial; font-size:12px;\">Refine search: " + 
		"limit datasets to a specific type</font>";	
		
	$("#somediv").qtip("option", {"content.text": ta});
	$("#somediv").qtip("option", {"content.title.text": tit});
	$("#somediv").trigger("click");
	
	return false;
});



});
