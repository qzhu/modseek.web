var toNormalize = "0";
if(normalize_data){
	toNormalize = "1";
}
var next_code="<a class='tutorial_link' href='#' id='tutorial_advance'>Next</a>";
var prev_code="<a class='tutorial_link' href='#' id='tutorial_previous'>Previous</a>";

var tutorial1 = [
{ target: $(window), my: "center", at: "center", width: "300px", title: "<span class='basic_font'>Welcome</span>", content: "<span style='color:white;'>This tutorial shows you how to navigate the search result page. Click next to begin the tutorial. A sample query's result will be launched. <a class='tutorial_link' id='tutorial_advance' href='viewer33.jsp?sessionID=1358281807867&d_page=1&g_page=1&d_num_per_page=5&g_num_per_page=100&gradient_up=2.0&gradient_low=-2.0&norm=true&sort_sample_by_expr=false'>Next</a></span>"}, 
{ target: $("#main_coexp"), my: "top center", at: "bottom center", width: "400px", title: "<span class='basic_font' style='color:white;'>Step 1: Genes</span>", content: "<span style='color:white;'>In the result page of this query, Seek identifies an ordered list of genes that are found to be coexpressed with the query. The list is sorted by the coexpression score. Mouse over or click on a gene label to learn more. " + next_code + "</span>"}, 
{ target: $("#main_weight"), my: "bottom left", at: "center right", width: "600px", title: "<span class='basic_font' style='color:white'>Step 2 Datasets</span>", content: "<span style='color:white'>Along with the genes, Seek has also determined datasets that are related to the query. These datasets are arranged in decreasing order of their query-relevance weight. In general, higher weighted dataset contributes more to the overall coexpression score of each gene. Weights are calculated from the coexpression score among the query genes (see Hibbs et al). Feel free to mouse-over or click on a dataset label. " + next_code + " " + prev_code + "</span>"},
{ target: $("#microarray"), my: "bottom left", at: "top left", width: "600px", title: "<span class='basic_font' style='color:white'>Step 2 Datasets (cont.)</span>", content: "<span style='color:white'> Datasets are also shown visually in the image below. Here, datasets are separated by white vertical strips. Each row shows a progression from negatively to positively colored bands - designating the most underexpressed conditions (the left side) and the most overexpressed conditions (the right side) in the context of the query genes. Click on the image to learn about these conditions. " + next_code + " " + prev_code + "</span>"},
{ target: $("#table_div"), my: "bottom right", at: "center right", width: "400px", title: "<span class='basic_font' style='color:white'>Step 3 Navigation</span>", content: "<span style='color:white'>Don't forget to use the scroll bars (right and bottom) to see additional genes and datasets. By default, each page shows 100 genes, 10 datasets. " + next_code + " " + prev_code + "</span>"},
{ target: $(".page-nav").eq(2), my: "top center", at: "bottom center", width: "400px", title: "<span class='basic_font' style='color:white'>Step 3 Navigation (cont.)</span>", content: "<span style='color:white'>To see genes and datasets beyond the first page, click on Next page of datasets, or Next page of genes. " + next_code + " " + prev_code + "</span>"},
{ target: $("#genes_complete"), my: "bottom left", at: "top left", width: "400px", title: "<span class='basic_font' style='color:white'>Step 4 View everything</span>", content: "<span style='color:white'>Users can also view the entire list of genes, or datasets in one page, but without the visualization. <br>This is the last tutorial page. Please close the tutorial.</span>"}
];
	
function advance_tutorial(){
	var pageID = parseInt(sessionStorage.getItem("tutorial_page"));
	pageID+=1;
	sessionStorage.setItem("tutorial_page", pageID.toString());
	show_tutorial();
}

function set_tutorial(pageID){
	$("#tutorial_tooltip").qtip("option", {"content.text": tutorial1[pageID].content});
	$("#tutorial_tooltip").qtip("api").set("content.title.text", tutorial1[pageID].title)
	$("#tutorial_tooltip").qtip("api").set("position.target", tutorial1[pageID].target)
	$("#tutorial_tooltip").qtip("api").set("position.my", tutorial1[pageID].my)
	$("#tutorial_tooltip").qtip("api").set("position.at", tutorial1[pageID].at);
	$("#tutorial_tooltip").qtip("api").set("style.width", tutorial1[pageID].width);
}

function show_tutorial(){
	var enable = sessionStorage.getItem("tutorial_enable");
	if(enable=="1"){
		var pageID = parseInt(sessionStorage.getItem("tutorial_page"));
		set_tutorial(pageID);
		$("#tutorial_tooltip").qtip("api").hide();
		$("#tutorial_tooltip").trigger("click");
	}
}

$(document).ready(function(){
/*requires viewer_common.js be imported */
$("#tutorial_enable").click(function(){
	sessionStorage.setItem("tutorial_enable", "1");
	sessionStorage.setItem("tutorial_page", "0");
	show_tutorial();
	return false;
});

$("#tutorial_tooltip").qtip({
	id: "tutorial_tooltip",
	overwrite: false,
	content: {
		text: tutorial1[0].content,
		title: {
			text: tutorial1[0].title, 
			button: true
		}
	},
	position: {
		my: tutorial1[0].my,
		at: tutorial1[0].at,
		target: tutorial1[0].target,
		viewport: $(window)
	},
	show: {
		event: "click", 
	},
	hide: false,
	events: {
		show: function(event, api){
			sessionStorage.setItem("tutorial_enable", "1");
			$("#tutorial_advance").click(function(){
				var pageID = parseInt(sessionStorage.getItem("tutorial_page"));
				pageID+=1;
				sessionStorage.setItem("tutorial_page", pageID.toString());
				if($("#tutorial_advance").attr("href")=="#"){
					set_tutorial(pageID);
					$("#tutorial_tooltip").qtip("api").hide();
					$("#tutorial_tooltip").trigger("click");
					return false;
				}
			});
			$("#tutorial_previous").click(function(){
				var pageID = parseInt(sessionStorage.getItem("tutorial_page"));
				pageID-=1;
				sessionStorage.setItem("tutorial_page", pageID.toString());
				set_tutorial(pageID);
				$("#tutorial_tooltip").qtip("api").hide();
				$("#tutorial_tooltip").trigger("click");
				return false;
			});
		},
		hide: function(event, api) {
			sessionStorage.setItem("tutorial_enable", "0");
			//sessionStorage.setItem("tutorial_page", "0");
		},
	},
	style: {
		classes: "ui-tooltip-dark ui-tooltip-shadow ui-tooltip-genes",
		width: "600px",
	},
});

$("#stretch_window").click(function(){
	if(microarrayWindowWidth==950){
		microarrayWindowWidth=1200;
		panelWindowWidth=1400;
	}else if(microarrayWindowWidth==1200){
		microarrayWindowWidth=1600;
		panelWindowWidth=1800;
	}else if(microarrayWindowWidth==1600){
		microarrayWindowWidth=950;
		panelWindowWidth=1150;
	}
	adjust_window();
	return false;
});
	
$("#somediv").qtip({
	id: "somediv",
	overwrite: false,
	content: { 
		text: "Loading",
		title: {
			text: "Loading",
			button: true,
		},
	},
	position: {
		my: "center", 
		at: "center", 
		target: $(window),
	},
	show:{
		event: "click", 
		//solo: true,
		modal: {
			on: true,
			blur: false
		},
		effect: false,
	},
	hide: false,
	style: {
		//classes: "ui-tooltip-dark ui-tooltip-shadow",
		classes: "ui-tooltip-light ui-tooltip-shadow",
		//width: "800px",
		//height: "500px",
	},
	events: {
		show: function(event, api) {
			$("#status_message2").text("");
			$("#overlapdiv").qtip("hide");
			var enable = sessionStorage.getItem("tutorial_enable");
			if(enable=="1"){
				$("#tutorial_tooltip").qtip("api").hide();
				sessionStorage.setItem("tutorial_enable", "1");
			}
		},
		hide: function(event, api) {
			$("#overlapdiv").qtip("hide");
			var enable = sessionStorage.getItem("tutorial_enable");
			var pageID = sessionStorage.getItem("tutorial_page");
			if(enable=="1"){
				set_tutorial(pageID);
				$("#tutorial_tooltip").qtip("api").hide();
				$("#tutorial_tooltip").trigger("click");
			}
		},
		/*render: function(event, api){
			$(this).draggable({containment: 'window', 
				handle: api.elements.titlebar});
		},*/
	},
});	
		
$("#overlapdiv").qtip({
	id: "overlapdiv",
	overwrite: false,
	content: { 
		text: "Loading",
		title: {
			text: "Loading",
			button: true,
		},
	},
	position: {
		my: "top left", 
		at: "bottom right",
	},
	show:{
		event: "click",
		effect: false,
	},
	hide: false,
	style: {
		//classes: "ui-tooltip-dark ui-tooltip-genes",
		classes: "ui-tooltip-blue ui-tooltip-genes",
	},
});
	
$.ajaxSetup({ cache: false }); 
			
/* make column header and row header scroll as you scroll the heatmap image */
$("#table_div").scroll(function(){
  $("#divHeader").scrollLeft($("#table_div").scrollLeft());
  $("#query_table_div").scrollLeft($("#table_div").scrollLeft());
  $("#dendrogram_div").scrollLeft($("#table_div").scrollLeft());
  $("#firstcol").scrollTop($("#table_div").scrollTop());
});

function process_gene_score(responseText){
	var v = responseText.split("\n");
	var tt = $("<table>")
		.attr("cellpadding", "0")
		.attr("cellspacing", "0")
		.addClass("basic_font");
	tt.append($("<tr>").addClass("table_header_underline")
		.append($("<td>"))
		.append($("<td>")
			.text("Rank"))
		.append($("<td>")
			.text("Gene"))
		.append($("<td>")
			.text("Full name")));

	sessionStorage.setItem("export_gene_select_list", "");
	sessionStorage.setItem("export_genename_select_list", "");
		
	for(var i=0; i<v.length; i++){
		var vt = v[i].split("\t");
		//vt[0] - rank,
		//vt[1] - gene name
		//vt[2] - entrez gene id
		var dd_str = "../img/red_unchecked.png";
		if(StorageListFind("export_gene_select_list", vt[2])){
			dd_str = "../img/red_checked.png";
		}else{
			dd_str = "../img/red_unchecked.png";
		}
		tt.append($("<tr>")
			.append($("<td>")
				.append($("<img>")
					.addClass("checkbox_large")
					.attr("src", dd_str)
					.attr("id", "export_enable_g_" + vt[2])
					.attr("name_short", vt[2])
					.attr("name", vt[1])
					.click(function(){
						var state = $(this).attr("src");
						var gn = $(this).attr("name_short");
						var dname = $(this).attr("name");
						if(state=="../img/red_unchecked.png"){
							$(this).attr("src", "../img/red_checked.png");
							StorageListAdd("export_gene_select_list", gn);
							StorageListAdd("export_genename_select_list", dname);
						}else{
							$(this).attr("src", "../img/red_unchecked.png");
							StorageListDelete("export_gene_select_list", gn);
							StorageListDelete("export_genename_select_list", dname);
						}
						return false;
					})
				)
			)
			.append($("<td>").text(vt[0]))
			.append($("<td>").text(vt[1]))
			.append($("<td>").text(vt[4]))
		);
	}
	return tt;
}

var currentTabClass = "";
$("#expression_tab").mouseenter(function(){
	if($("#expression_tab").hasClass("seek-tab-unhighlighted")){
		currentTabClass = "seek-tab-unhighlighted";
		$("#expression_tab").removeClass("seek-tab-unhighlighted").addClass("seek-tab-highlighted");
	}else{
		currentTabClass = "seek-tab-highlighted";
	}

}).mouseleave(function(){
	if(currentTabClass == "seek-tab-unhighlighted"){	
		$("#expression_tab").removeClass("seek-tab-highlighted").addClass("seek-tab-unhighlighted");
	}

}).click(function(){	
	window.location = "viewer33.jsp?sessionID=" + sessionID + "&sort_sample_by_expr=true";
});

$("#coexpression_tab").mouseenter(function(){
	if($("#coexpression_tab").hasClass("seek-tab-unhighlighted")){
		currentTabClass = "seek-tab-unhighlighted";
		$("#coexpression_tab").removeClass("seek-tab-unhighlighted").addClass("seek-tab-highlighted");
	}else{
		currentTabClass = "seek-tab-highlighted";
	}

}).mouseleave(function(){
	if(currentTabClass == "seek-tab-unhighlighted"){	
		$("#coexpression_tab").removeClass("seek-tab-highlighted").addClass("seek-tab-unhighlighted");
	}

}).click(function(){	
	window.location = "viewer44.jsp?sessionID=" + sessionID
	+ "&sort_sample_by_expr=false";
});

$("#lock_image").click(function(){
	var tt = $("#lock_image").text();
	if(tt=="Disable image scroll"){
		$("#table_div").css("overflow", "hidden")
		$("#lock_image").text("Enable image scroll");
	}else{
		$("#table_div").css("overflow", "auto")
		$("#lock_image").text("Disable image scroll");
	}
	return false;
});

$("#main_cross_val_tip").attr("title", "Shows how well each query gene is able to retrieve the remaining query genes in each of 50 datasets");
$("#main_cross_val_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_coexp_gene_gradient_tip").attr("title", "Z-score of Pearson correlation");
$("#main_coexp_gene_gradient_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_coexp_query_gradient_tip").attr("title", "Individual query's cross validation score<br><br>" + 
"This validates retrieval of N-1 remaining query using 1 of query genes");
$("#main_coexp_query_gradient_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_weight_coexp_tip").attr("title", "Top query-relevant datasets (i.e. datasets where query genes are co-regulated). <br><br>" + 
"Display format for a dataset: <br><br>" +
"<span class='small_font'><b>1. " +
"Testicular, Germ Cell, Seminoma...<br></b></span><br>"+
"<b>1</b>: Rank of the dataset <br>" +
"<b>Testicular, Germ Cell...</b>: Keywords describing the dataset. (Based on mining dataset and condition titles)");
$("#main_weight_coexp_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_current_dset_tip").attr("title", "Listed are datasets being considered for the current query." + 
"<br><br>By default, if the query is multi-gene, modSEEK applies dataset weighting on <b>all datasets</b>. Otherwise, if the query is single-gene, modSEEK applies equal contributions to all datasets. It is possible to improve search quality by using Quick Refine (on the right) to a <b>category of datasets</b> of interest (particularly for single-gene query)" + 
"<br><br>This <b>Quick Refine</b> function narrows down the dataset scope." +
"<br><br>Automatic dataset weighting can often recognize the right biological context if user's query is multi-gene, so Quick refine may not be needed." +
"<br><br>For single-gene query, we also recommend guided search by using functional network.");
$("#main_current_dset_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_quick_refine_tip").attr("title", "Shortcut for the Refine Search function (see below the page and see the Refine Search tutorial). This will restrict the search scope. <br><br>Recommended for single-gene query or small query, or if users not satisfied with the datasets prioritized. Several choices: " + 
"<br><br><b>All datasets</b>: do not refine" + 
"<br><b>Multi-tissue datasets/modENCODE/commonly weighted</b>: each dataset contains multiple tissues, or is profiled by modENCODE, or is a commonly weighted dataset. Recommended for 1-gene query" + 
"<br><b>Guided search by FN</b>: dataset weighting is guided by functional network (FN). FN is a supervised gene association network learnt from gene ontology annotations, PPI, and expression data. Recommended for 1-gene query" + 
"<br><b>Other categories</b> are listed and where more advanced categories can be accessed");
$("#main_quick_refine_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_weight_exp_tip").attr("title", "Top query-relevant datasets (i.e. datasets where query genes are co-regulated). <br><br>" + 
"Display format for a dataset: <br><br>" +
"<span class='small_font'><b>1. (1.62)<br> " +
"Testicular, Germ Cell, Seminoma...<br></b></span><br>"+
"<b>1</b>: Rank of the dataset <br>" + "<b>1.62</b>: The weight of the dataset <br>"+
"<b>Testicular, Germ Cell...</b>: Keywords describing the dataset. (Based on mining dataset and condition titles)");
$("#main_weight_exp_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});
//$("#main_weight_exp_tip").qtip({style:{classes:"ui-tooltip-dark ui-tooltip-shadow", width:"300px"}});

$("#main_coexp").find("a").attr("title", "Integrated co-expression score to the query. <br><br>This value, distributed from -3 to +3, represents z-score of Pearson correlation weighted by each dataset's relevance. See paper for details."); 
$("#main_coexp").find("a").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_query_tip").attr("title", "Gene(s) that represent a biological context of interest, and to which co-expressions will be searched and analyzed.<br><br>" + 
"Genes for constructing a query could come from a common biological process or pathway, common function or cellular component, molecular complex, or differentially expressed gene list.");
$("#main_query_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_gradient_tip").attr("title", "Gene centered expression.<br>Left end of the gradient: down-regulation. <br>Right end of the gradient: up-regulation.");
$("#main_gradient_tip").qtip({style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_expression_tip").attr("title", "View the expression profile of the query and co-expressed genes in top weighted datasets. View up- and down-regulated conditions.");
$("#main_expression_tip").qtip({position:{my:"top right", at: "bottom left",},style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#main_coexpression_tip").attr("title", "View the summarized co-expression scores across 50 datasets at a time.");
$("#main_coexpression_tip").qtip({position:{my:"top right", at: "bottom left",},style:{classes:"ui-tooltip-blue ui-tooltip-shadow", width:"300px"}});

$("#expression_view").attr("title", "Global view of the expression of the query genes and their " + 
	"related genes across datasets. Datasets (horizontal dimension) are sorted by relevance " + 
	"to the query context. " + 
	"Similarly, genes (vertical dimension) are sorted by the distance to the query genes. " + 
	"The first few rows in yellow are the query genes' expression.<p>" +
	"By default, 100 genes and 10 datasets are shown per page. " +
	"Click on the expression image to see more detail.");

//$("#expression_view").qtip({style:{classes:"ui-tooltip-dark", width:"300px"}});
$("#expression_view").qtip({style:{classes:"ui-tooltip-blue", width:"300px"}});

$("#span_enrich_gene").attr("title", "Gene set enrichment analysis for the top retrieved genes. Users can detect any statistically signficant biological themes that may be shared among the top retrieved genes.");
//$("#span_enrich_gene").qtip({position: {my: "bottom left", at: "top right", }, style:{classes:"ui-tooltip-dark", width:"300px"}});
$("#span_enrich_gene").qtip({position: {my: "bottom left", at: "top right", }, style:{classes:"ui-tooltip-blue", width:"300px"}});

$("#span_select_dataset").attr("title", 
"After the search is completed, users may wish to further refine the search results, for example, to perform a co-expression analysis only on datasets " + 
"related to a particular tissue, cell type or disease. " + 
"Using this option, they can specify the dataset categories to be used for the search analysis.");
//$("#span_select_dataset").qtip({position: {my: "bottom left", at: "top right", }, style:{classes:"ui-tooltip-dark", width:"300px"}});
$("#span_select_dataset").qtip({position: {my: "bottom left", at: "top right", }, style:{classes:"ui-tooltip-blue", width:"300px"}});


$("#enrichment").click(function(){
	show_gene_enrichment();
	return false;
});


if(sessionStorage.getItem("gene_refine")=="true"){
	show_gene_refine();
	$("#selection_done").show();
}

if(sessionStorage.getItem("dataset_refine")=="true"){
	show_dataset_refine();
	$("#selection_done").show();
}

$("#export_genes").click(function(){
	window.open("../servlet/GetScoreServlet?organism=" + organism + "&sessionID=" + sessionID + "&type=gene_score&keyword=all_sorted");
	return false;
});

$("#export_datasets").click(function(){
	window.open("../servlet/GetScoreServlet?organism=" + organism + "&sessionID=" + sessionID + "&type=dataset_weight&keyword=all_sorted");
	return false;
});

$("#export_download").click(function(){
	window.open("../servlet/GetExportFile?organism=" + organism + "&file=" + sessionID + "_export");
	return false;
});

$("#export_exp").click(function(){
	sessionSetDefault("pval_filter", "-1");
	pvalCutoff = sessionStorage.getItem("pval_filter");
	if(pvalCutoff=="-1") pval_filter = false;
	else pval_filter = true;

	sessionSetDefault("rbp_p", "0.99");
	sessionSetDefault("multiply_d_weight", "true");

	if(pageMode=="coexpression"){
		$.post("../servlet/ExportExpression", {organism:organism, sessionID:sessionID, mode:"1", annotation:"true",
		normalize:"false",
		d_range:"false", g_range:"false", gene:geneNames.join("+"), dataset:dsetNames.join("+"), 
		query:queryNames.join("+"), rbp_p:sessionStorage.getItem("rbp_p"), 
		filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
		mult_by_dset_weight:sessionStorage.getItem("multiply_d_weight"), 
		specify_genes:sessionStorage.getItem("specify_genes")},
		function(responseText){
			$("#export_download").trigger("click");
		}); 
	}else{ //expression mode
		var so = [];
		for(var i=0; i<sample_order.length; i++){
			var sx = [];
			for(var j=0; j<sample_order[i].length; j++)
				sx.push(sample_order[i][j].toString());
			so.push(sx.join("|"));
		}
		$.post("../servlet/ExportExpression", {organism:organism, sessionID:sessionID, mode:"2", annotation:"true",
		normalize:"true",
		d_range:"false", g_range:"false", gene:geneNames.join("+"), dataset:dsetNames.join("+"), 
		query:queryNames.join("+"), sample_order:so.join("+"),
		filter_by_pval:pval_filter, pval_cutoff:pvalCutoff, 
		specify_genes:sessionStorage.getItem("specify_genes")},
		function(responseText){
			$("#export_download").trigger("click");
		}); 
	}

});

//not needed for now
/*$("#export_matrix").click(function(){
	var th = $("<font>").css("font-family", "Arial")
		.css("font-size", "12px")
		.append("<b>Options:</b>");

	var topt = $("<table>")
		.addClass("basic_font")
		.append($("<tr>")
			.append($("<td>")
				.append("Select genes to export (maximum 100): ")
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<a>")
					.attr("href", "#")
					.addClass("basic_font")
					.attr("id", "export_clear_gene_selection")
					.text("None")
					.click(function(){
						select_none_rows($("#export_gene_window table").find("tr").slice(1), 
							"export_gene_select_list", "export_genename_select_list");
						return false;
					})
				)
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<a>")
					.attr("href", "#")
					.addClass("basic_font")
					.attr("id", "export_all_gene_selection")
					.text("All")
					.click(function(){
						select_all_rows($("#export_gene_window table").find("tr").slice(1), 
							"export_gene_select_list", "export_genename_select_list");
						return false;
					})
				)
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<a>")
					.attr("href", "#")
					.addClass("basic_font")
					.attr("id", "export_top_100_gene")
					.text("Top 100")
					.click(function(){
						alert("Clicked on top 100");
						return false;
					})
				)
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<input>")
					.attr("type", "text")
					.attr("name", "gene_search")
					.attr("id", "gene_search")
					.css("font", "12px Arial")
					.attr("size", "15")
					.keypress(function(e){
						if(e.which==13) $("#gene_search_button").click();
					})
				)
				.append($("<input>")
					.attr("type", "button")
					.attr("name", "gene_search_button")
					.attr("id", "gene_search_button")
					.attr("value", ">")
					.css("font", "12px Arial")
					.click(function(){
						var searchTerm = $("#gene_search").val();
						var table = $("#export_gene_window table");
						show_hide_rows(table.find("tr").slice(1), searchTerm);
						return false;
					})
				)
				.append($("<input>")
					.attr("type", "button")
					.attr("name", "gene_search_clear")
					.attr("id", "gene_search_clear")
					.attr("value", "X")
					.css("font", "12px Arial")
					.click(function(){
						$("#gene_search").attr("value", "");
						$("#gene_search_button").click();
						return false;
					})
				)
			)
		)
		.append($("<tr>")
			.append($("<td>")
				.attr("colspan", "9")
				.append($("<div>")
					.attr("id", "export_gene_window")
					.css("width", "700px")
					.css("height", "300px")
					.css("overflow", "auto")
					.append($("<font>")
						.addClass("basic_font").text("LOADING TABLE... (1 - 30 seconds).")
					)
				)
			)
		)
		.append($("<tr>").css("height", "10px"))
		.append($("<tr>")
			.append($("<td>")
				.append("Select datasets to export (maximum 10): ")
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<a>")
					.attr("href", "#")
					.addClass("basic_font")
					.attr("id", "export_clear_dset_selection")
					.text("None")
					.click(function(){
						select_none_rows($("#export_dset_window table").find("tr").slice(1), 
							"export_dset_select_list", "export_dsetname_select_list");
						return false;
					})
				)
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<a>")
					.attr("href", "#")
					.addClass("basic_font")
					.attr("id", "export_all_dset_selection")
					.text("All")
					.click(function(){
						select_all_rows($("#export_dset_window table").find("tr").slice(1), 
							"export_dset_select_list", "export_dsetname_select_list");
						return false;
					})
				)
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<a>")
					.attr("href", "#")
					.addClass("basic_font")
					.attr("id", "export_top_10_dset")
					.text("Top 10")
					.click(function(){
						alert("Clicked on top 100");
						return false;
					})
				)
			)
			.append($("<td>").css("width", "10px"))
			.append($("<td>")
				.append($("<input>")
					.attr("type", "text")
					.attr("name", "dset_search")
					.attr("id", "dset_search")
					.css("font", "12px Arial")
					.attr("size", "15")
					.keypress(function(e){
						if(e.which==13) $("#dset_search_button").click();
					})
				)
				.append($("<input>")
					.attr("type", "button")
					.attr("name", "dset_search_button")
					.attr("id", "dset_search_button")
					.attr("value", ">")
					.css("font", "12px Arial")
					.click(function(){
						var searchTerm = $("#dset_search").val();
						var table = $("#export_dset_window table");
						show_hide_rows(table.find("tr").slice(1), searchTerm);
						return false;
					})
				)
				.append($("<input>")
					.attr("type", "button")
					.attr("name", "dset_search_clear")
					.attr("id", "dset_search_clear")
					.attr("value", "X")
					.css("font", "12px Arial")
					.click(function(){
						$("#dset_search").attr("value", "");
						$("#dset_search_button").click();
						return false;
					})
				)
			)
		)
		.append($("<tr>")
			.append($("<td>")
				.attr("colspan", "9")
				.append($("<div>")
					.attr("id", "export_dset_window")
					.css("width", "700px")
					.css("height", "200px")
					.css("overflow", "auto")
					.append($("<font>")
						.addClass("basic_font").text("LOADING TABLE... (1 - 30 seconds).")
					)
				)
			)
		);

	var ta = $("<div>")
		.attr("id", "export_matrix_window")
		.css("width", "750px")
		.css("height", "320px")
		.css("overflow", "auto")
		.append($("<font>").addClass("basic_font").text("LOADING TABLE... (1 - 30 seconds)."));

	ta = th.after(topt).after("<p>");
	var tit = "<font style=\"font-family:Arial; font-size:12px;\">Export Matrix</font>";	
	
		$("#somediv").qtip("option", {"content.text": ta});
		$("#somediv").qtip("api").set("content.title.text", tit);
		$("#somediv").trigger("click");
	
	setTimeout(function(){	
	$.get("../servlet/GetScoreServlet", 
		{sessionID:sessionID, type:"gene_score", keyword:"1000_sorted"}, 
		function(responseText){
		var t = process_gene_score(responseText);
		$("#export_gene_window").html(t);
	});
	$.get("../servlet/GetScoreServlet", 
		{sessionID:sessionID, type:"dataset_weight", keyword:"100_sorted"}, 
		function(responseText){
		var t = process_dataset_score(responseText);
		$("#export_dset_window").html(t);
	});
	}, 500);
	return false;	
});*/


});

