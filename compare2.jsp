<!DOCTYPE HTML>
<html>
<head>
<title>SEEK: Search based exploration of expression kompendia</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="/modSeek/css/viewer.css" rel="Stylesheet">
<link type="text/css" href="css/jquery.qtip.css" rel="Stylesheet" />
<link type="text/css" href="css/smoothness/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.22.custom.min.js"></script>
<script src="/js/jquery.ui.touch-punch.min.js"></script>
<script src="/js/jquery.qtip.js"></script>
<script src="/js/jquery.ba-throttle-debounce.min.js"></script>
<script type="text/javascript">

var numQuery = 2;

var sessionID_array = [];
var top_dataset_array = [];
var top_gene_array = [];
var organism_array = [];
var page_org_array = [];
var page_gene_org_array = [];
var queryNames_array = [];
var bg_color_array = ["pink", "lightblue", "gold", "rosybrown"];

var sessionID = "";
var organism = "";
var opt = "";

var current_organism = "";
var current_sessionID = "";
var current_queryNames = [];
var current_id = 0;
var completed = 0;
var arrayQuery = [];

//misc function
function get_servlet_path(org){
	if(org=="human")
		return "/servlet";
	return "/modSeek/servlet";
}

function get_web_path(org){
	if(org=="human")
		return "";
	return "/modSeek/" + org;
}

function get_default_top_dataset(org){
	var top = 0;
	if(org=="yeast" || org=="fly" || org=="worm" || org=="zebrafish")
		top = 50;
	else
		top = 100;
	return top;
}

function get_default_top_gene(org){
	return 100;
}

//for search status update
function assign_parameters(){
	if(current_id==numQuery-1){ //all organisms have finished by now
		var vi = current_id;
		queryNames_array[vi] = current_queryNames;
		sessionID_array[vi] = current_sessionID;
		completed+=1;
		for(vi=1; vi<=numQuery; vi++){
			$("#query_" + vi + "_session").attr("value", sessionID_array[vi-1]);
		}

		$("#statusdiv").qtip("hide");
		$("#dataset_link").trigger("click");

		for(vi=1; vi<=numQuery; vi++){
			$("#query_" + vi).show();
			get_query(vi, organism_array[vi-1], sessionID_array[vi-1]);
			$("#s_name_" + vi).text("(sessionID: " + sessionID_array[vi-1] + ")");
			$("#span" + vi).show();
		}
		get_ready();
	}
	else{   //do next organism
		queryNames_array[current_id] = current_queryNames;
		sessionID_array[current_id] = current_sessionID;
		current_id+=1;
		completed+=1;
		var vi = current_id;
		var queryUpper = $("#search_query_" + (vi+1)).attr("value").toUpperCase();
		queryUpper = queryUpper.replace(/;/g, " ").replace(/,/g, " ")
			.replace(/"/g, " ").replace(/\s{2,}/g, " ");
		search_one_query(organism_array[vi], queryUpper);
	}
}

//for search status update
function poll_progress(){
	$.get(get_servlet_path(current_organism) + "/GetSearchMessage", {organism:current_organism, param:"status", sessionID:current_sessionID}, function(responseText){
		var a = responseText.split("\n");
		a.pop();
		$("#search_result").html(a.join("<br>"));
		$("#status_pane").scrollTop($("#status_pane")[0].scrollHeight);
		if(a[a.length-1].indexOf("Error: no dataset contains")==0){
			alert("Error: no dataset contains any of the query genes. Please go back and fix your query.");
			document.location.reload();
		}else if(a[a.length-1].indexOf("Error: not enough query genes present")==0){
			alert("Error: not enough query genes present in >70% of the datasets. Suggest restricting datasets to those containing all query genes, or change query.");
			document.location.reload();
		}else if(a[a.length-1]=="Done writing results."){
			//$("#status_percentage").text("100%");
			$("#status_text").text("Finished");
			$("#status_pane").scrollTop($("#status_pane")[0].scrollHeight);
			resetGenes();
		}else{
			setTimeout(poll_progress, 1000);
		}
	});
}
//for search status upate
function resetGenes(){
	$.get(get_servlet_path(current_organism) + "/GetSearchMessage", {organism:current_organism, param:"query_attr", sessionID:current_sessionID}, function(responseText){
		var res = responseText.split("\n");
		if(res[res.length-1]==""){
			res.pop();	
		}
		current_queryNames = [];
		for(var vi=0; vi<res.length; vi++){
			current_queryNames.push(res[vi]);
		}
		//$("#statusdiv").qtip("hide");
		assign_parameters();
	});
}

//for search status update
function search_one_query(org, queryUpper){
	var d = new Date();
	var n = d.getTime();
	current_sessionID = n.toString();
	current_organism = org;

	$("#status_text").text("(" + (current_id+1) + "/" + numQuery + ") Searching in " + current_organism + " compendium");

	$.get(get_servlet_path(current_organism) + "/GetSearchMessage", {param:"status", sessionID:current_sessionID}, function(resT){	
		$.post(get_servlet_path(current_organism) + "/DoSearch2", {organism:current_organism, sessionID:current_sessionID, dset:"all",
			query:queryUpper, query_mode:"gene_symbol", 
			search_alg:sessionStorage.getItem("search_alg"), 
			search_distance:sessionStorage.getItem("search_distance"),
			rbp_p:sessionStorage.getItem("rbp_p"), 
			correlation_sign:"positive",
			percent_query:sessionStorage.getItem("percent_query"),
			percent_genome:sessionStorage.getItem("percent_genome")}, 
			function(responseText){}
		);
	});
	setTimeout(poll_progress, 1000);
}

function get_query(num, org, sess){
	$.post("/modSeek/servlet/GetQuery", {organism:org, sessionID:sess}, function(responseText){
		$("#q_name_" + num).text(responseText);
	});
	$.post("/modSeek/servlet/GetQuery", {organism:org, sessionID:sess, entrez:1}, function(responseText){
		arrayQuery[num-1] = responseText.split(" ").join("+");
	});
}

function adjust_table_width(){
	var widthPerQuery = 475;
	var tableWidth = 475 * numQuery;
	var canvasWidth = tableWidth + 200;
	$("#head1").css("width", tableWidth + "px");
	$("#table1").css("width", tableWidth + "px");
	$("#wrapper").css("width", canvasWidth + "px");
	var percentage = 100 / numQuery;
	$("#query_row").find("td").each(function(){
		$(this).css("width", percentage + "%");
	});
	$("#table1_colspan1").attr("colspan", numQuery);
	$("#table1_colspan2").attr("colspan", numQuery);
	$("#table1_colspan3").attr("colspan", numQuery);
	$("#table1_colspan4").attr("colspan", numQuery);
	$("#table1_colspan5").attr("colspan", numQuery);
}

function resize_array(array1, numQuery){
	var array2 = new Array(numQuery);
	for(var vi=1; vi<=numQuery; vi++){
		array2[vi-1] = array1[vi-1];
	}
	return array2;
}
function get_ready(){
	top_dataset_array = new Array(numQuery);
	top_gene_array = new Array(numQuery);
	sessionID_array = resize_array(sessionID_array, numQuery);
	organism_array = resize_array(organism_array, numQuery);
	page_org_array = resize_array(page_org_array, numQuery);
	page_gene_org_array = resize_array(page_gene_org_array, numQuery);
	queryNames_array = resize_array(queryNames_array, numQuery);	
	arrayQuery = new Array(numQuery);

	$("#compare_button").unbind("click").click(function(){
		for(var vi=1; vi<=numQuery; vi++){
			var o1 = $("#org" + vi).val();
			if(o1=="-"){
				alert("Select an organism for query " + vi);
				return false;
			}
			var n1 = $("#query_" + vi + "_session").attr("value");
			if(n1==""){
				alert("Enter sessionID for query " + vi);
				return false;
			}
		}
		for(var vi=1; vi<=numQuery; vi++){
			sessionID_array[vi-1] = $("#query_" + vi + "_session").attr("value");
			organism_array[vi-1] = $("#org" + vi).val();
		}
		$("#dataset_link").trigger("click");
		for(var vi=1; vi<=numQuery; vi++){
			$("#query_" + vi).show();
			get_query(vi, organism_array[vi-1], sessionID_array[vi-1]);
			$("#s_name_" + vi).text("(sessionID: " + sessionID_array[vi-1] + ")");
			$("#span" + vi).show();
		}
		return false;
	});


	$("#search_button").unbind("click").click(function(){
		for(var vi=1; vi<=numQuery; vi++){
			var o1 = $("#org" + vi).val();
			if(o1=="-"){
				alert("Select an organism for query " + vi);
				return false;
			}
			var n1 = $("#search_query_" + vi).attr("value");
			if(n1==""){
				alert("Enter the query genes for query " + vi);
				return false;
			}
		}
		sessionStorage.setItem("dset_search_mode", "all");
		for(var vi=1; vi<=numQuery; vi++){
			organism_array[vi-1] = $("#org" + vi).val();
		}
		$("#dataset_result").html("");
		$("#search_result").html("");
		$("#status_text").text("(1/" + numQuery + ") Searching in " + 
			organism_array[0] + " compendium");
		$("#statusdiv").trigger("click");
		var queryUpper = $("#search_query_1").attr("value").toUpperCase();
		queryUpper = queryUpper.replace(/;/g, " ").replace(/,/g, " ")
			.replace(/"/g, " ").replace(/\s{2,}/g, " ");
		current_id = 0;
		completed = 0;
		search_one_query(organism_array[0], queryUpper);
		return false;
	});

	$("#mode1").unbind("click").click(function(){
		if($("#mode1").is(":checked")){
			$("#mode2").prop("checked", false);
			$(".retrieve").show();
			$(".new_query").hide();
		}else{
			$("#mode2").prop("checked", true);
			$(".retrieve").hide();
			$(".new_query").show();
		}
	});
	$("#mode2").unbind("click").click(function(){
		if($("#mode2").is(":checked")){
			$("#mode1").prop("checked", false);
			$(".retrieve").hide();
			$(".new_query").show();
		}else{
			$("#mode1").prop("checked", true);
			$(".retrieve").show();
			$(".new_query").hide();
		}
	});

	$("#mode1").prop("checked", false);
	$("#mode2").prop("checked", true);
	$(".retrieve").hide();
	$(".new_query").show();
}

$(document).ready(function(){
	$.ajaxSetup({ cache: false }); 

	sessionStorage.clear();
	sessionStorage.setItem("search_alg", "RBP");
	sessionStorage.setItem("search_distance", "ZscoreHubbinessCorrected");
	sessionStorage.setItem("rbp_p", "0.99");
	sessionStorage.setItem("percent_query", "0.5");
	sessionStorage.setItem("percent_genome", "0.5");
	sessionStorage.setItem("pval_filter", "-1");

	$("#overlapdiv").qtip({
		id: "overlapdiv",
		overwrite: false,
		content: { 
			text: "Loading", title: {text: "Loading", button: true,},
			title: {
				text: "Loading",
				button: true,
			},
		},
		position: {
			my: "top right", 
			at: "bottom left",
			adjust: {y: -5,},
		},
		show:{
			event: "click",
			effect: false,
		},
		hide: false,
		style: {
			classes: "ui-tooltip-blue ui-tooltip-genes ui-tooltip-shadow",
		},
	});

	$("#somediv").qtip({
		id: "somediv",
		overwrite: false,
		content: { 
			text: "Loading",
			title: {
				text: "Loading",
				button: true,
			},
		},
		position: {
			my: "center", 
			at: "center", 
			target: $(window),
		},
		show:{
			event: "click", 
			//solo: true,
			modal: {
				on: true,
				blur: false
			},
			effect: false,
		},
		hide: false,
		style: {
			classes: "ui-tooltip-light ui-tooltip-shadow",
			tip: false,
		},
	});	

	$("#visdiv").qtip({
		id: "visdiv",
		overwrite: false,
		content: { 
			text: "Loading",
			title: {
				text: "Loading",
				button: true,
			},
		},
		position: {
			my: "center", 
			at: "center", 
			target: $(window),
		},
		show:{
			event: "click", 
			//solo: true,
			modal: {
				on: true,
				blur: false
			},
			effect: false,
		},
		hide: false,
		style: {
			classes: "ui-tooltip-light ui-tooltip-shadow",
			tip: false,
		},
	});	

	$("#statusdiv").qtip({
		id: "statusdiv",
		overwrite: false,
		content: { 
			text: "Loading",
			title: {
				text: "Loading...",
				button: false,
			},
		},
		position: {
			my: "center", 
			at: "center", 
			target: $(window),
		},
		show:{
			event: "click", 
			solo: true,
			modal: {
				on: true,
				blur: false,
			},
			effect: false,
		},
		hide: false,
		style: {
			classes: "ui-tooltip-light",
		},
	});

	$("#statusdiv").qtip("option", {"content.text": $("#statusdiv")});
	$("#statusdiv").qtip("option", {"content.title.text": $("#status_message")});

	//add_comparison();
	//adjust_table_width();
	get_ready();

	/*
	$("#add_button").click(function(){
		if(numQuery==4){
			return false;
		}
		add_comparison();
		return false;
	});

	$("#delete_button").click(function(){
		if(numQuery==1){
			return false;
		}
		delete_comparison();
		return false;
	});
	*/
});

</script>
</head>

<body style="padding:0px;margin:0px;font-family:Arial;">
<div id="wrapper" style="width:1024px;">
<table id="wrapper_table">
<tr>
<td style="padding:0px;">
<div id="query" style="background-color:rgb(94,39,80);height:80px;">
	<div style="position:relative; top:20px; left:10px">
		<img width="119" height="58" style="position:absolute" src="/modSeek/img/seek_mouse.png">
	</div>
	<table>
		<tr>
			<td style="width:200px"></td>
			<td style="width:400px; color:white;"></td>
			<td class="basic_font" style="width:200px;color:white;text-align:right;"><a class="white" href="/modSeek/">Home</a></td>
		</tr>
		<tr>
			<td style="width:150px; height:60px;"></td>
			<td style="vertical-align:middle;font-size:18px;width:450px; color:white;">
			Compare multiple queries
			</td>
			<td style="height:25px;width:100px;"></td>
			<td style="width:5px;"></td>
			<td style="height:25px; width:110px;"></td>
			<td style="width:5px;"></td>
		</tr>
	</table>
</div>

<!--
<div id="screen_info" style="height:30px; background-color:rgb(94,39,80);">
	<table>
	</table>
</div>
-->

<table id="table1" name="table1" style="width:950px;">
<tr>
<td id="table1_colspan1" colspan="2">

<div name="head1" id="head1" style="width:950px">
<table style="width:100%">
<tr>
<td style="width:70%;">
<!--
<input id="add_button" type="submit" value="Add Query">
<input id="delete_button" type="submit" value="Delete Query">
-->
</td>
<td style="padding:5px;background-color:cornflowerblue;color:white;">
<a class="white_underline" id="gene_link" href="#">Genes</a>
</td>
<td style="width:1%"></td>
<td style="padding:5px;background-color:cornflowerblue;color:white;">
<a class="white_underline" id="dataset_link" href="#">Datasets</a>
</td>
<td style="width:1%"></td>
<td style="padding:5px;background-color:cornflowerblue;color:white;">
<a class="white_underline" id="gene_enrichment_link" href="#">Gene<br>
Enrichments</a>
</td>
<td style="width:1%"></td>
<td style="padding:5px;background-color:cornflowerblue;color:white;">
<a class="white_underline" id="dataset_enrichment_link" href="#">Dataset<br>
Enrichments</a>
</td>
</tr>
</table>
</div>

</td>
</tr>

<tr id="usage">
<td colspan=2 style="padding:10px;">
How to use:
<p>

This page compares the results of two or more queries.
You can either enter query ID from a previous modSEEK session or directly search and compare using this interface. 
<p>

First enter two queries. Then click "Search" button.
<p>

Then use the four blue tabs locate at the top right corner to see different side-by-side comparisons.

</td>
</tr>
<tr id="query_row">

<td style="width:50%; padding:10px;">
<b>Query 1: </b>
<span id="q_name_1" class="q_name"></span>
<span id="s_name_1" class="s_name"></span>
<br>
Organism
<select name="org1" id="org1" qid=1>
<option value="-">--</option>
<option value="yeast">yeast (SCE)</option>
<option value="fly">fly (DME)</option>
<option value="mouse">mouse (MMU)</option>
<option value="human">human (HSA)</option>
<option value="worm">worm (CEL)</option>
<option value="zebrafish">zebrafish (DRE)</option>
</select>
</td>

<td qid=2 style="width:50%; padding:10px;">
<b>Query 2: </b>
<span id="q_name_2" class="q_name"></span>
<span id="s_name_2" class="s_name"></span>
<br>
Organism
<select name="org2" id="org2" qid=2>
<option value="-">--</option>
<option value="yeast">yeast (SCE)</option>
<option value="fly">fly (DME)</option>
<option value="mouse">mouse (MMU)</option>
<option value="human">human (HSA)</option>
<option value="worm">worm (CEL)</option>
<option value="zebrafish">zebrafish (DRE)</option>
</select>
</td>


</tr>

<tr>
<td id="table1_colspan2" colspan="2" style="padding-left:10px;">
<input type="checkbox" name="mode1" id="mode1">
Option 1 - Retrieve an existing result
</td>
</tr>

<tr id="session_row">
<td style="padding-left:10px; vertical-align:top;">
<div class="retrieve">
SessionID
<input size="12" id="query_1_session" name="query_1_session">
<br>
<span style="font-size:10px;">
A session ID is a identifier located in the URL of your query result: e.g:
http://seek.princeton.edu/modSeek/worm/viewer33.jsp?sessionID=1428540933775&amp;sort_sample_by_expr=true. SessionID is 1428540933775. 
</span>
</div>
</td>

<td style="padding-left:10px; vertical-align:top;">
<div class="retrieve">
SessionID
<input size="12" id="query_2_session" name="query_2_session">
</div>
</td>
</tr>

<tr>
<td id="table1_colspan3" colspan="2" style="padding-left:10px;">
<div class="retrieve">
<input id="compare_button" type="submit" value="Retrieve"> 
</div>
</td>
</tr>

<tr>
<td id="table1_colspan4" colspan="2" style="padding-left:10px;">
<input type="checkbox" name="mode2" id="mode2">
Option 2 - Search new queries
</td>
</tr>

<tr id="new_query_row">
<td style="padding-left:10px">
<div class="new_query">
New query
<input size="12" id="search_query_1" name="search_query_1">
<br>
(eg: ptc-1 ptc-2 in worm)
</div>
</td>
<td style="padding-left:10px;">
<div class="new_query">
New query
<input size="12" id="search_query_2" name="search_query_2">
<br>
(eg: ptch1 ptch2 in mouse)
</div>
</td>
</tr>

<tr>
<td id="table1_colspan5" colspan="2" style="padding-left:10px;">
<div class="new_query">
<input id="search_button" type="submit" value="Search"> 
</div>
</td>
</tr>

<!-- Stores the results============================ -->
<!--
<tr id="result_row">
<td style="vertical-align:top;">
<div id="gene_result_1" name="gene_result_1" style="display:none;width:100%;">
<table class="main" style="font-size:12px;width:100%;">
<tr class="head"><td colspan="4">All Genes</td></tr>
<tr class="head navigation"><td colspan="4"></td></tr>
<tr class="head"><td>Rank</td><td>Score</td><td>Gene</td><td>Snippet</td></tr></table>
</div>
<div id="dataset_result_1" name="dataset_result_1" style="display:none;width:100%;">
<table class="main" style="font-size:12px;width:100%;">
<tr class="head"><td colspan="4">All Datasets</td></tr>
<tr class="head navigation"><td colspan="4"></td></tr>
<tr class="head"><tr><td>Rank</td><td>PValue</td><td>Dataset</td><td>Title</td></tr></table>
</div>
<div id="gene_enrichment_1" name="gene_enrichment_1" style="display:none;width:100%;">
<table class="main" style="font-size:12px;width:100%;"><tr class="head"><td colspan="4">All Gene Enrichments
<br>in
<select name="top_genes_1" id="top_genes_1" qid=1>
<option value="100">Top 100 genes</option>
<option value="200">Top 200 genes</option>
<option value="500">Top 500 genes</option>
</select>
</td></tr><tr class="head"><td>Term</td><td>PValue</td><td>QValue</td><td>Overlap</td></tr></table>
</div>
<div id="dataset_enrichment_1" name="dataset_enrichment_1" style="display:none;width:100%;">
<table class="main" style="font-size:12px;width:100%;"><tr class="head"><td colspan="4">Prioritized Dataset Enrichments
<br>in
<select name="top_datasets_1" id="top_datasets_1" qid=1>
<option value="20">Top 20 datasets</option>
<option value="50">Top 50 datasets</option>
<option value="100">Top 100 datasets</option>
<option value="200">Top 200 datasets</option>
</select>
</td></tr><tr class="head"><td>Term</td><td>PValue</td><td>QValue</td><td>Overlap</td></tr></table>
</div>
</td>
</tr>
-->


</table>
</td>
</tr>
</table>

</div>

<div id="somediv" style="display:none;"></div>
<div id="visdiv" style="display:none;"></div>
<div id="overlapdiv" style="display:none;"></div>
<div id="statusdiv" style="display:none;">
    <div id="status_pane" style="height:500px; width:300px; overflow:auto;">
      <span id="dataset_result"></span><br>
      <span id="search_result"></span><br>
    </div>
  <div id="status_message" style="display:none;">
    <font class="basic_font"><b> 
    <span id="status_percentage"></span>
    <span id="status_text"></span>
    <img src="/ajax-loader.gif" style="vertical-align:text-bottom;">
    </b></font>
  </div>
    <a id="search_box" href="#"></a>
</div>

<script src="/modSeek/js/ready_query_expression.js"></script>
<!--<script src="/modSeek/js/viewer_comparative_process_result.js"></script>
<script src="/modSeek/js/viewer_comparative.js"></script>
-->
</body>
</html>
