<!DOCTYPE HTML>
<html>
<head>
<title>SEEK: Search based exploration of expression kompendia</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" href="/css/viewer.css" rel="Stylesheet">
<link type="text/css" href="/css/jquery.qtip.css" rel="Stylesheet" />
<link type="text/css" href="/css/smoothness/jquery-ui-1.8.22.custom.css" rel="Stylesheet" />
<script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.22.custom.min.js"></script>
<script src="/js/jquery.ui.touch-punch.min.js"></script>
<script src="/js/jquery.qtip.js"></script>
<script src="/js/jquery.ba-throttle-debounce.min.js"></script>

<script type="text/javascript">
var sessionID1 = "";
var sessionID2 = "";
var organism = "";
var organism1 = "";
var organism2 = "";
var search_option = "";
var analyze_option = "";
var ex_option = "";
var sessionID = "";
var opt = "";

$(document).ready(function(){
	$.ajaxSetup({ cache: false }); 

	$("#overlapdiv").qtip({
		id: "overlapdiv",
		overwrite: false,
		content: { 
			text: "Loading",
			title: {
				text: "Loading",
				button: true,
			},
		},
		position: {
			my: "top right", 
			at: "bottom left",
			adjust: {y: -5,},
		},
		show:{
			event: "click",
			effect: false,
		},
		hide: false,
		style: {
			classes: "ui-tooltip-blue ui-tooltip-genes ui-tooltip-shadow",
		},
	});

	$("#somediv").qtip({
		id: "somediv",
		overwrite: false,
		content: { 
			text: "Loading",
			title: {
				text: "Loading",
				button: true,
			},
		},
		position: {
			my: "center", 
			at: "center", 
			target: $(window),
		},
		show:{
			event: "click", 
			//solo: true,
			modal: {
				on: true,
				blur: false
			},
			effect: false,
		},
		hide: false,
		style: {
			classes: "ui-tooltip-light ui-tooltip-shadow",
			tip: false,
		},
	});	
			
	analyze_option = "shared";
	search_option = "up_up";
	ex_option = "up";

	$("#analyze_mode").find('option[value="shared"]').attr("selected", "selected");

	$("#shared1").show();
	$("#search_mode").find('option[value="up_up"]').attr("selected", "selected");

	$("#exclusive1").hide();
	$("#ex_mode").find('option[value="up"]').attr("selected", "selected");

	function extract_organism(q1){
		var org = "";
		if(q1.indexOf("organism")==-1){
			if(q1.indexOf("/modSeek/", 10)!=-1){
				var n1 = q1.indexOf("/modSeek/", 10)+9;
				var n2 = q1.indexOf("/", n1);
				org = q1.substring(n1, n2);
			}else{ //must be human
				org = "human";
				//alert("Error, not found q1");
			}
		}else{
			var n1 = q1.indexOf("organism") + "organism".length + 1;
			var n2 = q1.indexOf("&", n1);
			org = q1.substring(n1, n2);
		}
		return org;
	}

	function extract_sessionID(q1){
		var n1 = q1.indexOf("sessionID") + "sessionID".length + 1;
		var n2 = q1.indexOf("&", n1);
		var ss = q1.substring(n1, n2);
		return ss;
	}

	$("#switch_button").click(function(){
		var q1 = $("#query_1_url").attr("value");
		var q2 = $("#query_2_url").attr("value");
		$("#query_1_url").attr("value", q2);
		$("#query_2_url").attr("value", q1);
		return false;
	});

	$("#analyze_mode").change(function(){
		analyze_option = $("#analyze_mode option:selected").val();
		if(analyze_option=="shared"){
			$("#shared1").show();
			$("#search_mode").find('option[value="up_up"]').attr("selected", "selected");
			$("#exclusive1").hide();
		}else{
			$("#shared1").hide();
			$("#exclusive1").show();
			$("#ex_mode").find('option[value="up"]').attr("selected", "selected");
		}
	});

	$("#search_mode").change(function(){
		search_option = $("#search_mode option:selected").val();
	});

	$("#ex_mode").change(function(){
		ex_option = $("#ex_mode option:selected").val();
	});

	$("#submit_button").click(function(){

		//create sessionID
		var d = new Date();
		var n = d.getTime();
		sessionID = n.toString();

		var q1 = $("#query_1_url").attr("value");
		var q2 = $("#query_2_url").attr("value");
	
		organism1 = extract_organism(q1);	
		organism2 = extract_organism(q2);	

		sessionID1 = extract_sessionID(q1);
		sessionID2 = extract_sessionID(q2);

		if(organism1==organism2 && analyze_option!="shared"){
			alert("Error: l1 and l2 refers to the same organism: " + organism1 + 
			". As a result, you can only analyze genes that are shared between l1 and l2. Please go back and change your options.");
			return false;
		}

		if(analyze_option=="shared"){
			opt = search_option;
			organism = organism1;
		}else if(analyze_option=="exclusive_to_left" || analyze_option=="exclusive_to_right"){
			opt = ex_option;
			if(analyze_option=="exclusive_to_left"){
				organism = organism1;
			}else{
				organism = organism2;
			}
		}
		var show_all = "false";
		var show_other_not_significant = "false";
		if(opt=="down"){
			show_all = "true";
			show_other_not_significant = "true";
			opt = "down_down";
		}
		else if(opt=="up"){
			show_all = "true";
			show_other_not_significant = "true";
			opt = "up_up";
		}
	
		$.post("/modSeek/servlet/GetDistanceServlet", {organism1:organism1, show_list:analyze_option, option:opt, 
		organism2:organism2, sessionID1:sessionID1, sessionID2:sessionID2, show_all:show_all, 
		show_other_not_significant:show_other_not_significant}, function(responseText){
			//alert(responseText);
			$("#enrich_button").show();
			$("#export_button").show();
			var aa = responseText.split("\n");
			var query1 = aa[0];
			var query2 = aa[1];
			var len1 = aa[2];
			var len2 = aa[3];
			var similarity = aa[4];

			if(analyze_option=="shared" && (search_option=="sum" || search_option=="diff")){
				//var len1 = aa[2];
				var t = $("<table>")
					.css("font-size", "12px")
					.append($("<tr>")
						.css("font-weight", "bold")
						.append($("<td>")
							.append("Genes in <i>l<sub>1</sub></i> +/- <i>l<sub>2</sub></i>")
						)
						.append($("<td>")
							.append("Gene scores in <i>l<sub>1</sub></i> +/- <i>l<sub>2</sub></i>")
						)
					);
				for(var vi=5; vi<aa.length; vi++){
					var g1 = aa[vi].split(" ");
					t = t.append($("<tr>")
						.append($("<td>")
							.append(g1[0])
						)
						.append($("<td>")
							.append("<span style='color:red'>" + g1[1] + "</span>")
						)
					);
				}
				$("#result_header").empty()
					.append("Shared genes in <i>l<sub>1</sub></i> +/- <i>l<sub>2</sub></i>: " + len1 + "<br>");
				$("#result_table").empty()
					.append(t);
				

			}else if(analyze_option=="shared"){
				//var len1 = aa[0];
				//var len2 = aa[1];
				var t = $("<table>")
					.css("font-size", "12px")
					.append($("<tr>")
						.css("font-weight", "bold")
						.append($("<td>")
							.append("Genes in <i>l<sub>1</sub></i>")
						)
						.append($("<td>")
							.append("Ranks in <i>l<sub>1</sub></i>")
						)
						.append($("<td>")
							.append("Corresponding Genes in <i>l<sub>2</sub></i>")
						)
						.append($("<td>")
							.append("Ranks in <i>l<sub>2</sub></i>")
						)
						.append($("<td>")
							.append("Sequence similarities in <i>l<sub>2</sub></i>")
						)
						.append($("<td>")
							.append("Additional orthologs in <i>l<sub>2</sub></i>")
						)
						.append($("<td>")
							.append("Vis.")
						)
						.append($("<td>")
							.append("Vis.")
						)
					);
				for(var vi=5; vi<aa.length; vi++){
					var tx = aa[vi].split("|");
					var g1 = tx[0].split(" ");
					var g2 = tx[1].split(";");
					var tfirst = g2[0].split(" "); //first similar homolog
					
					var cc = []; //additional homologs
					for(var vj=1; vj<g2.length; vj++){
						var tt = g2[vj].split(" ");
						cc.push(tt[0] + " <span style='color:red'>" + tt[1] + "</span> " + 
							"<span style='color:blue'>" + tt[2] + "</span>"); 
					}
					var r1 = parseFloat(g1[1]);
					var r2 = parseFloat(tfirst[1]);
					var color1 = "";
					var color2 = "";
					if(r1<=0.50){
						var rlevel = 255 - Math.round((0.50 - r1) / 0.50 * 255.0);
						color1 = "rgb(255, " + rlevel + ", " + rlevel + ")";
					}else{
						var blevel = 255 - Math.round((r1 - 0.50) / 0.50 * 255.0);
						color1 = "rgb(" + blevel + ", " + blevel + ", 255)";
					}
					if(r2<=0.50){
						var rlevel = 255 - Math.round((0.50 - r2) / 0.50 * 255.0);
						color2 = "rgb(255, " + rlevel + ", " + rlevel + ")";
					}else{
						var blevel = 255 - Math.round((r2 - 0.50) / 0.50 * 255.0);
						color2 = "rgb(" + blevel + ", " + blevel + ", 255)";
					}

					t = t.append($("<tr>")
						.append($("<td>")
							.append(g1[0])
						)
						.append($("<td>")
							.append("<span style='color:red'>" + g1[1] + "</span>")
						)
						.append($("<td>")
							.append(tfirst[0])
						)
						.append($("<td>")
							.append("<span style='color:red'>" + tfirst[1] + "</span>")
						)
						.append($("<td>")
							.append("<span style='color:blue'>" + tfirst[2] + "</span>")
						)
						.append($("<td>")
							.append(cc.join(", "))
						)
						.append($("<td>")
							.css("background-color", color1)
						)
						.append($("<td>")
							.css("background-color", color2)
						)
					);
				}
				$("#result_header").empty()
					.append("<b><span style='font-size:12px;'>Your Queries</span></b><br>")
					.append("<b>Query 1 (l1) </b>: " + query1 + " (<b>" + organism1 + "</b>)<br>")
					.append("<b>Query 2 (l2) </b>: " + query2 + " (<b>" + organism2 + "</b>)<br><br>")
					.append("<b><span style='font-size:12px;'>Context Similarity</span></b><br>")
					.append("Homologous genes in <i>l<sub>1</sub></i> and <i>l<sub>2</sub></i>: <span class='big_font'>" + len1 + " " + len2 + "</span><br>")
					.append("There are <span class='big_font'>" + (aa.length-5).toString() + "</span> homologous genes in the context that qualify the criteria <b>" + 
						$("#search_mode option:selected").text()  + "</b>. <br>")
					.append("These <span class='big_font'>" + (aa.length-5).toString() + "</span> genes have cross-organsim sequence similarity average of <span class='big_font'>" + similarity + "</span> percent. <br>")
					.append("Below are these genes: <br>")
					;
				$("#result_table").empty()
					.append(t);

			}else if(analyze_option=="exclusive_to_left" || analyze_option=="exclusive_to_right"){
				//var len1 = aa[0];
				var ss = "";
				if(analyze_option=="exclusive_to_left"){
					ss = "1";
				}else if(analyze_option=="exclusive_to_right"){
					ss = "2";
				}
				var t = $("<table>")
					.css("font-size", "12px")
					.append($("<tr>")
						.css("font-weight", "bold")
						.append($("<td>").append("Genes in <i>l<sub>" + ss + "</sub></i>"))
						.append($("<td>").append("Ranks in <i>l<sub>" + ss + "</sub></i>"))
					);
				for(var vi=5; vi<aa.length; vi++){
					var g1 = aa[vi].split(" ");
					t = t.append($("<tr>")
						.append($("<td>").append(g1[0]))
						.append($("<td>").append("<span style='color:red'>" + g1[1] + "</span>"))
					);
				}
				$("#result_header").empty()
					.append("Genes exclusive to <i>l<sub>" + ss + "</sub></i>: " + len1 + "<br>")
					.append("Here are the " + (aa.length-5).toString() + 
						" genes (exclusive to <i>l<sub>" + ss + "</sub></i>)" + 
						" that are " + $("#ex_mode option:selected").text()  + ": <br>");
				$("#result_table").empty()
					.append(t);

			}


		});

		return false;
	});	

});

</script>
</head>
<body style="padding:0px;margin:0px;font-family:Arial;">
<div id="wrapper" style="width:800px;">
<table id="wrapper_table">
<tr>
<td style="padding:0px;">
<div id="query" style="background-color:rgb(94,39,80);height:28px;">
	<div style="position:relative; top:1px; left:10px">
		<img width="119" height="58" style="position:absolute" src="/modSeek/img/seek_mouse.png">
	</div>
	<table>
		<tr>
			<td style="width:200px"></td>
			<td style="width:400px; color:white;"></td>
			<td class="basic_font" style="width:200px;color:white;text-align:right;"><a class="white" href="index.jsp">Home</a></td>
		</tr>
	</table>
</div>

<div id="screen_info" style="height:30px; background-color:rgb(94,39,80);">
	<table>
		<tr>
			<td style="width:150px"></td>
			<td style="font-size:18px;width:450px; color:white;">
			Get the overlap of two lists of genes
			</td>
			<td style="height:25px;width:100px;"></td>
			<td style="width:5px;"></td>
			<td style="height:25px; width:110px;"></td>
			<td style="width:5px;"></td>
		</tr>
	</table>
</div>
<table>
	<tr>
		<td style="width:20px;"></td>
		<td class="basic_font" style="width:880px; padding-top:20px;">
<b>Query 1's result URL (<i>l<sub>1</sub></i>)</b>
<input size="60" id="query_1_url" name="query_2_url"> 
<input id="switch_button" type="submit" value="Switch"> 
<p>

<b>Query 2's result URL (<i>l<sub>2</sub></i>)</b>
<input size="60" id="query_2_url" name="query_2_url">
<p>
	
Analyze genes that are
<select name="analyze_mode" id="analyze_mode">
<option value="shared">homologs between l1 and l2</option>
<option value="exclusive_to_left">only in l1 but not in l2</option>
<option value="exclusive_to_right">only in l2 but not in l1</option>
</select>

<div id="shared1" style="display:none;">
Return homologous genes that are
<select name="search_mode" id="search_mode">
<option value="up_up">ranked high in <i>l<sub>1</sub></i> AND high in <i>l<sub>2</sub></i></option>
<option value="up_down">ranked high in <i>l<sub>1</sub></i> AND low in <i>l<sub>2</sub></i></option>
<option value="down_up">ranked low in <i>l<sub>1</sub></i> AND high in <i>l<sub>2</sub></i></option>
<option value="down_down">ranked low in <i>l<sub>1</sub></i> AND low in <i>l<sub>2</sub></i></option>
<option value="up">ranked high in <i>l<sub>1</sub></i></option>
<option value="down">ranked low in <i>l<sub>1</sub></i></option>
<option value="diff">the result of (<i>l<sub>1</sub></i> - <i>l<sub>2</sub></i>)</option>
<option value="sum">the result of (<i>l<sub>1</sub></i> + <i>l<sub>2</sub></i>)</option>
</select>
</div>

<div id="exclusive1" style="display:none;">
Return genes that are ranked
<select name="ex_mode" id="ex_mode">
<option value="up">high</option>
<option value="down">low</option>
</select>
</div>
<p>

<input id="submit_button" type="submit" value="Compare"> 

</td>
<tr>
	<td style="width:20px;"></td>
	<td style="height:10px;border-bottom:1px solid gray;"></td>
</tr>
<tr>
	<td style="width:20px;"></td>
	<td class="basic_font" style="padding-top:10px;">

<div id="result_header">
</div>

<input style="display:none;" id="enrich_button" type="submit" value="Perform an enrichment analysis on these genes"> 
<input style="display:none;" id="export_button" type="submit" value="Export these genes"> 

<div id="result_table">
</div>

		</td>
	</tr>
</table>
</td>
</table>



<div id="somediv" style="display:none;"></div>
<div id="overlapdiv" style="display:none;"></div>

<script src="/modSeek/comparelist_enrichment.js"></script>

<script type="text/javascript">
</script>
</div>
</body>
</html>
